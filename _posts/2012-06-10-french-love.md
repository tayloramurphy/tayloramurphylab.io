---
id: 178
title: Love in the Land of Romance
date: 2012-06-10T07:44:08+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=178
permalink: /french-love/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Comfort Zone
  - Future
tags:
  - american
  - cheese
  - eating
  - efficient
  - experience
  - france
  - french
  - friends
  - language
  - love
  - ocean
  - travel
  - vegan
  - vegetarian
---
I love that they use metric units. Meters and degrees centigrade makes more sense than miles and degrees fahrenheit.

I love that they use day/month/year as opposed to the illogical month/day/year.

I love that breakfast always had french bread and a croissant.

I love that everything is smaller and more compact. It’s efficient and not prone to excess.

I love that all the toilets have two buttons, one for low and high flow, and that this wasn’t a part of some huge marketing campaign. It just was.

I love that the stalls were more private. They’re more like personal closets than barely divided areas.

I love the language and the fact that I could figure out the gist of what was being said without severe effort.

I love the cafe culture. People talking, sharing a drink, taking a drag. The social time is important.

I love walking around and seeing throngs of people relaxing, eating bread and drinking wine. Socializing is valued.

I love that I didn’t see many bars or restaurants with TVs. Again, the message is clear: the people you are with are most important.

I love that they have a subway system  and decent public transportation.

I love that I was able to take a train across the country for a fair price.

I love the weather, the scenery, and especially the ocean.

I love the beautiful women that inhabit this place and that they’re made more attractive to me by their language.

I love the fact that there is cheese, _fromage_, everywhere. Even if I choose not to eat it.

I love that the streets don’t make sense. It’s controlled chaos and it’s fascinating.

I love that there are fewer fat people. Walking is important and not all aspects of the American lifestyle have be adopted.

I love that requesting a vegetarian meal means they bring you fish. It’s not techinically right, but it is amusing.

I love that there is a deep level of history, even if some of it is disliked.

I love that everywhere I went people were staring at my [huaraches](http://www.invisibleshoe.com/).

I love that nudity was optional at the beach. Women had their tops off and it was no big deal. Some had there tops on and it was no big deal.

I love that I was lost at times and that things were a bit out of control. My comfort zone did not contain me and it was amazing.

I love that the money is logical and can be used by anyone immediately.

I love that wine with every meal was just a given.

I love that Heineken was the primary beer on tap. It’s not my favorite but it was very good.

I love that many people had scooters or motorcycles. Why transport more than you need to?

I love that I could leave my key in a common area at the hotel and not have to worry.

I love that surfing was so prevalent.

I love that Diet Coke is Coke _light._

I love that meaningful glances can be shared between people who don’t share the same language. Laughter is universal.

I love that locals dress nicer than foreigners.

I love that the elevators were meant for one person. Or two if someone really wanted.

I love that I could go to an Asian History Museum with a friend and have a great time.

I love that grocery stores are the same and yet quantitatively different.

I love that while all of these are specific to my time in France, many are applicable to other places in the world.

Most of all, I love that I was able to experience this country. I love that my love for [travel](http://tayloramurphy.com/why-i-really-dig-traveling-now-even-for-a-funeral/ "Why I Really Dig Traveling Now, Even for a Funeral") has just grown by leaps and bounds.

I love that I am a citizen of the world.

I love that **we all are**.