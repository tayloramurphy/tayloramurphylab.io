---
id: 364
title: Neurosis, Books, and What If?
date: 2013-06-04T22:02:29+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=364
permalink: /neurosis-books-and-what-if/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Learning
  - Personal Development
tags:
  - altucher
  - books
  - freelance
  - negative
  - neurosis
  - positive
  - what if I go crazy
  - writing
---
I’m was looking at my catalog of posts on this blog: since January 2012 I’ve posted 59 times (60 if you count this one). In 2013, however, I’ve only posted 5 times (now 6). Why is that? What about this year has made me less public with my writing?

In trying to dissect this behavior I’ve landed on one main idea: fear. I’m terrified of posting my thoughts online. Why? I don’t know. Maybe I feel like I’m going to be judged.

I’m afraid if I say what I think about things I’ll be judged by potential employers, clients, friends, or worse, my parents. What if something I write causes someone to call me and lash out at me? What if the world is handed to me on a silver platter but it’s then taken away because of something I wrote on my blog? What if I let people into my brain and they don’t like what they see?

What if my sisters read my crazy thoughts and then they don’t like their brother? What if my parents are even more disappointed in me? What if I lose even more friends than I already have? What if my ex calls me up and cusses me out? What if what if what IF?

It can be neurosis inducing. Well, it _is_ neurosis inducing.

Over the past week and a few days I’ve written over twenty thousand words. Most of them you haven’t seen. I’ve posted a bunch on my [642 Things](http://needsomerope.tumblr.com/) Tumblr page, but I haven’t been sharing much of what I write. The majority has been in this ‘book’ I’m writing about my time in grad school and all that’s happened in between. But am I really going to share that? It scares me. There’s almost 10,000 words in this self-serving memoir. I expect it’ll be five times that when I’m “done” writing it. Why am I writing it? I don’t know. It feels good to get the memories and experiences on paper. I’d like to think it could help people. But maybe it’ll just dredge up old things nobody wants to think about. What if I forget to mention something? That happens a lot. I’m scared of my own memory and what I don’t or can’t remember.

Plus, who am I to be writing about something that, in comparison to a lot of other things, isn’t that big of a deal. Why am I dwelling on the past? That’s really all I’m doing with that story. I’m thinking about everything from 2008 up until now. There’s a lot of shit in there. There’s some awesome stuff too, but there’s also a lot of shit. I’ve been changing names as I write. But maybe I’ll never publish. I’m not sure I want anyone to read it. I think I do.

I know this is all rambling. These are the crazy things that are going through my head.

Some of this comes from the fact that I have zero income right now. I have some savings and I have a few more things I know I could sell, but that’ll dry up at some point. Plus I have a ton of medical debt. 15 tons actually. What am I going to do with that?

I’m working to start a freelance academic editing business. But I’ve yet to hear back from anyone I’ve pitched. What if I write 1000 emails and nobody replies?

Intellectually, I know what I’m doing wrong and why I’m feeling like this. I’m making a lot of fear-based decisions and, really, those never work out. The things I’m afraid of aren’t a big deal. So what if people judge me? So what if I lose friends or piss people off? That can be a good thing. Maybe I’ll scare off some people but what if I draw in the right people for me?

All of my questions and fears are looking at what could go wrong. But aren’t there just as many possibilities of what could go right? Things that may happen that would be great and awesome.

What if, by sharing all these crazy thoughts, I connect with someone who presents a great opportunity? What if my parents read what I write and maybe they get a better understanding of me and we increase our dialogue? What if I write this book and share it with the world and somebody finds some great value in it, whether it’s about graduate school, marriage, atheism, or whatever? What if I get in touch with people who accept me for who I am and want to give me money because I’ve helped them and can continue to help them?

Why focus on the negative?

I’m reading a book right now by [James Altucher](http://www.jamesaltucher.com/) called [Choose Yourself](http://www.amazon.com/Choose-Yourself-ebook/dp/B00CO8D3G4/ref=sr_1_1?ie=UTF8&qid=1370401153&sr=8-1&keywords=choose+yourself). He basically posits that the middle-class is dead and so is working for an employer who a)doesn’t really care about you and b)has singular control of your well-being. Technology is killing many of the jobs and the only way to make it in this world is to choose yourself for success. Educate yourself, pursue your own things, and create the life you want. And it all starts with building a strong, internal foundation for your life. Strong physical, mental, emotional, and spiritual health. (Spiritual is kind of a crappy word in my opinion. He’s not talking about religion, simply being present and not time traveling in your mind.)

Much easier said than done, of course, but I get the message.

The writing on the wall is clear, at least to me: abundance and happiness are not going to come into my life through a traditional job. I have to be master of my work and time if I want to be successful and happy, according to my definition of such things.

The road isn’t easy and it’s not straight and I’m likely going to fail a few times. But no matter how neurotic I feel right now while on this path, I know it’s less than what I’d feel 10 years down the road stuck in a place I don’t want to be.

It’s time for me, and everyone else, to stop making decisions based on fear. It’s easy to fall into that habit, especially when you start uncovering the challenging things in your life that need work or that scare you. Fear-based decisions are not good. Growth and abundance-based decisions are better. So what if the shit hits the fan because I’m open and public with my writing?At least you’re clearing out the shit so the nuggets of hidden gold can shine.