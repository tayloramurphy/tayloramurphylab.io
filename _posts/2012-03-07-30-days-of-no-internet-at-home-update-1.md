---
id: 82
title: '30 Days of No Internet at Home: Update 1'
date: 2012-03-07T12:04:05+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=82
permalink: /30-days-of-no-internet-at-home-update-1/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
tags:
  - art
  - connectedness
  - distraction
  - internet
  - online
  - productivity
  - reading
  - reddit
  - writing
---
On the evening of February 29, I unplugged the router and modem and said goodbye to my wireless connection. It was a relatively simple task; one quick jerk on the power strip and the connection was severed. I felt like a burden had been lifted and I could breathe: a deep inhalation and I was ready for bed.

## The moon set; the sun rose: the first day.

To be perfectly honest, living without internet at home has been relatively easy. During the week, especially, it’s actually a relief to come home and be disconnected. I feel like I’ve shed a rain-soaked cloak when I walk through the door and see that I have no signal.

I have between 3-5 hours between the time I arrive home and the time I go to bed. Typically I’ve been doing some minimal chores around the house: dinner, dishes, laundry. When my work is done, I have quite an abundance of things I could do.

I’ve really enjoyed reading in peace. I finished reading _Bird by Bird_ by Anne Lamott and I’m nearly done with _Eats, Shoots & Leaves_ by Lynne Truss. My writing has gone well and I’ve been able to produce 1000 words, at least, per day consistently. I’ve also spent time playing sports or socializing with friends.

However, this weekend presented a new challenge. At work during the day, I have full access to the internet and I use it well. Probably too well. On the weekend, though, I am without for over 12 hours. This was definitely a first for me.

I filled my time in a few ways. I deep a deeper cleaning of my home, I met up with a man from craigslist to sell some of my junk, I ran 9 miles, and I did some reading and writing. After I exhausted those possibilities, I still found that I had some time. I decided to get cleaned up and head downtown for an art crawl.

The art crawl was a unique experience for me; I had been to one previously, but only with friends. This time I went alone. Being alone in a social situation is an interesting experience. I enjoyed people-watching in a detached sort of manner, but I also craved the interaction of these people. I did talk to a few random strangers very briefly, but for the most part I observed and soaked in the atmosphere. It was a very enjoyable way to spend 2 hours, even if I was mostly silent.

Sunday was similar. I spent a good portion preparing for my first Toastmaster’s speech. _Ed. Note: It went swimmingly; I earned Best Speech._ I also spent time with my family at lunch and hung out with friends playing frisbee golf. The weekend was a success and I expect the coming weekend to proceed in similar fashion.

The silence at my house has also been good for meditation. I’ve been working on increasing the time I meditate; this isn’t an easy task for someone like myself who’s mind goes a mile-a-minute. But every time I focus and actually meditate, I find I am much calmer and pleased with my overall life situation. Mediation is good.

## The Dark Side: Or Why 3G is a Crutch

As beautifully as I’ve portrayed my disconnected home-life, I’ve left out one important detail: my iPhone. Even though I am disconnected from the coaxial connection, I am still able to surf the net, albeit at a much slower rate. Having 3G access on my phone means I haven’t fully disconnected.

There were several times where I did rely on my phone to entertain me. In the mornings I’ve noticed that I still check my email and hop on Reddit for 15-30 minutes. I have been playing Words with Friends (5 games going on at last counting&#8230;). And I’ve been able to check twitter, NPR, and hockey scores.

I believe this to be a mild form of cheating. While technically okay because I’m not using my “at home” internet, I am still connecting to the web on a semi-regular basis. It would seem like I’m a drug addict who is taking the slightest of hits and claiming to be clean.

To step up my game, I’m going to do the following for the next few days. My reddit app is now deleted off my phone. No more of the black hole at home when I’m ‘bored’. Also, I am going to commit to not checking email in the mornings at least until I sit down for breakfast. Perhaps it will be better even to wait until I am at work, but I’ll reevaluate after a few days.

## Onward and Upward

Overall, I’m pleased with how it’s going. I managed to actually cancel the Internet after talking with Comcast and they’ll reactivate it on April 5th (so this experiment will be a bit longer than one month). My phone has been somewhat of a hindrance to true connectedness, but my activity level is a far cry from where it used to be.

I expect the rest of the time to go even better and I’ll be sure to update my progress soon.