---
id: 473
title: The HOA as a Slice of Americana
date: 2017-09-04T10:12:00+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=473
permalink: /americana/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Personal Development
---
<p class="p1">
  Just over a week ago my neighborhood had their annual Home Owner&#8217;s Association meeting. It was a unique experience for my wife and I and our close friends that live two doors down. Leaving that meeting gave me a lot to think about and I had to write something to coalesce my thoughts.
</p>

<p class="p1">
  As the week progressed, I thought broadly about the purpose of the HOA. As stated by most HOA&#8217;s, they exist to protect the value of homes within their neighborhood. They do this by attempting to control behavior. As far as I could tell, most everything in the HOA governing documents (Covenants, Conditions, and Restrictions &#8211; CCR) is an attempt to limit certain behaviors in the belief that if people were to do whatever they wanted, property values would plummet. Everything else is about enforcement of those rules and how you can go about changing them. Our document is quite long and the PDF I have is in horrible condition.
</p>

<p class="p1">
  The morning of our meeting, members of our HOA board were canvassing the neighborhood attempting to drum up support for a measure to change the CCR. As it stands, 90% of homeowners have to agree to a change to the CCR for anything to actually change. The board was attempting to lower that cutoff to 51%. There wasn&#8217;t much of a pitch and it felt like a power grab. Lindsay and I reviewed the ballot and the notes they&#8217;d given us and felt like 51% was too low and we were resolved to vote &#8220;No&#8221; on the measure.
</p>

<p class="p1">
  That evening we walked down to the common area where the meeting was to be held. Along the way we chatted with our friends and agreed that 51% was too low and that somewhere around two-thirds was the sweet spot. 51% was called the nuclear option in Congress and we felt we had to be better than that.
</p>

<p class="p1">
  The meeting got underway and almost immediately I felt like I&#8217;d been transported to an episode of The Office meets Parks and Recreation. We had &#8220;angry guy&#8221; who seemed genuinely pissed at the power grab that 51% seemed to imply. Many people agreed with him. Immediately following that was a guy who claims to have read the entire CCR and says that, actually, we only needed 75% of homeowners because of something in the CCR that automatically lowered it. The board wasn&#8217;t sure of that which then set off angry guy again because one of the board members was a representative from Ghertner and what were we paying all of this money for if he didn&#8217;t know what the CCR says?
</p>

<p class="p1">
  Then came the impassioned pleas from the current board and former members of the board. They&#8217;d been trying to change it to 51% for years just so they could get <i>something</i> done. Enforcement, they said, was a big problem with our HOA. Letters could be written all day long but without any sort of monetary penalty behind them, people ignored the letters. Second on the agenda after lowering the bard to 51% would be to change the CCR to enable the HOA to enact fines.
</p>

<p class="p1">
  Renters were a big concern too. As it stands, the CCR had no provisions for the number of homes that could be rented out. They wanted to change that. This led to a funny (to me) moment when someone not on the board (but who definitely wanted to be) asked who here has rental property in this neighborhood? One guy raised his hand. Then he amped it up by asking whether that guy actually lived in the neighborhood. Which he did. His comments then went from snobbish to conciliatory as he thanked the landlord and neighbor for being a good citizen. The underlying subtext, of course, was that basically all renters were bad. It was humorous, but just goes to show you can&#8217;t always make assumptions about the attendees.
</p>

<p class="p1">
  A full range of other issues came up without much resolution either. We had the lawyer who looked nothing like you&#8217;d expect a lawyer to look like offer to read through the CCR for free, we had the guy complaining about dog shit (not me!), we had the older man complaining about <span class="s1">kids</span> millennials and their kids, another guy who kept saying we should &#8220;be more East Brentwood and not West Antioch&#8221;, and a woman about our age who loudly asked if the fees could ever go down. The topics that received the most agreement and excitement were around what we could do to prevent people from running the light at the entrance to our neighborhood and how we could convince/force the new owners that weren&#8217;t in the Woodlands HOA but were using our roads and common areas to actually join the HOA.
</p>

<p class="p1">
  At the end of the meeting when new board members were being voted on, our friend Christina surprised us all by standing up and throwing her hat in the ring. It was an amazing moment because she&#8217;s quite pregnant and sticks out compared to the rest of the people on the board. Everyone else on the board was an older white male, and here she is, a young, tattooed, pink-haired pregnant woman volunteering to do something you probably wouldn&#8217;t expect. From her view, though, it made perfect sense. Her day job is a community manager for a tech startup and being a member of the HOA board is basically like managing a community. Plus, she brings some needed diversity to the board. She was voted in and that basically ended the meeting. Lindsay and I had to leave immediately after to go to a party for the Mayweather-McGregor fight (where the host had hired a harp player for a few hours…)
</p>

<p class="p1">
  &#8212;
</p>

<p class="p1">
  Looking back, I feel a little guilty with my attitude towards the meeting. It&#8217;s fun to stand in the back and laugh at people who are taking all of this so seriously. There was such a cast of characters at this meeting it was hard not to imagine the cameras panning and zooming around the place with a random cut to me and my best &#8220;Jim Face&#8221;.
</p>

<p class="p1">
  But this isn&#8217;t the office, this is real life. And real life is full of complicated and different people. I feel like that meeting was a true slice of Americana. The attendance at the meeting was bigger than they&#8217;d seen in the previous few years partly, I think, due to weather, but also because of the world we&#8217;re now living in. Looking at the news everyday and seeing everything insane that&#8217;s going on, it&#8217;s easy to wonder just what the heck you&#8217;re supposed to do as an individual. For me, going to the HOA meeting felt like I was fulfilling some of my responsibility as a citizen to participate. Think global, act local; nothing is more local than one&#8217;s own HOA.
</p>

<p class="p1">
  Actually seeing these people as real and not as characters on a sitcom also makes it easier to empathize with the challenge of people on the board, past and present. We had maybe 40 homeowners represented at the meeting. There are 199 homes in the neighborhood. That&#8217;s 20%. The passion and care from that percentage was obvious, but it&#8217;s difficult to translate that into action for the remaining 70% of homeowners who would block us, simply by their inaction, from moving things forward. I could see how our little HOA is a microcosm of our greater national struggle to get people to take action.
</p>

<p class="p1">
  And the reality is that, like them or not, your neighbors are who they are. Very little can be accomplished when you&#8217;re antagonistic to them, so it&#8217;s better to work with them and find common ground. And it&#8217;s not always easy. Our HOA meeting was a true showing of the diversity of Americans. Many different backgrounds, cultures, beliefs, and values were represented. I may not agree with the hard-core Christians, but I still have to work with them. They&#8217;re my neighbors and they have a right to speak their mind and share their opinions. At the end of the day, we&#8217;re all<i> Homo Sapiens</i> and we all generally want the same thing. I really believe that we&#8217;re all basically good people and if you can connect with that goodness and share some love, that things will work out and progress can be made.
</p>

<p class="p1">
  I&#8217;m looking forward to continuing to work with the HOA and be a participant in my community. It definitely helps that a close friend is now on the board, but I think either way I would have tried to do more. The only other option is apathy and that, obviously, is the worst choice of all.
</p>