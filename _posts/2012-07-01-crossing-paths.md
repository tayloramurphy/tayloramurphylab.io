---
id: 187
title: Crossing Paths
date: 2012-07-01T19:30:54+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=187
permalink: /crossing-paths/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Relationships
tags:
  - crossing
  - emotion
  - experience
  - friends
  - grow
  - learn
  - path
  - relationships
---
“Sleeping out all night beneath the desert stars, a dream in my eye and a prayer in my heart”

We all walk our own path on the journey of our lives. Each is completely unique and no one else can fully know another’s.

At times, two paths may parallel for a while and touch during brief moments. Other times these paths can twist around each other like a strand of DNA only to break apart and go in opposite directions. Still others become so close that they look as one and together they ride a brief roller coaster only to separate with mutual understanding that what they had was temporary and not meant to last, but neither traveler regrets the time or energy spent.

Each of these encounters with another’s path can leave a lasting impression, but only if you allow it. After separation, it is easy to drown out true thoughts and feelings with temporal distractions. Much harder is facing reality and learning and growing from the experience. I choose the latter.

I choose to see the beauty in temporary things and appreciate them for their short existence. I choose to cherish the memories, the experiences, and the learned lessons and move on with no regrets. My choice is to grow with each crossing and to not live in the past or long for the future. I choose to live in each moment and embrace the now.

The separation of two paths comes with a certain sense of loss, some stronger and longer lasting than others. No two paths can ever stay joined for ever. They did not start together and death will inevitably and irrevocably separate the two. Our job when sharing another’s path is to leave it better than we found it. To have cultivated their garden by some measure.

My hope is that I leave those around me better off than when our paths first crossed. Perhaps our paths will cross again in a different form, or maybe they won’t. And if they don’t, I want to be able to say that I added as much value as I could and treated the other’s path with love and respect.

Your path and mine may never cross again, but I’m better off for the shared travels, even if it was for the briefest of moments.