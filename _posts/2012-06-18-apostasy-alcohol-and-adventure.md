---
id: 182
title: Apostasy, Alcohol, and Adventure
date: 2012-06-18T07:08:16+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=182
permalink: /apostasy-alcohol-and-adventure/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Relationships
tags:
  - atheism
  - atheist
  - bacteria
  - camping
  - drinking
  - drunk
  - friendship
  - fun
  - hiking
  - outdoor
  - raccoon
  - relax
  - respect
  - swimming
  - trust
  - vacation
---
Wallace the raccoon was getting drunk eating the remains of our vodka infused watermelon.

My friend had named him that after he’d watched the creature almost run off with our bag of Sun Chips. As the evening wore on and the Great Chip Heist was a distant alcohol-soaked memory, he started trying to befriend all the racoons that wanted to share in our revelry. He managed to close the gap to 2 feet before Wallace (or maybe it was his brother Charlie) declared ‘fuck this’ and scurried away to the drainage ditch.

Four of us had set up camp in site 14 at Old Stone Fort State Park in Manchester, TN. We had all met on the way to the Reason Rally back in March thanks to one of our member’s eavesdropping and nosiness. As we became better friends on the trip, we thanked her profusely for being nosey because it lead to epic times on the bus and a promise of more epic times camping.

This trip was the fulfillment of that promise. We are all outdoor enthusiasts and experiencing nature in this way allowed us to have a raucous good time on the cheap.

In less than 36 hours of total camping time, we’d succeeded in creating unforgettable memories. The shared experiences of dining on fire-cooked food, imbibing vodka and rum, and freely conversing strengthened our comraderie. A sign of a good friendship is when you can later discuss everything you said while drunk.

The four of us went on several hikes together as well as found several areas to go swimming. I managed to touch a catfish, catch a snake, jump off a 30 ft cliff and cover myself in fecal coliform all within a 1 mile stretch of river. Had one of my friends not read the ill-placed sign we would have never known about the fecal coliform (thanks buddy) and subsequently wouldn’t have fretted about the potential diseases that would ravage our bodies because of this foul bacterium. But seeing as how the damage had been done, we decided to keep swimming and just live in the moment.

In addition to the outdoor activities, we also had deep conversations about life, philosophy, and science while drinking and smoking. Our atheism is what originally brought us together, and it’s appropriate that we would have these self-aware conversations.  We’re all cognizant enough to value quality people when we meet them and we saw in each other something worth sticking around and cultivating. If leaving the church has taught me one thing, it’s that it’s better to have a few high-quality friends than a congregation of acquaintances.

The people I was sharing food and drink with this weekend are of the highest-qualilty and deserve the trust and respect owed to close friends. With these kinds of friends, even during the most debaucherous times knowledge can be gained and value added in unexpected ways.

Wallace, sadly, was not a true friend our of group. And now he’s probably nursing one hell of a hangover.