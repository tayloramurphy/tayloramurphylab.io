---
id: 411
title: Thoughts in the Wild
date: 2014-06-30T19:43:10+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=411
permalink: /thoughts-in-the-wild/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Future
  - Personal Development
  - Relationships
---
My mind wanders from thought to thought. Mental parkour that balances on one idea long enough to get to the next without lingering long enough to establish a true connection.

What is it my brain needs? It&#8217;s the age-old ponderance of everyone who has thought for long at all. What is immediately clear is I am wholly non-unique in my struggles. Men have lived and died before me and have wondered and searched all the same. Only the externalities are different. I have modern life. They had ancient life, although it was clearly modern at the time.

My truth, right now, is I&#8217;ve never truly been happier with all aspects of my life. My work, my loves, my hobbies, my life. They are all exceeding any sort of expectations I would dare to have. Even so, I don&#8217;t anticipate any inevitable fall. Naiveté hasn&#8217;t gripped me so hard as to make me think things continue like this until I die. But I have prepared myself for the waves of life. Valleys follow peaks. So it goes.

&#8212;

I wrote this in April when Lindsay and I were camping. The thoughts I had then are much the same now. My monkey-brain still jumps from idea to idea, but I am getting better at training it. I am still happier than ever with my life. Absent from my view is any sort of anticipation that it&#8217;s all going to come falling apart. True to my Stoic beliefs, I&#8217;m prepared to handle whatever may come my way, but I&#8217;m not letting any potential unknown ruin my outlook. Life, on average, is great. I&#8217;ve worked hard (and been very lucky) to make it great. I will continue to work hard as well as accept any luck that happens to come my way.  Life goes on, up, down, and around. So it goes.