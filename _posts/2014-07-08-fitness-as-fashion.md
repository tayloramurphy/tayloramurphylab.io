---
id: 414
title: Fitness as Fashion
date: 2014-07-08T20:36:10+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=414
permalink: /fitness-as-fashion/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Future
  - Personal Development
tags:
  - discipline
  - exercise
  - fashion
  - fitness
---
I had a flash of clarity the other day about fashion. The first bit of truth was that I suck at it. Sure, I can throw on a pair of jeans and a nice shirt and look decent. My nice suit looks pretty good on me too. But, in general, I suck at fashion and I don&#8217;t really care to improve.

The second bit of truth is that the best form of fashion I&#8217;ve ever seen is the one our country struggles to maintain: fitness.

Fit people look good. They project an aura of confidence and inner strength that bleeds through the clothes they wear. Men and women who are able to pick up more than a gallon of water have a presence that shines.

The fact that someone works out says a lot about them. They care for their bodies. They have the discipline to do the hard work of stressing their muscles. They have the foresight to invest in themselves for the future. They are probably happy because of all the feel-good chemicals that your brain releases when you exercise. A fit person has all these things and more.

A t-shirt looks better on a guy who&#8217;s muscles are visible through the fabric. Yoga pants look better on women who actually do yoga and fit into them properly. Shorts look better on both when the legs are toned and tight. The face of someone who is fit has clear definitions between the neck, the chin, the cheeks, and the eyes. Form fitting clothes look great on both men and women. The simple becomes great when the support structure is strong.

Even the simple act of exercising transforms an outfit. The scale might not have changed, but you&#8217;ve gained confidence that shows in how you stand and act. Your inner strength and effort oozes out and reveals itself to the world.

Physical strength supports every single aspect of a person&#8217;s life. It just makes everything better. And it makes fashion easier.