---
id: 140
title: The End of a 30-day Challenge and an Inspirational Story
date: 2012-05-07T08:46:56+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=140
permalink: /the-end-of-a-30-day-challenge-and-an-inspirational-story/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Comfort Zone
tags:
  - arthur
  - beliefs
  - challenge
  - change
  - eating
  - fasting
  - inspire
  - life
  - vegan
  - vegetarian
  - weight loss
---
May 4th, aka Star Wars Day, was the last day of eating an entirely vegan diet as well as attempting to not eat after 7:00 PM.

Eating vegan was, in a word, easy. I was mostly vegan prior to this month due to my lack of the lactase enzyme. Eliminating eggs and meat from my diet on top of what I was already doing wasn&#8217;t difficult to do. Going out to eat hasn&#8217;t even been that big of a hardship. Chipotle is great for vegans (beware the pinto beans) and most places have some sort of vegetarian option as well.

One positive aspect of eating out was the lack of choice. For many people this would probably cause a lot of anxiety. All of those food options that you can&#8217;t have. For me, I tried to spin it in a positive light. Instead of waffling back and forth between all of these differenct choices, I was able to pick what I want quickly and get back to the more important things like conversation and fellowship. There was no analysis by paralysis because the choice was essentially made for me.

There were a few times I had to remove some cheese myself, and I did catch myself craving a burger occasionally, but overall it was a rousing success.

The cool thing about eating a vegan diet is that even when I overate, which happened a few times, I still felt pretty &#8216;light&#8217;. The food isn&#8217;t naturally heavy and I never felt like crap after eating a vegan meal.

From here on out, I plan to stick with this diet for 95% of the time. I imagine there will be a few times where I&#8217;ll get a burger, but for the most part, I just feel too damn good to go back to eating the other way. My mind feels clearer, I feel more athletic, and generally &#8216;lighter&#8217;.

As far as not eating after 7 PM, that was a bust. I managed to do it for a few nights, but there were times when I was not getting home till 8 or 9 and I just had to eat something. Several days I was just weak and said screw it. I&#8217;m still interested in trying to achieve this, but my schedule is a little wonky. Perhaps I&#8217;ll try this again in a few months.

I&#8217;ve been thinking, briefly, on what my next challenge should be. If you, dear reader, have any suggestions, please leave a comment or shoot me a tweet. One thing I definitely want to do is fast for 48 hours. But that&#8217;s a short challenge. I want something a bit longer and it doesn&#8217;t have to be related to food. It also doesn&#8217;t have to be sacrificing something. Perhaps I do a 30-day challenge where I meditate every day or 30-days of waking up at 5 am. I&#8217;m open to suggestions.

&#8212;

Before I close, I wanted to share one of the most inspiring videos that I&#8217;ve seen this year. This video is about a man named Arthur Boorman who was told that he was never going to walk unassisted again.

He took that notion to heart and accepted it as fact.

But then he started to question it. He challenged the notion that he couldn&#8217;t walk without assistance. He doubted whether the way he was living was the only way he could live.

Once that doubt started, and once he got a taste of what might be possible, he set about making it happen. Though only one person would help him do what he wanted, he made the effort and put in the hard work to change his life.

The result speaks for itself.

[Arthur Boorman](http://www.youtube.com/watch?v=qX9FSZJu448)

When you&#8217;re feeling down, remember Arthur. Remember his perseverance and remember that he is no different than you. He wanted something bad enough so he made it happen. You can too.