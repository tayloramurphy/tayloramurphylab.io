---
id: 150
title: When life gets rough, make good art.
date: 2012-05-23T22:54:13+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=150
permalink: /when-life-gets-rough-make-good-art/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Emotions
tags:
  - art
  - challenge
  - comfort
  - create
  - fail
  - gaiman
  - neil
  - passion
  - pretend
  - quotes
  - succeed
  - support
  - write
  - writing
---
Rather than try to explain why I haven’t made a post lately and give lame excuses as to my absence, I’ll just sum it up by saying shit happens and focus was lost.

But I’m back in action, ready to slam some words into a cogent structure.

I’ve been watching several motivational videos over the past two weeks and they’ve all been quite excellent. From [Will Smith](http://www.youtube.com/watch?v=aaRqr7oQ2bo) to [JK Rowling](http://www.ted.com/talks/lang/en/jk_rowling_the_fringe_benefits_of_failure.html), they’ve each had an inspiring message that gets you so pumped up that you just want to do everything. One speech in particular resonated with me on a deep level. So much so that I found myself writing down quotes and staring enraptured at the screen.

Neil Gaiman gave a [commencement address](http://vimeo.com/42372767) at the University of the Arts recently and it was the best one I’ve ever seen.

He had never gone to a place of higher-learning. His success in life came through hard work, luck, and perseverance. He has no cause for derision of colleges and the school system, but it just wasn’t for him. Writing was his passion and so he did that. Eventually people started to pay him for his writing.

Throughout the talk he offered some advice which spoke to me and, I hope, many others. He gave the usual advice such as being thick-skinned, making mistakes, doing things you’re excited about (and not just for money), as well as envisioning your goal as a distant mountain. These are well-known pseudo-platitudes that are easy to gloss over.

But he added to the layer of wise cliches.

_The moment you’re naked and feeling like you’re sharing too much, that’s the moment you may be getting it right._

_The keys to success are to produce good work, turn it in on time, and be a pleasure to work with. But you don’t even need all three. Two will work just fine. If you produce good work and turn it in on time, people will ignore how much of a pain you are to work with. If you’re a pleasure to work with and you do good work, people will forgive it being a little late. And if you turn your work in on time and you’re and absolute joy to work with, then your work doesn’t even have to be that good._

_Pretend you’re someone who could do it_.

All three of these quotes went beyond the usual sentiments of motivation and success. I’m familiar with the doubt and fear that comes with feeling naked and wondering if I’ve exposed too much of my soul. It’s somewhat comforting to know that you don’t really have to have all three traits to be a success in your chosen path. But I imagine that those who can embody all three will go far beyond those with two. And for those of us who feel like we have no skill, the power of imagination can carry us through the dip to the top of our mountain. Playing pretend is not only for children, but for those who want to succeed. Visualization of the success you yearn for can be a powerful method of getting you there. It won’t do the work for you, but it can make it easier.

The above are some great and inspiring quotes, but they’re not the one that resonated the most with me.

**_When life gets rough, make good art._**

I chose it as the title of this post because of how succinct and fervent it is.

Over the past few weeks, my life has been rough. Perhaps not in a visible manner to those who would examine it, but on the inside, things were not calm. This is part of the reason why I wasn’t writing. It’s easy to get wrapped up in your own head and make excuses.

The thought never occurred to me that instead of distracting myself from my feelings and my own humanity, I should turn and face those demons and push back by creating something good. When Neil said those words, it was like a beacon in the night. A guiding light that promised to drag me out of the muck.

Yet even with that knowledge I still wasn’t doing anything about it. I had relegated it to the recesses of my mind as something that seemed ‘nice’ and ‘should be done’. It wasn’t until my friends and family, my readers, said something that I was able to break the spell.

Some of my family asked what’s up with the lack of posts? Friends said they were looking forward to reading what I had to say and that they missed my writing. One friend even asked what my next 30-day challenge was going to be.

This absolutely humbled and inspired me. It’s one thing to know that people enjoy what I’m writing (and believe me, I’m so thankful). But it’s another thing to have those same people express concern when they see that you’re not doing what they know you love to do. Their thoughtfulness showed that they support me in my endeavors, and they want to see me succeed. More than that, they enjoy what I have to say!

All of this, the motivational videos, the lack of writing, the support of friends and families, finally coalesced into action. I sat down again and actually got back to writing. And it felt so good. There’s something so immediate about writing that I just love. Creating something from nothing is a powerful feeling and it’s amazing how much I miss it when it’s not happening.

So here I am. Back for good I hope. I’m not naive enough to think that life won’t throw me challenges or make it difficult to write, but I am naive enough to think that I will always push back and do the work. **_When life gets rough, make good art_.** Those words are my guiding force right now.

Feeling sick? **Make good art.**

Depressed? **Make good art.**

Tired? **Make good art.**

Full of fail? **Make good art.**

Anything shitty at all happen? **Make good art.**

It’s simple and powerful.

_What’s your art? Do you create when things get tough or do you distract yourself from it? What motivational videos have you watched that really inspired you?_

__

As part of my recommittment to this whole writing thing, I’ve decided upon my next 30-day challenge. Most of my challenges have been focused on removing something from my life. This is is adding to it. For the next 30 days, I plan to write every day. But the real challenge is the amount: 2000 words per day. This is a number that intimidates me. I almost erased that number and put 1500 instead because I thought it would be more reasonable. But I knew I must be onto something if I was unsure of my ability to actually achieve that lofty goal. It wouldn’t be a challenge if I know I’m going to succeed, right? So there you have it. 2000 words per day. They may not be the best, but they’ll definitely get written.