---
id: 220
title: Walking Through Memory
date: 2012-10-04T12:43:45+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=220
permalink: /walking-through-memory/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Learning
  - Personal Development
  - Relationships
tags:
  - deer
  - hiking
  - meditation
  - nature
  - outdoors
  - reflection
  - solitude
---
This morning was such a idyllic environment that I decided to go for a walk. There are no trails on my grandparent’s property meaning I have to make my own or simply follow the road.

I chose the road and started walking in the opposite direction from which I drove in. Best I could tell, it was Northeast.

In my memory is a map of the road. The pavement turns into gravel, turns into dirt, turns into wooded underbrush. But it’s still a road. My family and I used to take walks on that road. Scattered memories bring up reminders of teenage angst and nascent maturity. I remember walking with my best friend as she picked up a frog, which my mother deathly feared, and it peed all on her hands. There was an abandoned and shuttered coal mine, long since rusted and forgotten. Animal tracks along the ground.

It was with these memories that I started my walk. The road is a bit more paved than it used to be. It switches back to gravel before turning back into even newer, blacker pavement. Our family friends had built a house a few years ago and recently decided to pave the road up to their entrance. I continued following the pavement before I realized it ended.

_Weird_, I thought to myself. _I knew there used to be a road here_.

I turned around and started back the way I had come. My focus was now on looking for signs of this hidden path. With my head turned to the left, I looked for places where the brush was lower, the trees smaller. It _had_ been many years since I’d been on it.

I came upon a pile of gravel that brought back another memory. Prior to the pavement, the road was covered with these small stones. They used to be in a massive pile, taller than I was, which marked the ‘beginning’ of my lost road. Now, that pile was smaller, barely above my knees. But it was still there.

I had found the road. Rather, I had found what used to be the road. It was now covered with growth, washed out by the rains, and nature was starting to reclaim it. Clearly, no vehicle, and doubtful any person, had been this way in some time.

My heart was saddened at this thought. I saw no reason why it should be closed off and forgotten. It was fun to walk on and probably quite useful. But now it was nearly inaccessible.

Part of me took this as a sign that I shouldn’t continue in this direction. I knew other people had bought the property surrounding my grandparent’s land and perhaps they wanted this road to be blocked off. But there was another part of me that desperately wanted to go on it.

So I started walking. I pushed aside some trees, slipped in the still damp churt, and felt all manner of flora brushing and scraping my leg.

This lasted for about 100 feet until, almost as if a switch had been turned off, it stopped. I was now on the road that I remembered. Covered in dead leaves, about 10 feet wide, and a clear direction to it. There was no evidence of recent travels, but it seemed that this part of the road had been preserved.

I smiled to myself and continued walking. Periodically I came upon a dead tree that crossed the road, a few required me to go under them, some around. I kept walking downhill, enjoying the present moment. The cool air on my face, the sounds of squirrels dropping acorns, the crunch of the leaves and twigs beneath my feet, and the sight of nature all around me. A few sips of water completed the engagement of all my senses.

After about 20 minutes, I came to a T in the road. To my left was a downhill slope which had a chain strung across it, tied off to two trees. To my right was a massive thicket of growing, thorny plants, fallen trees, and scraggly underbrush. Directly in front of me was one of the most gorgeous sights I’d seen all week.

The ground at the T intersection quickly sloped off. In my line of sight there were about 20 feet of trees partially blocking my view. The slope quickly carried the tops of the trees away from my field of view. Beyond those trees was a spacious valley which took my breath away with its expanse.

The valley was filled with a marshmallow-like cloud which reflected the sun back into the sky. It seemed to be radiating and shouting its own glory. On the other side of the valley was another part of the mountain, illuminated by the clouds, greeting the morning sun. I stood and stared at the scene, letting the reflected light hurt my eyes so that I could really _feel_ the moment. I was above the clouds and the sun was coming up to meet me. This was happening whether I was there to experience it or not, but here I was, some random primate, experiencing and embracing the natural beauty of the world.

Wanting to stay longer, I decided to find a seat. About 75 feet up the slope behind me and to my right were some rocks that seemed appealing. I climbed up and found one on which I could perch with my knees drawn up to around my chin. There I sat for what seemed like hours.

I meditated. I wrote in my notebook. I stared at the scenery. Completely alone with my thoughts and my feelings, I reflected on everything and nothing. Here’s a taste of what I wrote in my notebook:

“I am alone out here. There are sounds, but they do not intrude on my thoughts. I reflect and ponder, thinking about my life. Around me are signs for no trespassing and there is tape around trees. I stand on a rock in my shoes and jacket. I am alone. It is here that I would feel closer to a god, if I believed in one.”

About 20 minutes into my respite, I heard a more distinct noise coming from directly in front of me. It sounded like something was traipsing through the woods.

I nearly called out to it, but decided to just wait and see. Directly in front of me, about 100 feet away, a buck came up the slope and entered the clearing. For 10 minutes I watched as it sniffed the ground, listened to the random sounds, and finally walked back down the slope a few yards from where it came up.

The deer and I never made eye contact. I was completely still; a part of the scenery, watching and listening. Perhaps it did see me and viewed me as no threat. I wonder what it would make of such a creature? Black jacket, white shorts, pasty and hairy legs, with some weird looking finger-shoes on. The deer was very simple: 6-point antlers, brown fur coat, white tail. A part of nature, not wrapped in artificial material, living off what it could find and following its instinct.

After the deer left, I stayed for a bit longer, thinking on what I had just witnessed. It was such a blessing to be so close to a deer. Just me and him, doing our thing and not bothering one another. I was, and still am, grateful for that chance encounter. There’s something amazing about how close in proximity something so natural can be to such modernity.

Feeling like the time had come for me to leave the area, I walked back the way I’d came. On the way down there had been no evidence of the mine shaft. Perhaps it was covered by the brush, reclaimed even further my the earth. I kept a sharper eye for signs of it.

The walk back was slower, more deliberate. I was going uphill now and I was acutely aware of every step and sound I made. Not because I was struggling or tired, I simply had a heightened sense of everything I was doing.

For a brief moment, I thought I saw a sign of the old mine. A shifting shadow perhaps that my mind forced into the shape of a mechanical object. I went about 50 feet into the woods and found nothing but what was already there: dead trees and brush. Looking around, I saw the top of our friends house peeking through the trees. In that moment, I knew there was no way I would find the mine. Whatever I may remember or have forgotten about the mine, I knew it wasn’t this close to the start of the road.

Feeling a strange mix of elation and defeat, I traipsed back through the brush at the start of the road. My feet touched pavement again and walked back up the hill with more celerity. The pavement doesn’t call for slow, measured steps. And I had no reason to deign it with that effort.

Reflecting on the hike, I realized how different the experience would have been had I not pushed through the initial resistance. There were some good reasons not to push myself through the woods to get to the road. I was probably on somebody’s property. There was probably poison ivy in all that growth. The road was obscured and seemed to indicate no one should be on it.

But my memory wouldn’t allow me to give up. There was a road there and I knew it. I had to find it. I pushed through the difficulty and was rewarded with the simplest of gifts: nature itself.

I think there’s something to be said for going it alone sometimes and overcoming obstacles. No one will ever know exactly what you went through, even if you write about it and share it. Those moments are yours alone to have and hold. Great memories and experiences can be had when you’re with others, but it’s those times when we’re by ourselves that really give us an anchor on who we are, what we value, and how we live.

Not every difficult path is rewarded with greatness. But sometimes, you might just see a deer that makes it all worthwhile.

&nbsp;

_There’s this old path_

_I used to walk._

_Beautiful, serene, peaceful._

_But I haven’t walked_

_It in a while._

_It’s no longer mine._

_But it still has_

_Beauty, peace, and serenity._

&nbsp;

_Orange and blue ribbons._

_A lone, forgotten feather._

_Squirrels climbing high._

&nbsp;

_I used to know an old mine._

_Now I can’t find it._

_Lost among the trees and past._