---
title: Lessons learned managing the GitLab Data team
date: 2020-02-10T13:21:00+00:00
author: Taylor Murphy
layout: post
permalink: /lessons-learned-as-data-team-manager/
---

I wrote a post on the GitLab blog talking about my experience as the GitLab data team manager.

Read about it [here](https://about.gitlab.com/blog/2020/02/10/lessons-learned-as-data-team-manager/)
