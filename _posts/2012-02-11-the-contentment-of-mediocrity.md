---
id: 51
title: The Contentment of Mediocrity
date: 2012-02-11T09:10:07+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=51
permalink: /the-contentment-of-mediocrity/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
tags:
  - belief
  - dream
  - future
  - great
  - greatness
  - job
  - life
  - limit
  - mediocre
---
Somewhere along the way from birth until adulthood, we were conditioned to think that life is for some other purpose than living. We are told we have certain obligations that we are expected to meet. We are convinced that life is not useful if we don’t have a career. The expensive gadget, the biggest TV, and the best seats to the sporting game are what it’s all about.

People wind up pursing a dream that’s not really theirs. In the pursuit of this ideology, we lose so much of ourselves. We try to become something we’re not because we believe that’s what we’re supposed to want.

Many of us have the ‘well it could be worse’ mentality. We tell ourselves how fortunate we are to be in our current situation. We look at those less fortunate and say, well at least I’m doing better than that guy.

Then we look at people who we perceive to have more success than us, and envy comes over us. We yearn for the material possessions, the attention of strangers, and the prestige of wealth. But that’s not for us, we tell ourselves, we should just be happy with what we have.

Be thankful you have a job, after all, look at the economy. It could be worse.

This mentality is so disheartening to witness. It creates a limit where none should exist. Instead of being ignited by passion and taking a risk, we convince ourselves to do nothing lest we upset the safety net we’ve created for ourselves. By being content with a mediocre life, we prevent the possibility of something greater and fulfilling.

I believe it doesn’t have to be like that. There is another way. We can eschew these societal goals and instead live with passion. Finding fulfillment in the things that truly make us happy is the way to live. Releasing ourselves from the limitiations we’ve set up in the name of Security, can be scary. But that’s the only way to get to a better place. It may be a difficult experience, but if you want it bad enough, it’s only a [matter of time](http://www.tayloramurphy.com/blog/just-as-good-as-them/ "Just as Good as Them") before you break out of mediocrity and into greatness.