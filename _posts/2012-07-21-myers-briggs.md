---
id: 199
title: 'My Myers-Briggs Personality Type: ENTJ/P'
date: 2012-07-21T18:14:49+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=199
permalink: /myers-briggs/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Learning
  - Personal Development
tags:
  - careers
  - counseling
  - decision making
  - ENTJ
  - ENTP
  - grad school
  - leadership
  - learning
  - life
  - MBTI
  - myers-briggs
  - personality
  - playing
  - procrastination
  - psychology
  - stress
  - teamwork
  - work
  - writing
---
I recently had the opportunity to partake in some vocational counseling offered by my school. I’ve been seeing a counselor at the Psychological and Counseling Center (PCC) for over a year and one of the topics we recently discussed was anxiety that I was having about my future career choices after graduate school.

Like many grad students, my decision to go to grad school was partially motivated by a fear of the real world. Graduate school offers a way to ‘hide’ from the adult world and spend more time in school. Not all people do this, but I know I did. I was afraid that I wasn’t a good enough engineer so I made the decision to get more learned by earning my Ph.D.

Unfortunately, by not dealing with my fears and insecurities about life after school I’ve just postponed these difficult questions. That later date is fast approaching and I’ve now had to deal with these topics.

My counselor recommended that I take a vocational assessment. The assessment is, thankfully, offered at a significant discount to current students, but that doesn’t mean it’s value is diminished. This is a scientifically supported analysis of the skills, interests, personality traits, emotions, and history of a person that will help that individual learn more about themselves and make better informed decisions.

My assessment incorporated information from 5 different career-related instruments: Career Thoughts Inventory (CTI), Fundamental Interpersonal Relations Orientation (FIRO-B), Myers-Briggs Type Indicator (MBTI), Campbell Interest and Skill Survey (CTSS), and Work-Values Inventory (no cool acronym, sorry).

I spent some time today going over my assessment report and really thinking about what each part is telling me. I wanted to share each test individually to help others understand my own personality, to help me articulate my personality traits and the associated desires, and to encourage other people to undergo this type of assessment themselves.

The first test I wanted to go through is the Myers-Briggs Type Indicator. If you search for Myers-Briggs on Google, you’ll found dozens of resources that offer free testing to determine your type. There are 16 personality types within the MBTI and there are 4 different 2-letter pairs that describe a different aspect of each personality.

E/I &#8211; Extraversion / Intraversion &#8211; Do you prefer to be more outgoing or do you focus more on your inner world?

N/S &#8211; Intuition / Sensing &#8211; Do you rely more on your intuition or on your five senses when gathering information?

T/F &#8211; Thinking / Feeling &#8211; Do you appeal more to logic or are your emotions more important in decision making?

J/P &#8211; Judging / Perceiving &#8211; Do you prefer to have a plan and more structure or do you prefer more spontaneity?

These are very short descriptors of the personality basics and the internet has a font of information on these types. I want to walk you through my personality type and give you some detailed information from the reports.

My personality is ENTJ/P. When you take the test, each letter pair is scored. My E/I ratio was 21:0. This indicates a high degree of extraversion. My N/S ratio was 21/5 which is more moderate, but still heavily weighted towards intuition. The T/F ratio was 15/9 which is close to the middle, but I still tend to rely more on logic than emotion. My J/P ratio was 11/11 which is right in the middle indicating a shifting attitude toward structure and spontaneity.

The counselor who ran the test gave me the booklets for both ENTJ and ENTP based on my results. Reading through them, I definitely find myself fitting into both categories. Each packet is split into different sections, each of which I will go through and summarize here.

## **ENTJ/P and School**

### **Learning**

The pamphlets suggest that my personality type will often do the following:

  * Strive to be knowledgeable
  * Prefer to learn challenging or difficult material that stimulates their mind
  * Desire competent, intelligent, and creative instructors
  * Initiate and lead study groups
  * Complete unassigned reading on serious topics
  * Effectively learn through debating and discussing topics
  * General their own ideas through working alone

All of these traits I definitely see in myself. The fact that I’m getting a Ph.D. in Chemical Engineering and that my research topic is cancer metabolism is evidence of the accuracy of these traits. Note: I was never good at biology and once claimed that I hated it. Talk about a challenge.

### **Writing**

In the writing process, the ENTJ/P will often:

  * Need to limit the number of topics they consider
  * Easily connect patters that exist in their gathered information
  * Focus on the big picture and possibilities
  * Incorporate witty humor but should be cautious not to offend the reader
  * Break down a topic into sensible sections
  * Use a clear and logical writing style
  * Reluctantly request feedback or not use the feedback they receive
  * Quickly complete their initial drafts and benefit from revising and building on their original ideas

I resonate strongly with many of these traits. The fact that I’m breaking this article down into sections is evidence of how accurate this list is. I do use humor sometimes, but I try not to worry whether somebody is going to be offended or not. (Maybe I should starting giving more of a shit in that arena?) My first drafts have always been above average, at least in school, and because that work was acceptable I never spent much time on editing. This is something that I’ve realized over the years and now make a conscious effort to improve my writing through focused editing. Because one of my goals is to be an excellent writer, this information is very useful as it enables me to be aware of my tendencies and to adjust accordingly.

### **Procrastination**

Highlights from the pamphlets for both types:

“The ENTP’s pursuit of numerous ideas and projects can result in leaving some tasks unfinished or producing inadequate work. It is often difficult for them to create closure because they are pulled in many different directions and captivated by a variety of interests. These factors combine to make it difficult for the ENTP to focus their attention on any one area.

ENTPs will generally continue to procrastinate as long as possible and often work right up until the deadline. They typically do not see a need to modify their behavior because they are able to still complete their work and receive an acceptable outcome. They only recognize a need to change when their procrastination significantly affects their lives. It is important for the ENTP to learn how to evaluate the possibilities they identify and establish a list of what is important. It can also be beneficial for them to identify and examine the long-term impact of their choices.

To help reduce procrastination, the ENTJ often benefits from developing skills or restructuring the situation to acquire a greater sense of control. ENTJs may also benefit from addressing a particular issue and the aspects of a task that are daunting. This is often accomplished by discussing their concerns with others.”

I resonate a lot with the information that I highlighted. I find myself almost overwhelmed by the myriad of interests that I have as well as the inability to focus on one thing at a time. It’s also quite clear to me that I procrastinate when I believe myself to not be capable or skilled at a certain task. As much as I want to be a writer, I still have that negative belief that I completely suck and so I avoid it at all costs.

Being called out on my bad behaviors like this is eye-opening and motivating.

## **ENTJ/P and Career Exploration**

An ENTJ/P tends to find career satisfaction with careers that have the following characteristics:

  * Supports innovative thinking and new ways to complete tasks
  * Involves diverse and challenging tasks
  * Provides and opportunity to interact with a variety of competent individuals to complete tasks
  * Involves few restrictions or rules and promotes freedom and spontaneity
  * Fosters a fun, exciting, and action-oriented environment
  * Requires logically analyzing issues and creatively solving problems
  * Encourages assessing and improving the current organizational processes
  * Rewards individuals based on their knowledge and achievements
  * Provides and opportunity for development, promotion, and advancement

When exploring career options, an ENTJ/P will often:

  * Gather information by contacting people through networking
  * Logically evaluate their career options
  * Benefit from incorporating their values into a final career decision
  * Creatively develop their own careers
  * Struggle with making a decision and benefit from setting a deadline

Career exploration is, honestly, something that I’m just getting into. This information is helpful because it gives me some guidance on what methods might be most suitable for me. The pamphlets also list several careers in which ENTJ/Ps have reported high satisfaction, a few of which I’ll highlight here:

  * Marketing Manager
  * Business Consultant
  * Teacher: Science / Social Studies
  * Chemical Engineer (hmm&#8230;)
  * Family Physician
  * Systems Designer
  * Photographer
  * Advertising Director
  * Marketing Research
  * Venture Capitalist
  * Entrepreneur
  * Research Worker
  * Physician: Psychiatry
  * Engineer
  * Restaurant / Bar Owner
  * Inventor
  * Journalist
  * Actor
  * Literary Agent

This is definitely a shortened list of the career options they list. I highlighted the ones that I’m actually somewhat interested in (or currently doing&#8230;). I find it interesting that there is a high level of diversity in the careers. Very scientifically oriented jobs along with business and more creative options.

### **Job Search**

Also highlighted are some of the behaviors that an ENTJ/P will undertake during a job search. They will often:

  * Identify a company’s potential needs and position themselves accordingly
  * Need to examine the realities of a job in addition to the possibilities
  * Network with a large number of individuals
  * Benefit from patiently conducting their job search
  * Need to be tolerant of the job search details
  * Use their creativity to overcome obstacles

And during an interview and ENTJ/P will often:

  * Confidently respond to questions about their skills and abilities
  * Assume they know enough about the job and benefit from asking additional questions
  * Need to be cautious not to appear too aggressive
  * Convey their enthusiastic attitude
  * Benefit from ensuring that they listen and not assume what will be said

I’ve done several interviews in the past during my limited ‘job search’. I was really looking for temporary co-op assignments, but I felt very confident during the interviews and took a pretty logical approach to look at the places I wanted to work. In fact, I did so well during my co-op interview that the person in charge of hiring new co-ops invited me to sit in on some interviews to gain perspective from the other side of the table. I hope to do some more interviews in the future, but I will most likely be pursuing a non-traditional income-generating methods.

## **ENTJ/P and Work**

At work, the ENTJ/P will often:

  * Generate excitement and support for their ideas and visions
  * Excel at learning new techniques, skills, or information
  * Remain optimistic in spite of rejection
  * Desire freedom and autonomy to complete their tasks and take risks
  * Easily adapt to change and remain flexible
  * Creatively solve problems
  * Prefer working with competent coworkers that remain independent
  * Organize people, time, and resources to complete tasks in the most effective way
  * Complete a task on the first attempt and dislike making mistakes
  * Understand difficult topics or complex ideas
  * Desire challenging tasks and find routine to be boring
  * Accept feedback from individuals they perceive to be competent

At work, the ENTJ/P should be aware that they may:

  * Become frustrated with others who do not reflect their work styles
  * Focus too heavily on a task and neglect to communicate with other employees
  * Feel uncomfortable and uncertain of how to provide encouragement or positive feedback
  * Need to patiently allow people in positions of authority to make decisions
  * Need to be open to further exploring issues that have been decided
  * Benefit from creating a plan that supports their overall vision
  * Become easily distracted while working on a project
  * Struggle to complete uninteresting tasks
  * Benefit from developing their time management skills

All of these are spot on. The times that I have felt most fulfilled and accomplished at work have been when I’ve been in alignment with many of the first set of bullet points. The second set is painful to hear, but again if I’m being honest with myself, I recognize the truth in these claims.

### **Teamwork**

On a team, the ENTJ/P will often

  * Generate and clarify numerous ideas
  * Work to overcome obstacles
  * Challenge team members to excel and work beyond their specified roles
  * Provide fun, humor, and energy
  * Use logic instead of emotions to deal with interpersonal issues
  * Reliably complete all assigned tasks
  * Desire to work alongside other competent and dedicated individuals
  * Be interested in teamwork when it results in the efficient achievement of goals

On a team, the ENTJ/P should be aware that they may:

  * Need to limit the amount that they direct and control others
  * Benefit from further developing their interpersonal skills
  * Become frustrated with spending additional time gaining consensus from the group
  * Feel irritated with group members that are uncommitted, deviate from the goal, continue to discuss an area that has been decided, or inefficiently use their time and resources.
  * Gain greater support for an idea by providing a more detailed explanation
  * Need to be more decisive when working with others who desire closure

All of these are accurate as well. Especially the fourth bullet in the second set, I can recall several times when I have been frustrated by my fellow group members.

### **Leadership**

As a leader, the ENTJ/P will often:

  * Generate unique ideas and present a broad visionary picture
  * Expect others to reflect their work pace and should be cautious not to be unrealistic
  * Promote independence among team members
  * Need to intentionally identify and appreciate individual contributions
  * Use a direct and upfront management approach
  * Motivate people to meet the identified goals and objectives
  * Surround themselves with people who replicate their own strengths and weaknesses
  * Benefit from being open to hearing input from all individuals and considering others’ views

I generally consider myself to be a leader. I often find myself thrust into the leadership role without any effort on my part and seem to adjust well to it. I haven’t had as many leadership opportunities within a ‘traditional’ work environment, but the ones I have give credence to the truth in the above list.

## **ENTJ/P and Life**

### **Communication**

The ENTJ/P will often:

  * Present information in a clear manner that reflect their position of control
  * Enjoy discussing and debating ideas and issues
  * Convey information that highlights their competence
  * Directly acknowledge anything they deem to be wrong or inaccurate
  * Question others and inquire into how things work
  * Desire to hear interesting and captivating ideas
  * Excel at public speaking or presentations

The ENTJ/P should be aware that they may need to:

  * Ensure they provide genuine comments and positive feedback
  * Incorporate more detail into their explanations
  * Be cautious not to finish people’s sentences
  * Tailor their intensity to prevent appearing too blunt, aggressive, or argumentative
  * Seek out and accept the opinions and thoughts of others
  * Improve their active listening skills by ensuring they hear and understand the message

I find all of these to be very interesting and accurate. My decision to join Toastmasters at the beginning of the year was partially motivated by my desire to be a better communicator. I feel comfortable giving speeches, but I want to get better at it and potentially earn some money from it. I do have a tendency to finish other people’s sentences (sorry!) and I _know_ I need to improve my active listening skills. This is why I’ve signed up to be an Evaluator at one of the meetings in August so that I can improve this ability.

### **Decision Making**

When it comes to decision-making, the ENTJ/P will often:

  * Logically analyze their options and make rational decisions
  * Need to incorporate their values when evaluating their options
  * Limit the amount of time spent making a decision
  * Use decision-making to create closure on an issue
  * Prefer to spend a lot of time in the exploration stage
  * Put off making a decision

This list cuts right to the heart of what I’m going through. I’m constantly making decisions about what to do with my day / week and I find myself spending too long thinking about it. Especially with choices that will affect my life after grad school, I find myself putting them off and spending too much time exploring. I have a feeling it will be best to just make a decision and then correct course along the way.

### **Playing**

This is one of the few sections that doesn’t have a bulleted list. Certain aspects of the given description I felt were off the mark, but some were very accurate.

“The ENTP is often interested in being active and may have trouble slowing down and relaxing. At times, their desire for play can interfere with their focus on completing tasks. ENTP students also report that their dating typically occurs in a group setting and is often focused around some type of activity.

Although they may struggle with being present at home, the ENTJ prefers to maintain a sense of control in their personal lives. As a post-secondary student, they are often interested in attending athletic games and are drawn to leadership positions within groups on campus. As well, their competitive and driven work habits are reflected in their approach to any activity that they participate. They also desire or their roommates to reflect their own reliable, thorough, hardworking, and organized approach to life.&#8221;

I don&#8217;t have too much to say here except, well said.

### **Stress**

An ENTJ/P will often experience stress when:

  * feeling unable to deal with their emotions or the emotional expression of others
  * perceiving their lack of knowledge created a problem
  * Dealing with people who ignore established principles, exhibit irrational behaviors, or miss deadlines
  * Working within an unorganized environment
  * Working with individuals they perceive to be incompetent
  * Feeling unreasonably judged by others
  * Disrespected or deemed incompetent
  * Projects are perceived to be boring or uninteresting

When they are affected by stress, an ENTJ/P will often

  * Overdo their pursuits and excessively eat, sleep, or exercise
  * Increase their work intensity but reduce their effectiveness
  * Remove themselves from situations and avoid others
  * With signficant stress react with a strong temper or become critical and judgemental of themselves and their abilities

An ENTJ/P can reduce stress by:

  * Reaching out to others to receive support
  * Expressing their emotions through conversation or other creative outlets
  * Spending time on their own to reestablish their control
  * Participating in physical or recreational activities
  * Reevaluating their decisions
  * Delegating tasks and establishing a list of priorities
  * Establishing a clearer understanding of their values
  * Completing all elements of a project

This is the section that I resonated with the most. The negative times have a way of sticking out in my mind and I can point to specific instances when I have exhibited these behavior. The recommendations on how to reduce stress are dead on as well. I’ve tried all of these at various times without quite articulating what I was doing. But I knew that they worked.

## **Overview**

Whew! That’s a lot of information to digest. If you read through all of that, congratulations, you now know what an ENTJ/P is like. Or, more appropriately, you have a better idea of what I’m like.

The categories ENTJ and ENTP are not hard and fast rules that dictate everything I do. At times, I will exhibit traits of all the personality types. These descriptions are meant to be a guide to help me understand how I act the majority of the time and guide me in making decisions based upon my strengths, weaknesses, and preferences.

I would encourage everyone to take some sort of MBTI assessment, even if it’s one of the online versions. The ones I’ve taken online have always turned up as ENTJ, so there has to be some inherent accuracy.

Learning more about yourself is an empowering way to help you live a better life. Knowledge is power, especially knowledge about yourself.

**_If you know your MBTI type, I’d love for you to share it in the comments. If you have any questions or want to learn more about the specifics of my personality quirks, please don’t hesitate to ask. _**