---
id: 58
title: Where are you in the bell curve?
date: 2012-02-17T09:11:39+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=58
permalink: /where-are-you-in-the-bell-curve/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
tags:
  - challenge
  - comfort
  - hate
  - life
  - love
  - mediocrity
  - tolerate
  - work
---
**People have three options for how they feel about their work: either they love it, they hate it, or they tolerate.** I would expect that if you plotte this out, it would be a bell curve (or normally distributed curve for you nerdy types). Most of the people are going to be in the middle with a few on either end. On one end is loving your work. If you love your work, that’s great. Keep on trucking. The other end is the people that hate their work. If you hate it, that’s great too. Why? Because it gives you the motivation necessary to make dramatic changes in life.

But in the middle. That&#8217;s where people just tolerate their work. And toleration of your job is the worst.

**Most people don’t hate their job**. Most people don’t love it either, but they don’t outright hate it with a passion. The individuals that tolerate their job probably have kind of good days and sort of bad days. They have probably made some friends at work and maybe some of the customers are pleasant to be around.

These workers are very comfortable in their life. They have a place in which they trade their time for money and which they feel relatively confident will last for quite some time. The money they earn during the week is spent during the evenings or weekends to help them de-stress from their work. Again, they don’t have to go crazy partying because they only have this general malaise about their career. Tame work leads to tame partying leads to a bland and mediocre life.

All of this combines to make that person very unhappy. But they won’t know why they’re unhappy. They have a job that gives them a decent salary. Maybe they have some benefits. They should be happy with their position, because after all, their just so lucky to have a job in this economy.

**This mentality is slowing killing them.**

At least if you work in a job that you absolutely hate, you will be more inclined to do something about it. When you’ve come to tolerate your work, you’ve agreed to become accustomed to mediocrity and some level of misery.

Imagine you’re driving your car down a city street in the middle of the summer. You crank up the air conditioning but out comes a few gnats with the cold air. They immediately aim for the warmest thing near them which, it so happens, is you. The only things these gnats do is fly around your head and periodically bit your skin. You consider turning off the A/C so they’ll stop coming out, but it feels so good. You convince yourself that the slight annoyance and dull ache you feel is offset by the cool air.

That cool air is the money you earn and those gnats are your crappy job. You tolerate the gnats to get something you believe is worth the suffering. All you really need to do is open the window and things will start to get better.

Small annoyances and subtle mediocrities have a way of sneaking up on a person and compounding on each other to make a person miserable. Over time, you become more unhappy and have this ennui that you can’t explain.

There’s only one way out of this. You have to challenge yourself. You have to constantly expose yourself to new experiences and challenges. Only by seeing what others do with their lives and what’s outside your narrow world view can you being to see what you life is truly like. Chances are you won’t like what you see and will be motivated to do something about it.

Your comfort zone is not where you want to be. Nothing great has ever been done by staying in a comfort zone. Push your boundaries, put yourself out there, and shake of the chains of your comfortable life. Don’t just tolerate living. Embrace it fully and make it what you want it to be. You’ll never regret trying something new, but you’ll always regret not trying it.

**_What are you currently comfortable with in your life? How could you make a change that would shake things up?_**

P.S. I’ve got some uncomfortable experiments that I’m going to be trying in the next month or so. Stay tuned for some craziness.

&nbsp;