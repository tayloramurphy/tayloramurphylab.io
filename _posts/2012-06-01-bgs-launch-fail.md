---
id: 157
title: 'Why I&#8217;m Not Launching Better Grad Student Today'
date: 2012-06-01T13:47:48+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=157
permalink: /bgs-launch-fail/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beginning
  - Future
---
Several weeks ago I had made a [declaration](http://tayloramurphy.com/getting-started-is-scary-as-hell-but-it-needs-to-happen/ "Getting started is scary as hell, but it needs to happen.") that I was going to relaunch Better Grad Student on June 1st. If you were go to the domain [bettergradstudent.com](http://bettergradstudent.com) you would find that there is nothing there. I’m having to push back the launch and I wanted to be clear about why.

When I originally made the commitment, I knew it was going to be a stretch to actually launch on that day. There was a lot coming up for me but I wanted to challenge myself and see if I could do it. Things didn’t work out like I had hoped and the plans have to be changed as a result.

However, I did make progress on getting it ready. I wrote a few articles, I designed a manifesto for email captures, and I’ve got several ideas and articles in the pipeline that I’m working on. The real crux of why it’s not launching is money. While a lot of blogs will tell you that it’s _super cheap_ to start a blog, and it is on a monthly basis, in reality the hosting provider I work with requires the full amount up front. $6 a month x 24 months adds up quick.

In addition to that, I’m trying to get a new stream of income by finding a roommate. But before that could happen I had to do some maintenance on the guest bedroom.

## Excuses, excuses.

I know, I know.

<div id="attachment_158" style="width: 610px" class="wp-caption aligncenter">
  <a href="http://tayloramurphy.com/blog/wp-content/uploads/2012/06/882193732_8c8f680bc3_b.jpg"><img class=" wp-image-158  " title="Launching Rocket" src="http://tayloramurphy.com/blog/wp-content/uploads/2012/06/882193732_8c8f680bc3_b-300x208.jpg" alt="" width="600" height="416" /></a>
  
  <p class="wp-caption-text">
    Photo credit: jurvetson (Creative Commons)
  </p>
</div>

All of these, however, really aren’t major stumbling blocks. Even the money isn’t as much of a problem; had I skipped going out for a few meals I would have the money. What’s really holding me back is myself. The fear of the unknown.

Being afraid of the unknown truly is the basis of all fears. This post by Sean Ogle at [Location 180](http://www.seanogle.com/lifestyle-design/american-dream) sums up this idea succintly. Essentially, all fears can be boiled down to not knowing the outcome of something. When we don’t know, we start imagining and sometimes we’re not our best friend when we start imagining the unknown.

A myriad of thoughts rattle through our skull like dice in a yahtzee game. We scare ourselves into being afraid of failure. Of success. Of strangers. Of money. All of these things are based on the unknown factor.

_What if I fail?_

_What if I succeed?_

_What if people I don’t know start reading my stuff?_

_What if I don’t have money? Or too much?_

It’s this sort of fear that’s driven me to push back the launch date. Sure there might be some seemingly legitimate excuses up there, but in reality, I didn’t want it bad enough this month. I chose the easy route more often than the tough one.

And it’s the tough route that produces results. No one can get from point A to point B without putting in a ton of hard work. It may seem like it happens to other people without that work, but the truth is that you just didn’t see the effort they put in.

I’m self-aware enough to know that I didn’t put in the work this month to reach my goals. It’s tough, and it takes time, but the alternative is much worse. The pain of regret is much worse than the pain of discipline.

## So what am I going to do?

I’m going to pick a new launch date: July 1. This gives me one month to really push towards my goal. The first step is launching the blog. The perfectionist in me wants it to be, well, perfect, but I know that it’s better to get started and correct course along the way.

Here are the specific tasks that need to be completed to launch this blog:

  * Domain hosting
  * Theme and basic logo design
  * 10 articles written and ready to go (20 would be better)
  * Manifesto for email capture
  * Website copy

That’s it. These are the basic things that I have to do. The manifesto is nearly finished. I have a few articles already written. The theme will be relatively simple and the logo isn’t anything complex. Getting a couple bucks for the domain hosting shouldn’t be too much of a problem here.

These aren’t difficult obstacles to overcome at all. The biggest problem is getting out of my own way. It’s tough to do the work when the motivation, passion, muse, or whatever you want to call it isn’t dropping inspirational plates on my ass. But the work still has to get done.

This is my public declaration: If I don’t launch Better Grad Student by July 1, then I will donate $50 to a local charity.

The psychological factor of tying a monetary ‘punishment’ to this goal will be an exceptionally motivating factor. Either way, whether my goal is met or not, some good will come of it. I’d rather launch the site and then donate when I’m more able to, but at least value will be added in some way.

**It’s time to do the work. The only barrier is my own inertia. Forget the passion and be [convicted of doing](http://www.iamconvicted.com/dont-count-on-passion).**