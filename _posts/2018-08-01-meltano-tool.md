---
title: Introducing Meltano - A Data Science Lifecycle Tool
date: 2018-08-01T13:21:00+00:00
author: Taylor Murphy
layout: post
permalink: /meltano-introduction/
---

I co-authored a post introducing the Meltano project that's being worked on by a separate team within GitLab.

Read about it [here](https://about.gitlab.com/blog/2018/08/01/hey-data-teams-we-are-working-on-a-tool-just-for-you/)
