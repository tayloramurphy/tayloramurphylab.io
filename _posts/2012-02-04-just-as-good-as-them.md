---
id: 41
title: Just as Good as Them
date: 2012-02-04T17:06:54+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=41
permalink: /just-as-good-as-them/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Dreams
tags:
  - "10000"
  - dreams
  - hours
  - success
  - time
  - work
---
A common symptom among the newly motivated is the thought that success should be immediately theirs. After spending a few weeks working, they become frustrated at the realization that they don’t have the success they want. The motivation starts to fade and they’re faced with the drudgery of actually doing the hard work.

A sentiment that might be uttered by these neophytes is, “But I’m just as good as such and such famous person! Why aren’t people looking at me?” They are flabbergasted as to why the world hasn’t turned to face them just because they’ve decided that they’re going to work hard for three or four weeks. After all, that should be long enough for them to see the results that they want, right?

## **The Unseen Element**

Some of these people deserve the benefit of the doubt. Perhaps they really are just as good as other successful individuals in their field. Maybe their writing/art/film/science really is of the highest quality. So why aren’t they being seen?

The difference between Kobe Bryant and the kid shooting hoops down the street is this: _**time**_. The number of days and hours that highly successful people have poured into their craft is beyond counting. Truly successful people have devoted a vast amount of their life to a specific task or skill and have become masters at it.

**10000 hours is required to become a master**. I doubt it’s exactly this number, but nobody can say that if you practice at something for that long that you’re not an expert. That time has been spent honing their craft and producing quality content.

Built into every success you see, whether it’s the printed book or the championship ring, is the hours of toiling when nobody was around. These people worked when they least wanted to, because they knew that the pain they were experiencing know was less than they pain they would feel if they didn’t succeed.

## **Fifth State of Matter**

It’s only a matter of time. That’s the attitude that you need to have. If you want any sort of success in this life, then you have to believe that it’s only a matter of time. It’s a matter of how long you’re willing to work for it. It’s a matter of how smart you’re willing to be. No amount of shortcuts will make up for the effort and struggle that you pour into your dreams.

**Make the time for you dreams, and they _will_ become reality.**

_I’ve been guilty in the past of not putting the time in to work on my dreams. What have you changed in your life to make the time to bring your dreams to fruition?_