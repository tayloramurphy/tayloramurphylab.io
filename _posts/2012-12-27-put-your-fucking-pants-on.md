---
id: 271
title: Put Your Fucking Pants On
date: 2012-12-27T22:33:35+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=271
permalink: /put-your-fucking-pants-on/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Personal Development
tags:
  - distraction
  - epic shit
  - lazy
  - pants
  - procrastination
  - reddit
  - sleeping
---
The title sentence has become my new morning mantra. Wake up. Stand up. Put on pants.

Lately, my mornings have been fraught with laziness and distraction. I’ll wake up and immediately check email, Twitter, Words with Friends, and anything else I can think of to avoid getting out of bed. This starts a cascade where I’ll choose to do the easy thing instead of what I truly want to be doing. I won’t get dressed, I won’t write, I won’t do anything productive. In short, I don&#8217;t live.

After wasting too much time, I start falling into a crappy mood. The energy required to start my day has become too onerous to expend and so I languish in the land of procrastination, particularly on Isla de Internet.

There are some days, however, where I get up and immediately start getting shit done. My whole day is infinitely more productive because of how it started and, at the end of the day, I’m happier and more contented. The difference? You guessed it: I put my fucking pants on.

When I make the simple effort to get dressed soon after waking, it changes my whole day. Putting my work clothes on immediately alters my mindset. When you’re wearing PJ’s, it brings to mind feelings of relaxation. People wear pajamas when they want to chill and go to bed, not when they want to get shit done. Day clothes (i.e. jeans and shoes) trigger the working and productive mindset.

It’s this state that I want to be in more often. In the moment, procrastination feels great. It really does. You’re laughing along to YouTube videos, commenting on funny Facebook or Reddit posts, or you’re simply sleeping. Whatever it is, I totally get that it feels great _right then_. But I know all too well the hangover that comes afterwards.

You feel like shit because you know you could’ve done better. You think about what _could’ve_ been done and compare it to what was _actually_ done and the two are worlds apart. It’s a rough feeling and it can drop you into a depressed state of mind quickly. That’s why it’s so important to nip it in the bud and do something, anything, to trigger yourself to be productive. For me, it’s putting on my fucking pants. For you, maybe it’s making breakfast, meditation, or yoga. Whatever it is, do that one thing that’s going to get you into the mood you _know_ you want to be in.

Just do it. Put your fucking pants on.