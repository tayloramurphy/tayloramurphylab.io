---
id: 223
title: Good Without God
date: 2012-10-09T20:17:38+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=223
permalink: /good-without-god/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Future
  - Learning
  - Personal Development
  - Relationships
tags:
  - adding value
  - atheism
  - honesty
  - minimalism
  - personal development
  - religion
  - responsibility
  - self-actualization
  - self-awareness
  - writing
---
I recently finished reading _Good Without God_ by Greg Epstein. About halfway through I came across a section that resonated deeply with me and I wanted to share it. These words come from Sherwin Wine, a friend of the author. He is speaking on what it means to cultivate dignity.

> The first [principle] is high self-awareness, a heightened sense of personal identity and individual reality. The second is the willingness to assume responsibility for one’s own and to avoid surrendering that responsibility to any other person or institution. The third is a refusal to find one’s identity in any possession. The fourth is the sense that one’s behavior is worthy of imitation by others.

He goes on to add that with these four characteristics comes three moral obligations.

> I have a moral obligation to strive for greater mastery and control over my own life. I have a moral obligation to be reliable and trustworthy. And I have a moral obligation to be generous.

Reading these passages was a confirmation of the philosophy that I’ve slowly come to choose for myself. Each of these principles and moral obligations I’ve brought into my life outside of a religious mindset and independent of a pre-defined philosophy.

## 

## **Principles**

**Self-awareness**

The first step of this process was becoming aware of the actions I was choosing every day. For most of my life I lived unconsciously. I knew what I was doing, but I wasn’t thinking through everything and I didn’t understand the motivations and desires that drove many of my choices. Reading _No More Mr. Nice Guy_ by Robert Glover and going through a rough spot in my marriage led me to become more aware.

**Responsibility**

With that awareness came a desire to take more responsibility for my life. At the time, I was ostensibly a very responsible person. I had a good job (ok, _grad school_), a house, a wife, two cars, and a career path that looked successful. On the outside it looked good. But on the inside it was turmoil. My thought patterns and behaviors were causing unhappiness in my life and those closest to me. By being selfish and playing the victim, I brought more negativity and hurt into my world. When I finally realized that my happiness is my responsibility, and mine alone, I stepped up and started self-actualizing: started becoming the man I could be.

**Minimalism**

During this journey, I became very interested in the concept of minimalism. I’d always had a vague notion that the materialistic message shoved down our throats in the media wasn’t the best one. Upon discovering the term minimalism, I fell in love with it. The idea is that you dis-identify yourself from your possessions and only allow things into your life that add value. I dove head first into this concept and am still on that journey. A greater sense of peace and contentment has come into my life because I have what I need and all the things in my life add value.

**Writing**

Upon discovering these ideas and incorporating them into my life, I also had a great urge to share them with the world. Initially, I had some vague idea about wanting to create some sort of art, but I quickly discovered writing was a true passion of mine and I’ve been working on my craft ever since. Writing is a way to share my message and story with the world and it’s something with which I hope to create a living.

&nbsp;

## **Obligations**

**Personal Development**

Since my becoming a more self-aware person and taking more responsibility for my life, it’s only natural that I would become very interested in self-actualization. Whether becoming vegan, lifting weights, or doing other random 30 day experiments, I enjoy testing whether or not something improves my life. I’ve incorporated many of these experiments into my daily living and have found the my quality of life has vastly improved. Personal development, I believe, can be a lifelong journey and I don’t have any intentions of ceasing my growth.

**Honesty**

My need for a great amount of honesty in life was formed around the same time I became self-aware. I didn’t go around telling lies to people prior to that time, so you could argue that I was an honest man. But I was being dishonest with myself and dishonest with others by not sharing and communicating my needs and desires. Not saying what’s on your mind can be as dishonest as telling a bald-faced lie. I value extreme honesty even if does take concerted, daily effort on my part.

**Adding Value**

Over the last year, I’ve read a massive number of blogs and books. One idea that keeps popping up over and over is the concept of adding value. Whether it was a blog or book devoted to business, dating, writing, marketing, or whatever, the idea that should you be adding as much value as you can was a recurring theme. I fell in love with this idea because it helped move me away from the tit-for-tat mentality that I had previously. I fully believe that by adding as much value as I can to those around me, whether it’s through writing, friendship, intimacy, or even cash, my life and the world around me will improve. Now, the majority of my actions are prefaced by an inner discussion: “Is this going to add value to the world in some way?” If so, do it. If not, reconsider.

&nbsp;

## **Atheism**

Finally, all of these are deeply intertwined with my unbelief in all supernatural deities. As a result of taking responsibility for my own life and not blaming God for my circumstances, I started seeing how unnecessary the concept of God even was. I gave myself permission to consider whether or not I actually believed in a god. For the longest time, my brain was short-circuited into thinking their was only one option: believe in God or else you go to Hell.  When I became honest about how I felt about religious services and beliefs, I found it easier to move away from them. Once I was fully self-aware about my actions, I came to understand how I never chose my faith, it was forced upon at birth. Given the option, it made no sense to pick Catholicism over any other religion. I just happened to be born into a Catholic family. It finally culminated one day in September of 2011 when I said out loud, “I am an atheist. I can no longer believe.”

&nbsp;

## **Where to?**

My life has changed drastically since that announcement. For a time it got worse. My marriage ended and I lost many friends and connections. (I actually installed a Facebook app that told me when people unfriended me. It’s kind of sadistic, but I find it fascinating when people unfriend and I wish I could ask why just to see where their coming from. What finally got them past the tipping point to click “unfriend”?) But the amazing thing about stepping more fully into who I actually am and being more confident in my beliefs and actions is that I am now surrounded by people who accept, and support, me for completely. It’s to the point now that when people unfriend me I see it as a positive event: one less person who is not aligned with me makes more room for people that are.

There were some dark days where I wondered if it would have been easier to just lie and pretend I was back to who I used to be. But I knew deep inside that I would never be satisfied with that for very long. That sort of behavior, of trying to please everybody all the time, made me miserable inside and hurt a lot of folks around me.

Towards the middle of this year, things started really looking up. Each day I grew more confident in every aspect of my life. I surrounded myself with amazing people who supported me and I started doing things that added true value to my life. This blog is just one example of that.

My journey forward from here is a mixture of certain and uncertain paths. I know that I will finish my PhD and leave Nashville. I have plans to land a job as an online adjunct teacher and start traveling the world. More vague are my plans to turn this blog into a personal development business to help people that are in similar situations like I was. And there are even more vague plans to become a freelance writer and speaker.

There’s a lot of uncertainty but I absolutely love it, even if somedays are fraught with anxiety. I know I’m on the right path for my life because it’s the one I’ve chosen. It wasn’t picked for me by anyone else and I can rightfully call it my own.

I’m completely in line with my own personal values and ethics and I’m working towards living the exact life I want. It’s amazing what happens in just one year. I can’t wait to see where I’ll be next.