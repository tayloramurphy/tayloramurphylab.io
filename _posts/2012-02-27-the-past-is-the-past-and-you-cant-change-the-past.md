---
id: 70
title: The past is the past and you can’t change the past.
date: 2012-02-27T08:26:03+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=70
permalink: /the-past-is-the-past-and-you-cant-change-the-past/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Relationships
tags:
  - belief
  - connect
  - date
  - friend
  - friendship
  - future
  - history
  - past
  - present
---
I recently reconnected with an old friend of mine; a blast from the past if you will. We had dated over 5 years ago and had barely spoken to each other since. She contacted me randomly offering words of support and from there we started talking more and we came to the same conclusion about our past:

> The past is the past and it’s a part of who we are, but it doesn’t control our future.

We see the people we were back then and recognize that we’re not the same people now. We’ve both grown and we’re able to look back on our history through a lens of maturity.

Time, the great healer of all wounds, has allowed us to comb through our history with some detachment. Whether we’ve forgotten the details, or just moved passed some of the issues, the specific events of our past hold no sway over us now. We’re able to discuss our previous actions and emotions without the heavy baggage associated with those memories.

Having a shared history with somebody can be a powerful connector. It creates a commonality that is hard to break. Even after 5 years of silence, the shared stories reformed our bonds of friendship quickly. This is why people like Leo Babauta and The Minimalists recommend sharing some part of your past with someone whom you’re trying to establish a friendship. Sharing some event or emotion is a powerful way to make strong connections.

Those connections are powerful and can be used to strengthen a relationship as it goes forward. We acknowledge the past because it’s unchanging and a part of who we are, but we choose to let it affect us only in ways that are beneficial. Difficult times are now somewhat humorous because hindsight allows us to see our follies. Each memory is shared with a quizzical, “Hey, do you remember?” Through this discussion a re-education occurs about who the person is now and how the past has shaped the present.

The past can’t be changed, and the future is unknown, but how we deal with the past in moving to the future says a lot about who we are in the present. We can allow the past to control us, or we can rise above it, acknowledge it, and move forward establishing new connections, bonds, and experiences.

_Have you reconnected with someone in the past? How has that relationship shaped who you are today?_