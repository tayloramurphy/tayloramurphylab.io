---
id: 482
title: Achtung Baby
date: 2018-03-26T07:49:37+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=482
permalink: /achtung-baby/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Learning
---
<span style="font-weight: 400;">This past week I’ve been reading through a book called “</span>[<span style="font-weight: 400;">Achtung Baby: An American Mom on the German Art of Raising Self-Reliant Children</span>](https://smile.amazon.com/Achtung-Baby-American-Self-Reliant-Children/dp/1250160170)<span style="font-weight: 400;">”. It’s been an excellent, quick read so far. One of the best things about reading books is the way they can challenge your view point. I wanted to take a moment and highlight some ideas that have stuck with me from the book.</span>

<span style="font-weight: 400;">Children should be exposed to other children and adults early and often. In America, there’s this undercurrent of belief that if one of the parents isn’t with the child at all times (usually the mother) that you’re bad parents. But in other cultures, it’s seen as a good thing to expose your child to the rest of the world. It’s a good thing to take your child to daycare because it allows them to interact with other children and learn from them and other adults. Child care in Germany isn’t a bad, expensive thing. It’s subsidized by the government and encouraged for all children. </span>

<span style="font-weight: 400;">Homeschooling is illegal in Germany. This, to me, was a bit shocking, but the justification for it is that you, as a parent, do not have the right to deprive your child of societal contact. That bit of reasoning was a good antidote to my initial reaction of the illegality of this. Of course it’s not illegal in the United States, but I know the harm that homeschooling in its most pure form can do in terms of isolation and development. Were I to try and effect a policy change around this I think I would fall somewhere towards the middle &#8211; not illegal but stronger assurances that children are bumping into the real world.</span>

<span style="font-weight: 400;">Children are allowed to walk freely, even in large cities. While reading, I was shocked to feel my own reaction to the following situation: Imagine you have a child of 9 or 10 years old. They are smart, self-reliant, and can be trusted on their own. You live less than a mile from a grocery store and your child wants to get some ice cream from the store. Would you feel comfortable letting your child walk to the grocery store, money in hand, to make the purchase?</span>

<span style="font-weight: 400;">I have mixed feelings about this. I would be nervous for my child, but I would trust them to make the trip. What I would be more worried of, however, is how other people would react. Would some busybody call the police on my child? Would the police then take my child and possibly arrest me? Would I be reported to child services for “endangering my child”? I’m less worried about their safety and more worried about the possible societal consequences of an overprotective and reactive culture. I suspect as I learn more about it I’ll be involved with the “free range parenting” movement, if only to have a source of knowledge and support. </span>

<span style="font-weight: 400;">This worry extends to my own person. We have such strong property rights that it’s almost weird to be walking in places that aren’t yours, even if they’re technically public. Recently on vacation, Lindsay and I were looking for a hiking spot but couldn’t find it. We started walking on a road but realized that we were on somebody’s property. Out of fear of discovery (and possibly accusations of trespassing), we left quickly. This made me sad because other countries have a true “</span>[<span style="font-weight: 400;">freedom to roam</span>](https://en.wikipedia.org/wiki/Freedom_to_roam)<span style="font-weight: 400;">” even on other people’s property. </span>

<span style="font-weight: 400;">Young children in other countries, as young as 6 or 7, are exposed to topics like sexuality, gender identity, and death. These are heavy topics that are discussed out in the open. American Puritan heritage creates a terrible culture of body-shaming and repression. Ironically, this creates more of the behavior such Puritan ideals would like to minimize. It’s toxic and completely at odds with how we humans exist naturally.</span>

<span style="font-weight: 400;">Risk tolerance in other countries is higher. Playgrounds are more dangerous and it’s expected that children will take risks. The reality is that there are risks everywhere and learning how to manage those risks and knowing what your limits are is an important part of growing up. Our litigious-happy culture creates an environment where all the rough edges have been smoothed. This stunts children’s growth and causes them to misjudge the actual risk of an activity, potentially causing more harm in the long-run. </span>

<span style="font-weight: 400;">Overall, this book has given me a lot to think about. I realize I’m painting Germany, and some other countries, with a broad brush and that there is a wide variety of belief and behavior in those countries. And I also realize I’m sharing second-hand knowledge. But the value I’m getting from this book is a challenge to some deep parts of my culture and it’s good to face them by shining a light on them. I’m sure my thoughts will change and grow as my kid grows &#8211; it’s one thing to read about it and another to have one of your own!</span>