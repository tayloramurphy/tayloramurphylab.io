---
id: 265
title: Grit and Commitment
date: 2012-12-08T22:36:05+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=265
permalink: /grit-and-commitment/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Personal Development
tags:
  - 642 things
  - commitment
  - grad school
  - grit
  - mark manson
  - marriage
  - science
  - tim ferriss
  - work
  - work ethic
  - writing
---
This past week has been a change of pace for me. It’s been a good change and one that’s helped me to realize a few things.

For the whole month of December I haven’t written my daily stories. (For those of you who have no clue what I’m talking about I’ve been following a book of writing prompts titled “[642 Things to Write About](http://www.amazon.com/Things-Journal-Francisco-Writers-Grotto/dp/1452105448/ref=sr_1_1?s=books&ie=UTF8&qid=1355027746&sr=1-1&keywords=642+things)”. I write 2 stories a day as a way to deliberately practice the craft and to get the juices flowing.) I stopped for one main reason: I was a few days behind and it started to feel like homework and I began to dread actually sitting down.

After three days of not writing for 642 Things, but still writing in general, I “officially” decided to take a break from the project and consider returning to it at a later date. A few days passed and I’ve come to the decision that yes, I do want to continue with this project.

Among the many things I read and listen to is a podcast called ‘[Blogcast FM](http://blogcastfm.com/)’. The host, Srinivas Rao, interviews prominent entrepreneurs and has recently posted episodes with [Tim Ferriss](http://www.fourhourworkweek.com/blog/), [Jonathan Fields](http://www.jonathanfields.com/blog/), [Danielle LaPorte](http://www.daniellelaporte.com/), and [Mark Manson](http://postmasculine.com/). If you don’t recognize those names, don’t worry, they’re “internet famous”. At the end of every episode Srini has been asking each guest what it is that makes someone successful. What separates those who are killing it versus those who linger in obscurity?

Two responses were repeated by different individuals so much that I finally took notice and began to “get it”. What makes people successful? Grit and extraordinary commitment. You could accuse me of being tautologous with the previous sentence, but in my mind they’re different concepts.

Grit, in my mind, is the resolve and determination required to push through difficult times. It’s the strength of character necessary to focus and do the work needed to make shit happen. People who have grit are tough, determined, and willing to sacrifice.

Commitment, in this context, is the willingness to stay with something, be it a project or business, well past the point that other people would give up. It’s the ability to see past what [Seth Godin](http://www.sethgodin.com/sg/) calls “[The Dip](http://www.amazon.com/The-Dip-Little-Teaches-Stick/dp/1591841666/ref=sr_1_1?ie=UTF8&qid=1355027718&sr=8-1&keywords=the+dip)” and stay with it to get to the other side. This ability does require grit, but more than that it requires the belief that things will get better if enough effort is poured into the endeavor.

Grit gets you through the little challenges. Commitment gets you through the big ones.

At some point while listening to a myriad of podcasts, I finally internalized these concepts and realized something mildly disheartening: my level of grit and commitment in certain areas of my life are lacking.

Graduate school has been good for building up my tolerance levels for difficult tasks. Sticking with a research project for four years has been a roller coaster ride of good and bad times. Being somewhat trapped in grad school has forced me to learn how to foster my work ethic (aka grit) and taught me the value of committing to a difficult project. When an experiment doesn’t work it’s grit that helps me figure out how to make it work. When I have a panic attack about how much I have to do in such a short amount of time, it’s the commitment to getting my PhD that keeps me going. My work isn’t easily abandoned and I’ve been through many dips in the past four years that have moulded me into a more mature and grittier man.

[Sidebar: I can imagine many people would now ask, okay, if you can commit to graduate school why couldn’t you commit to your marriage? While this is a seemingly valid question, you have to realize that these are wholly different situations. The relationship between two separate, thinking, feeling entities is completely different than the one between a person and their work. Perhaps this is a discussion for another time but I hope that you, dear reader, would have the presence of mind to understand the differences in these situations.]

The 642 Things project has also taught me several lessons. In some ways, I feel like this project is a microcosm of my future writing and entrepreneurial endeavors. If I’m unwilling to stick with a simple project lasting nearly a year, how would I fare in writing a blog or starting a business which might require 3 to 5 years before it becomes successful? Once the luster of newness fades, will I have the strength of will, rather, the _grit_ and the _commitment,_ to see it through to success?

The short hiatus I’ve taken from the 642 Things project has been good because it’s helped me to realize I do miss the work. I miss the release of creative energy from posting new stories. The sense of accomplishment that comes from daily achieving something I’ve set out to do. It’s a win for my ego. I have been writing while not working on 642 Things and, in some ways, the hiatus has been good because I’ve had more time to write what I want. But the project was better for stretching my writing abilities than just brain-dumping into a pseudo-journal.

On Monday I plan to start back into the project and see it through to the end. I’m just over 1/3 of the way through (111 out of 321 days) and have already created some awesome stories (at least in my humble opinion). I think when I get to 2/3 completed (214 days) it would be good to take another break so I can rest and recoup before the final push to the end. Vacation time is needed, even with something you’re committed to.

I know when I write the 642<sup>nd</sup> story that I’ll have done something awesome. Is it going to change the world? Definitely not. But it’ll be a sign for me that I’m able to stick with something from start to finish that _I_ chose to do. I’m the one who sits down every day and cranks out the words. The support from my friends is, of course, absolutely essential and has been great since I started. (My best friend regularly gives me a verbal ass-kicking when I haven’t written for a while.) Almost as good is the random words of encouragement I’ve received from people who I didn’t even know were reading my work. Hearing that what I’ve been doing is “cool” and “inspiring” gives me a temporary boost to get past the difficult point and bring it back to a more enjoyable level.

Graduate school and writing have been two of the best experiences for improving my grit and commitment. In the future, I know I’ll be able to see things through when it’s tough because I’ll have done it already. That won’t necessarily make it easier, but I’ll have the experience, and hopefully the relationships, to carry me to the other side.

Grit and commitment: the difference between success and failure.