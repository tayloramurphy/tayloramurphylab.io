---
id: 244
title: 'Bills and Pain: The Same Thing Twice'
date: 2012-11-18T21:47:58+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=244
permalink: /bills-and-pain/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Future
tags:
  - bills
  - frustration
  - healthcare
  - medicine
  - pain
  - rant
  - vent
  - wrist
---
I’m sitting at my desk writing this with my left wrist wrapped in a bandage. Periodic jolts of pain pierce the area between my palm and forearm causing me to make micro-adjustments to my positioning to avoid future strikes. But they come again no matter what.

It’s fitting I should be experiencing this right now. The injury is from a previous bike wreck where the initial point of contact of my body with the ground was the base of my left hand. I had swerved hard to avoid being hit by an inattentive driver and I wiped out immediately after passing the vehicle. Bruised and bloodied, I walked my bike the rest of the way to the office and then went to the health center to get cleaned up.

Ever since that day my wrist has been hurting. It comes and goes and when it goes I inevitably do something to aggravate the injury. My impatience wins out over my need to fully recover. Living an active lifestyle means I’m always doing something and there is a high potential for re-injuring myself. Just when I think I’m nearly healed, a volleyball spike will slam my hand back, I’ll fall and catch my self on my hands, or slam my palms into another body trying to grab a flag while playing football.

It’s now reached the point where it hurts quite a bit and I’m near the point of actually seeing a doctor about getting a better brace/cast. A quick glance at the typical symptoms for carpal tunnel has assuaged that particularly concern, but the nagging pain is clear evidence that _something_ is wrong. I’ll give it a few more days and see if it starts to feel better.

Now, I say it’s fitting that I should be having this pain because it was only yesterday I received a bill in the mail from Memorial Health System in Chattanooga. A few months ago I had gone to the emergency room for a particularly nasty case of pink eye (don’t ask how I got it, I have no idea). A few hours in the waiting room and 10 minutes with the doctor I was out of there with a prescription for some antibiotics. (Special thanks to my best friend for being my seeing-eye human.)In addition to the pain of having rusty spoons shoved into my retinas, I had a sick feeling in my stomach as I wondered how much this sojourn would cost. My insurance at the time was shitty (still is) and I knew I’d be on the hook for quite a bit.

The bill came and it was about $250. It was actually less than I was expecting and I made arrangements to pay monthly. Happy that it was cheaper than expected, I went about my life trying to stay as healthy as possible.

But the bill I received yesterday burst a hole in my happy bubble. Naively, I assumed the previous bill was the only one. Why would they send me more than one? I only get one bill when I go to a restaurant, buy a car, or order something online. Why should my health care be any different? Then I remembered my previous experience with medical billing: the doctors bill separately from the hospital. It’s an illogical system in my mind that places a heavy burden on patients who are shown no menu of options prior to service. It’s akin to going out to eat, getting what you want (or need) without any understanding of cost, and then receiving multiple bills from the server, chef, and busboy as well as a bill for time spent occupying a chair in the building. It’s stupid and borders on unethical.

The final cost (well, what I’m hoping is the final cost; another bill could be headed my way for all I know) is over $500 and I’m currently lucky to not be in collections for the newest bill. I had received the bill for a few months but I confused it with the bevy of other medical statements I get each month. In particular, I confused it with a bill I received about the same time for almost the exact same dollar amount for services rendered almost a year ago when I was tested to prove that I was not insane for becoming an atheist. I hope you’ll notice the irony in having to prove my non-insanity because I stopped believing in a god. Because of my confusion, I haven’t paid on the debt in almost three months. Too many hands on my time, too many bills on my mind.

Clearly, all of this is a venting of my frustration at the medical establishment in the United States. Go to any country with socialized (GASP! I said the dirty word) medical care and all of this would be free to me at the point of service. Of course it is not free and the money is recouped through taxes and other means, but the crippling costs (and eventual debt) are kept from the individual so as to avoid a wholly unnecessary level of stress and energy expenditure to stay healthy.

This now becomes my fifth medical bill for which I must pay monthly, some of which are from late 2010. It would have been number seven, but I chose to put the full amount of two others on my credit card rather than deal with the stress of adding two more separate online services to pay my debts. My total medical debt is over three grand and I’m considering my options of how I can pay these damnable bills faster.

Here’s to hoping my wrist heals on its own.