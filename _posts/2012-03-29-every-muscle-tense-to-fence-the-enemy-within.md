---
id: 104
title: Every muscle tense to fence the enemy within
date: 2012-03-29T22:44:15+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=104
permalink: /every-muscle-tense-to-fence-the-enemy-within/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Dreams
  - Future
tags:
  - beliefs
  - choices
  - distraction
  - dreams
  - enemy
  - entrepeneur
  - future
  - motivation
  - resistance
  - rush
  - struggle
  - write
---
_I’m not giving in to security under pressure. I’m not missing out on the promise of adventure. I’m not giving up on implausible dreams. Experience to extremes._

Those are lyrics from an excellent Rush song titled The Enemy Within. This song has resonated with me very strongly over the past few weeks because of the internal struggle I’ve been undergoing.

I originally had another essay I wanted to post tonight but I just wasn’t feeling it. I decided to write this one, but it’s taken a massive amount of effort to bring it to life.

Since I started writing consistently in January I’ve written over 60000 words. It’s a strong result of which I’ve been very proud. But something happened about 10 days ago. I stopped.

This was one of the results of my 30 days experiment of no internet at home. I was keeping track of how many days in a row I was writing 1000 words using the site chains.cc. The momentum of seeing that unbroken chain for over 45 days was a daily source of fuel that inspired me to write even when I didn’t want to.

But without that visual reminder, the Resistance, my enemy within, reared its ugly head and conspired to keep me from what I want. I started trading in what I wanted in the moment for what I want most. I stopped writing and it sucked.

When I was writing daily, I felt like my thoughts were more organized and I was more at peace with my journey. It was a release valve for pent up creativity and stress. Writing is a way for me to express myself in a way that is uniquely my own.

Sure, we all use the same 26 letters and myriad symbols, but the order in which we place them and the soul, if you will, that we give them makes it entirely unique. These words are _mine_ and that makes them awesome.

When I finished writing for the evening, whether it was a struggle to reach the 1000 word threshold or if I went out of my mind and wrote 3000, the sense of affirmation I got from seeing my words glow back at me was intense.

And yet with all these great reasons about why I shouldn’t stop writing, I did. Maybe I got busy, or tired, or just plain lazy. I had legitimate excuses such as school, funeral, and friends, but those don’t really qualify in my mind. I know that if I _really_ wanted to I could have made it happen. But I didn’t. The rationalizations came flooding in that I would make up those words later by writing 2000 words tomorrow. Well tomorrow became the next day and the next and my unwritten word count ticked up and up.

My internal enemy was even conspiring against me to not write this post. I’m over at my parents’ house doing laundry because my washer died (another excuse) and the urge to just veg on the couch and watch TV is strong. In fact I did that for a bit. Something about sitting down and writing scared me.

Perhaps it’s the fear of success. I’m getting closer to defining what I want out of my future. The Trailblazer program is going well (I promise I’ll post about that at some point) and the path I want to take is less murky. The possibility that I might actually _succeed_ at getting what I want scares me. Actually becoming a good writer and a successful entrepenuer is intimidating as fuck, especially when my whole life has been one giant security blanket.

I recently had coffee with a friend who’s in the Trailblazer program with me and he said something that has really stuck with me. I’m paraphrasing, but the gist is that if what you want to do makes you uncomfortable, then you’re on the right path. Well, I’m fucking uncomfortable. Engineers aren’t supposed to want to be better writers and share stories. Bloggers who get paid to write and create on the internet don’t come with a Ph.D. out of grad school. But that’s what I want.

It scares me to tell that to people, specifically my coworkers, boss, and family. Okay, _everybody_. The smart and safe path is to find a job which is moderately enjoyable, pays well, and doesn’t kill all of my happiness. That’s what I’m expected to start looking for in the coming months. But I don’t want that. The stars that are laid out for me are not the ones I want. My goal is to change my stars.

And it starts with honesty and commitment. I have to be honest about what I want. If I want to create a life of my own I need to be honest about what’s actually going to get me there. There are an infinite number of things that I can distract myself with which will feed my internal enemy. There’s a small amount of things I can do to actually achieve my dreams. Every action I take needs to be questioned. Is this going to help me get what I want most.? Will going to page 12 on reddit actually help me? No. Will sitting my ass down and writing 1000+ words? Yes. That conversation needs to happen often.

Recommitting myself to writing is also essential. There are a million things I can do besides writing such as improving my blog, designing a product, talking to people, reading random books, but none of these things is going to help me tell my story. I have a message that the world needs (I believe we all do) and I want mine to come out in a way that makes others stop and listen. And to do that, I need to be a good writer and communicator. The fundament of my success will be the quality of my communication. It’s the thing that will get people to pay attention and keep coming back for more. My words need to add value to people’s lives. That kind of impact takes dedicated practice. It’s not going to happen by accident.

Starting today, right now, I’m kickstarting my habit. I’m going to write everyday. 1000 words minimum. I did it for almost two months consistently and it was spectacular. I’m going to fight the resistance and fence the enemy within. If a year from now all I’ve done is write consistently and haven’t been able to make progress on achieving my dreams, then it won’t be a year wasted.

I have to write or die. I like living, ergo, l write.

&nbsp;

_Is it living, or just existence? _

_Yeah, you! It takes a little more persistence_

_To get up and go the distance_