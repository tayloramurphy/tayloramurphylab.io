---
id: 218
title: Into the Wild
date: 2012-10-02T20:11:29+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=218
permalink: /into-the-wild/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Future
  - Learning
  - Personal Development
tags:
  - escape
  - life
  - love
  - outdoors
  - reading
  - stress
  - thinking
  - vacation
  - work
  - writing
---
I’m sitting on top of a mountain. Well, sort of. I’m not sitting on the ground outside. I’m actually in a cushioned seat. To my right is a window. On the other side of that window is a somber scene: nature, dulled by heavy rains, patiently waiting for the sun to spring it life. The ground is damp and drops of water, still clinging to the leaves, fall on it in staccato patterns.

My sanctuary, for this is a true sanctuary for me, is a small camper. It’s the kind you’d attach to a truck and drag around the country as you snap pictures at every touristy thing there is to see. It’s probably 200 sq ft. To much of the world, it’s pure luxury. A bed, toilet, shower, kitchen (with fridge and freezer), a table, sofa, and a small TV. I think there’s even some climate control which I hope not to use. It’s nicer than many places I could imagine living in.

This camper is on my grandparents property, about 200 feet from their house. My car sits by the trailer hitch of the camper, waiting for me to command it back to the road. Wires carrying power run from my sanctuary to the main house, as does a hose carrying water. There’s even a faint WiFi signal coming from the house that I can connect to periodically.

I am by no means roughing it.

But I did not come here to rough it. My goal is not to challenge myself by limiting my access to eletricity, water, or creature comforts. The goal is to breathe. To take a break, a vacation I suppose, from the hustle of my life.

Writing that sentence brings with it a sad sort of realization. Over the past several months, I’ve been working at a break-neck pace on my research. I’m in the final year of my Ph.D. and I needed a break. The work I’m doing, while engaging and challenging, is something that I’m not fully invested in. I don’t 100% _believe_ in my research and, as such, it’s difficult to bring the kind of energy and focus that would do my work justice. So, for the past several months, I’ve been forcing it. That effort had finally caught up with me in the past few weeks. I was becoming depressed, sick, sluggish, and my performance had slipped. It was a struggle to do the things I actually enjoyed. Instead, I found myself wasting time and doing things I knew were not good for me.

It was in a moment of clarity that I decided I needed a break. It was a good time in my work. I had just given my committee update for the year (which went quite horribly) and many of the instruments I needed to use would be in use by other students. Papers and reviews would be submitted by Friday, and I knew that I could disappear for a week and things would be okay. Once I had that realization, it became my sole focus to make it happen. For the next 4 days, I was a man on fire. I accomplished more, quicker, than I ever have. I was excited to finish it because I knew it was bringing me closer to something I really wanted: a chance to be free for a while.

I went into my PI’s office and told him, straight up, that I was taking next week off. Whether he agreed with the idea or not, I think he could tell in my eyes that I was serious. I needed the time off. With his approval, I made the plans. I would visit my friends in Chattanooga for a few days, and then drive off to Dunlap and seclude myself in the woods.

Chattanooga was a whirlwind tour of fun and relaxation: tattoos, parties, hiking, reading, talking, and the pure enjoyment of being human. Then I said goodbye and drove into the darkness on a highway towards my freedom.

In the strictest sense, I am not free up here. Responsibilities are still mine and they await me when I return. There are bills to pay, emails to respond to, and science to be done. But they can all wait for a while. They will be there no matter what and they will demand my attention whether I wish it or not.

For right now, I choose not to give them my attention. There are some other things in my life that need my attention right now.

The first is my writing. I am a writer. That I know. But how worthy is one of that title if the output is just a dribble, much like the staccato notes playing across the roof of this camper? A musician isn’t a musician if they don’t play their instrument. A request for a performance from a real musician is not met with hemming and hawing about how they’ve been busy and other things have gotten in the way. Similarly, a writer who only talks about how much they love writing but never seems to publish anything to the world is no more a writer than one who sings in the shower is a performer. I am here to write, to practice my craft, to diligently create every day for many hours. To strain my brain and the muscles in my fingers as I wrestle with the words and form sentences and paragraphs. To create worlds that evoke the emotions of the long-dormant humanity in all of us. I am a writer and this is my craft. I will work at it with the devotion a wood smith or metallurgist has for his physical work. My domain is my heart and mind and the hearts and minds of my readers.

> “When a man becomes a writer, he takes on a sacred obligation to produce beauty and enlightenment and comfort at top speed.” ~ _Cat’s Cradle_

My second focus this week is reading. A good writer reads. There is no doubt about that. And boy do I have so much I wish to read. More than I can read in a week. But with the time that I have, I can put a dent in it. I read to enjoy myself as I get lost in a story. I read to learn about something new to me. I read to understand what good and bad writing looks and sounds like. I read to be inspired and motivated. I read to live. Because to me, reading is living and living is incomplete without reading. Reading brings us in touch with another person through their mind and it engages all aspects of our thoughts. Who can help but to grow when they read, especially when they read for love? I read for love of the words and for love of the humanity behind it all.

My final focus is reflection. Reflection is a state of deep inner thought. It is a time when the great power of our mental faculties are turned inward to examine their own machinations. Great insight and peace can be wrought from a reflective period. I seek no specific goal with my reflection. There are things I do wish to think about, such as my work, my beliefs, my feelings, my future and my present. But I do so with no specific aim. I wish to explore each of these things as they arise without force or direction. This sort of deep reflection needs time to occur. Much like a timid animal peering out from under a rock, this deeply reflective state needs time to feel comfortable with its surroundings before it can take a cautious step out and explore. So here I am, in the woods, away from my stuff and my responsibilities, creating the space, time, and external relaxation that is needed for true inner peace and reflection to occur.

As I sit and think about all this, I wonder if I will have enough time. Is 5 days, or even a week, really long enough to gain anything useful? What if I waste this time by distracting myself? Because, even in this space where there is a plentiful feast of nature, beauty, and free time, it is possible to waste the days. For all the driving I did, I’m still but steps away from hundreds of TV channels and thousands of websites. The temptation is there and so is the longing. But I know what I must do, what I am _compelled_ to do. I must write. I must read. And I must reflect.

I have given myself a gift. A small window of time in which every hour is mine to do with as I please. The true measure of myself, as a man, is what I do with that gift. Will I honor it and put my whole being into it? Or will I destroy it in frivolity and weakness?