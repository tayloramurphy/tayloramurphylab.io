---
title: Transparency in a Pandemic
date: 2020-05-08T13:21:00+00:00
author: Taylor Murphy
layout: post
permalink: /transparency-in-a-pandemic/
---

I am deeply frustrated by people and organizations that do not exhibit high levels of transparency. It's frustrating because I know that being transparent is totally possible and the primary reasons given for not being transparent are fear-based reasoning or just plain bullshit. I'm lucky enough to work at a company that claims to [value transparency](https://about.gitlab.com/handbook/values/#transparency) and actually follows through, so I know it can be done.

My recent frustration has been focused on my son's school. Like basically everyone else in the world, his school is shut down for the rest of the year due to COVID-19. My son is almost two, so technically at his age it's more of a day care, but being a Montessori school they like to think all age groups are in a school. However you describe it, it's been closed. 

The frustrating part is not the school being closed; that was absolutely the right decision. It's the lack of transparency around the administration's decisions and financials.

Unlike many schools in the area, our school has asked us to keep paying if possible. This was couched in the language of community, family support, and religious undertones ("showing grace"). Families who have been impacted financially by the quarantine or shutdown have been encouraged to speak to the administration privately.

I believe this is the wrong approach. 

In our particular case, the school is a 501c3 non-profit organization which means their financials are generally available ([Thanks ProPublica](https://projects.propublica.org/nonprofits)!). But I believe it's unreasonable to have families sleuthing around to find out what the current financial situation is. 

The school should have immediately been transparent about the following:

- Emergency fund levels
- Percentage of families unable to pay
- Expected cost reduction, if any, due to reduced facility use
- Current and future plans around funding

We've been lucky because my wife and I are both on parental leave for 3 months with our second son. This means we haven't had to hire extra help to watch our eldest while we work. We definitely would have if we were working. And we would have appreciated guidance from the school about whether extra money spent to have child care during this time would count as "financially impacted".

The ideal scenario would have been to communicate to all families about what is the current and future funding situation. Something akin to 

> "20% of families have said they're not going to be able to pay the full tuition, while 15% said they could only pay half. If you're able to pay 80% of your tuition we'll be able to maintain staff and payroll".

So far, there has been none of that. A break down by grade would be especially helpful I think to inspire solidarity within the grades.

What also hasn't been communicated well is the status of any Paycheck Protection Program applications. I discovered they have applied for one, but there's been zero information about how much they applied for, what the current status is, and what their plans are if they do receive the money. I would like to see a statement along the lines of 

> "We have applied for a $50,000 loan to cover two months worth of tuition for everyone. The application was made on March 25th and is still in process. If we receive these funds we will be refunding everyone their tuition for the months of April and May."

What's also not clear is how the school is preparing for something like this in the future. What is the plan if we have to shutdown for a month in the Fall? Is there a plan to have emergency funds for this situation? Are families expected to find their own child care if they absolutely need it? Answers to these questions would go a long way towards making everyone feel better about the situation.

The current system for getting any information or relief is a one-on-one discussion with the head of the school about your specific situation in which he'll waive your tuition if you've been impacted. I have no idea what the criteria are to get this relief.

What feels pernicious about communicating this way is that it limits the abilities of families to have a larger understanding and take collective action to support the school. If I knew that another family in my son's class or grade couldn't pay the tuition, then I'd actually be happy paying the full amount. The lack of transparency and understanding about what's going on makes it frustrating.

This frustration is exacerbated by the power imbalance that is implicit in our relationship with the school. Child care, especially infant and toddler care, is exceptionally difficult to find in Nashville. I've sent a couple of emails to try and push for more transparency, but there is an inherent understanding on my part that if I become too much of a thorn in the side of the school they can kick us out and they'd have 10 other families lined up the next day. This sort of power imbalance is why people form Unions and Parent Associations, but that's a topic for another day.

---

I rant about all of this to come to a sort of conclusion on why I think transparency in this, and many other, situations is so important: transparency breeds trust and cooperation. When information is withheld and obfuscated it breeds resentment, distrust, and enmity. When you're transparent about a problem, it enables you to bring people into the problem solving space so you can work the issue together. Being transparent means you have a mutual understanding of the reality of a situation and you don't leave people behind to conjure destructive fantasies on their own. 

I can imagine a world where my son's school was transparent about their current financial situation and we felt confident in their decisions. I can even imagine a world where I'm still paying full price for not receiving any child care, but I'm actually happy about it. I believe being transparent can positively affect people's emotions and bring understanding and peace to difficult situations. 

Transparency is very important to me and I will keep advocating for it as best I can. My hope is that I will live these values better than I see them being lived out in my community and government.
