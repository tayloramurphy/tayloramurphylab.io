---
id: 409
title: On Personality
date: 2014-06-24T07:22:24+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=409
permalink: /on-personality/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Personal Development
---
The climbing gym I go to recently opened a new location. It&#8217;s larger in ever sense: bigger routes, more equipment, more showers and more space overall. Everything is nice and shiny and new. Yet, I can&#8217;t help but feel like it&#8217;s missing something and I finally figured out what that is: personality.

What constitutes a specific personality can be challenging to explain to someone else, even if it&#8217;s easy to define. According to Webster&#8217;s dictionary, personality is &#8220;that which constitutes distinction of a person; individuality.&#8221; This adequatly sums the definition for me. It&#8217;s what makes a person unique. What&#8217;s missing at my new gym is that uniqueness. What&#8217;s worse is that it had a personality but now it&#8217;s been sanitized.

It was the little things at the old gym that made it unique. The benches had a chaotic assortment of stickers on them. All of these have been removed and the benches were washed. Along the walls as you walk in were autographed pictures of famous climbers. These have been moved to the upper floor in an out of the way location. The handoff of your member badge between you and the employee has been replaced by a self check-in station. The metal signs that were entirely too heavy to be reasonable are gone. In their place are painted signs and window clings.

None of these things in and of themselves are bad. For somebody new coming in, it&#8217;s probably a very welcoming sight. Everything is organized and laid out logically. You can find your way around and it&#8217;s not intimidating at all. So if that&#8217;s what they were shooting for, then they nailed it. But it does sadden me a bit that the rough edges have been smoothed over. The quirks have been silenced.

This got me thinking about our own personalities. How often do we sand down the rough edges of ourselves in the hopes of appearing more \*whatever\* to other people? How often do we not reveal our excitement over something for fear of judgement? How often is the thing we love the most the thing we keep most hidden?

Personality for a person is what makes them unique. It&#8217;s the ticks and quirks, hobbies and interests, mannerisms and laughter. The tattoos we get, the way we style our hair, the clothes we choose, and the music we listen to. Everything makes up the whole package that is you.

The thing about personality is that it&#8217;s going to stand out. People are going to notice yours. I guarantee some people won&#8217;t like it. It can be chaotic dealing with other people&#8217;s &#8220;baggage&#8221;. But if you never let your freak flag fly, then you&#8217;ll never find the people who do like and accept you.

When something has personality, it might be easy to get used to it. But when it&#8217;s gone, you realize how much your enjoyment of that thing or person was because of their quirks and uniqueness. Don&#8217;t be afraid to have a personality and don&#8217;t be afraid to take notice of other&#8217;s personalitites. It makes you more special, interesting, and friendlier. Will it annoy some people? Probably. But you don&#8217;t want to hang with those fuckers anyway.