---
id: 155
title: 'Why being told you&#8217;re smart is not good'
date: 2012-05-29T22:16:36+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=155
permalink: /smart-told/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Future
tags:
  - brain
  - distractions
  - effort
  - grad
  - lazy
  - motivation
  - school
  - science
  - smart
  - work
---
All my life people have told me that I’m smart. From a young age to now, it was my brain power that was praised. Very rarely did someone commend my work ethic.

This fact has had several detrimental effects on my life. The first is that it made me feel superior to many people. I thought I was hot shit. The second is that it made me lazy. Instead of recognizing that my will power and efforts were what would help me succeed, I focused on doing things that were moderately challenging to me and that would keep my ‘smart’ status elevated. It was a rare moment when I took on something that I might result in failure.

If I did fail, I brushed it aside instead of taking responsibliity for it. I would espouse some nonsense such as “well it was rigged” or it was “unfair”. Never did I think that it was really my fault or that I wasn’t good or smart enough.

This behavior has led me down paths that aren’t necessarily the best ones for me. It led me to choose things that I felt had a high probability of being achieved, whether I wanted to do them or not. Instead of looking for the brick walls that I might not be able to scale, I found the ones that looked impressive, but aren’t substantial.

Let’s take my current efforts in grad school for instance. Nearly everyone I talk to finds it very impressive that I’m working on my Ph.D. In some ways, it definitely is. Not everyone gets a higher level degree and certainly not in Chemical Engineering. While this might seem like it’s a huge challenge that I’m working my ass off to overcome, I actually chose it as the easier path.

Part of my decision to go to grad school was motivated by fear. My fear was that I would not be a good engineer and I decided to continue my education to become more learned. I didn’t really have _that_ much of an interest in research. I just wanted to feel comfortable that I would know enough so _I could avoid failure._ So grad school became an escape from entering the ‘real world’.

As it turns out, graduate school has been a big challenge for me. It has been a difficult path and I haven’t been able to fully rely on my smarts for everything. I’ve *_shocked gasp* _ had to work hard at it. But this brings up another question: have I really had to work that hard?

My honest assessment? No. While there have been times when I have to stay late, or come in at 4 AM, read a ton of papers, write page after page, and work on problems that used up an entire eraser, I think graduate school has been a _relatively_ easy challenge. Don’t get me wrong: it’s hard. But I haven’t felt like I had to work at it nonstop all the time.

I’ve made some sacrifices, but in reality, I still have it pretty damn good. And yet I still feel pretty lazy. I suppose the heart of what I’m getting at is that, while from the outside looking in it appears that I’m giving near 100%, I know from that on the inside, I’m not. I haven’t been giving my all to my project and my work.

I become distracted easily. I’m unfocused and lethargic about it at times. My motivation and excitement for the work has waned and I’m just going through the ‘sciencey’ motions at times. My work is still good and sound, but my heart just isn’t in it. There are times when I feel more excited than usual, but within a few days it’s back to the doldrums.

Why I’m so unmotivated is for another essay, but the fact is that I know I’m not reaching my potential because of it. And the only person I have to blame is myself. Some might look at what I’ve done and tell me that I’ve done a lot and I should be proud. And I have and I am. But I know in my gut I’m capable of doing more awesome things.

There’s a part of me that wants to sit back on my laurels and just kind of coast along. It wouldn’t take much effort to find a mediocre job doing somewhat interesting work and earning a good paycheck. That option is there. But the other option is to challenge myself, chase some dreams, and really push myself to be awesome. That’s an ambition that’s difficult. It’s tough because it has a higher chance of not working out than succeeding. But imagine if that barrier were overcome and I were to do some amazing things? If I were to really push the boundaries of what I’m capable and make a true difference in the world? How spectacular would that be?

The challenge, now, is to actually chase that dream. It becomes so easy to distract the mind with in-the-moment pleasures. In the present, it _is_ more fun to hang out with friends, go out to eat, take a nap, or just goof off. But I’m trading the potential of greater, long-term joy in the future for the short bursts of fun and pleasure.

“Don’t trade what you want most, for what you want now.” _Unknown_

These are wises words that everybody has probably heard but few follow. Why? Because it’s hard to sacrifice for your future self. This is why so many people are overweight and obese: that Big Mac looks so good right now, nevermind the weight you’ll gain because of it. Why should I exercise now when I can sleep? We sacrifice our future for our present (there’s a whole political side to this as well&#8230;).

So where does all this leave me in my present situation? I have some very specific tasks to complete to finish my PhD: Get another first author paper out, write my thesis, and defend. Every action I take has those thoughts in mind. I know that I can’t go back in time and make past Taylor work harder and focus better. But I can choose to do that now.

Simultaneously, I can choose to create a life that is more fulfilling to me. I do not want to lie on my deathbed and regret not challenging myself and failing to reach my potential. That would be one of the most heart-breaking feelings I can imagine. I want my life to mean something, to leave a positive mark on other people, and to make the world a better place for having been. My outer actions need to meet the high bar of my inner potential.

This is an call to action for myself and anyone else who has ever been praised for their intelligence and not their efforts. Your brains can only take you so far. They alone might take you to some impressive places, but imagine where you may go if you coupled that with your will power and determination. Do the work, focus on your dreams, and have no regrets.