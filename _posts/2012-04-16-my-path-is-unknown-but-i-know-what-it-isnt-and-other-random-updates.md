---
id: 117
title: 'My Path is Unknown, but I Know What It Isn&#8217;t. And Other Random Updates'
date: 2012-04-16T22:57:21+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=117
permalink: /my-path-is-unknown-but-i-know-what-it-isnt-and-other-random-updates/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Future
tags:
  - beliefs
  - biotech
  - challenge
  - dreams
  - eating
  - future
  - goals
  - graduate
  - happiness
  - industry
  - knots
  - nervous
  - path
  - school
  - self-reliance
  - success
  - toastmasters
  - vegan
---
My stomach was in knots the more I listened to her speak. The path she praised is not the one for me, and I knew it in my gut.

We had an alumnus of my PhD program come to school and talk about her experience in industry. She offered to have a roundtable discussion with graduate students interested in her career path and success. By all accounts, she is a very successful woman. She graduated in 2007 in 3.5 years, worked for several companies, started a 4 million dollar business (which she reminded us of frequently), and now travels and does consulting.

It seemed like she was living the dream. But why was I so alarmed by this conversation?

Ostensibly she was doing work that was making significant advances in drug discovery. In fact, she regularly took her team to the hospitals to remind them why they were doing their work every day. It seemed like she was in alignment with her why. And maybe she was. But I knew immediately that it wasn’t going to be good enough for me. I was not resonating with the description of this work at all.

At one point in my life I did. In fact, one would think that with my background and training that following a similar career trajectory to hers would be in alignment with who I wanted to be. But I know that is no longer the case. I’m on a path that I no longer _feel_ in my bones to be good for me.

I’m still trying to process what _exactly_ was my problem with her life. I suspect I will continue to process for a while. The good thing about this experience though, is that it reminded me why I’m trying to change my life. I know I don’t have to do work that doesn’t resonate on a deep level with who I am. I’m not on that path, but I intend to make it.

I will continue to go to events like this. Meeting people in the industry is great for several reasons; on the off-chance I do decide to follow that path, I could have some contacts. But it’s also an excellent way to remind myself about what I don’t want. Every time I see somebody doing work that seems cool, but that leaves them looking so uninspired and dead inside, I feel emboldened to continue chasing my dreams in the face of this massive inertia tearing me away from them.

I can do it. But it’s sure as hell not going to be easy.

__

On that note, I wanted to give a quick update on my current 30-day challenge. Going wholly vegan has been great. I feel light all the time and haven’t been starving or felt like I was missing out. I even went to McDonald’s and managed to stay on a vegan diet. I think continuing this for the rest of the time will be no sweat and I may even continue it after the 30 days are over. I will admit I slipped up once and got some frozen yogurt by mistake, but I immediately regretted it and remembered immediately why I gave up all dairy.

The not eating 3 hours before bed challenge has been a bit tougher. I, evidently, lose my mind when I’m around nuts and raisins. Those things will be consumed faster than a cheetah on crack. Some nights I’ve been able to do it. Others, not so much. Some days it’s hard to know what I should do. I get back from playing soccer or frisbee at 9 and I haven’t eaten dinner. I’m hungry and I probably need to replenish some energy. So I eat. I’m not beating myself up too much about it though. It has helped me to have better portion control, but even as I write this I’m munching on some almonds and raisins. Oh well.

__

Finally, I wanted to share a brief inspirational moment I had tonight at my Toastmaster’s meeting. The 3 speeches given tonight worked very well together even though the speakers had not discussed it before hand. One was about the speaker’s life of travel and how he came to be where he is at 42. The other was an inspirational speech about chasing after your dreams, believing in them, and focusing on what you want. The third was on the topic of self-reliance. Combined, they were a bright spot in a day that was filled with the negativity of societal expectations of success and happiness. My own happiness and success is what I make it, and these speeches were a great reminder of it. Also, performing my role well as General Evaluator helped improve my mood too.

If you’re looking to connect with people who are passionate about personal development and want to improve your public speaking and leadership skills, then check out Toastmasters. For real. You will not regret it for a second.

__

**Thanks for reading my ramblings. Most of my intramural sports have ended and I’m going to be writing more regularly, so expect more frequent updates. Peace!**