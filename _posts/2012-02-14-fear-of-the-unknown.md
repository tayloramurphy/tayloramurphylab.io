---
id: 54
title: Fear of the Unknown
date: 2012-02-14T08:41:04+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=54
permalink: /fear-of-the-unknown/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Emotions
tags:
  - comfort
  - fear
  - flinch
  - nerves
  - scary
  - travel
---
For those of you who haven’t read [Julien Smith’s The Flinch](http://www.amazon.com/The-Flinch-ebook/dp/B0062Q7S3S), I would ask that you stop reading this and go read his book now. I’m serious. It’s completely free and totally awesome. Go ahead. I’ll wait.

Read it? Awesome!

Now why did I have you do that? Because we need to be on the same page. I’ve experienced the flinch in signficant ways lately, and I wanted you to understand the meaning by flinching.

My thoughts have been on travel lately. I read the travel adventures of many other people and they’re all doing some really cool stuff. [Chris Guillbeau](http://chrisguillebeau.com/3x5/) is traveling to every country before he’s 35. [Niall Doherty](http://ndoherty.com) is traveling around the world without flying. [Colin Wright](http://exilelifestyle.com) lives in a place for 4 months and moves based on the votes of his readers. [Benjamin Spall](http://liferapture.com/) just quit his job and moved to Spain. Earl from [Wandering Earl](http://www.wanderingearl.com/about/) has been traveling for 12 years.

**These people are doing some amazing things.**

It’s extremely easy to sit in front of a comptuer and vicariously experience these strangers’ adventures. After all, it takes no effort on the part of the reader to journey with them in their words. We can read what they’re experiencing and feel a little bit reflected glory for having been affected by someone else remarkable experiences.

So I took it a step further. I started to imagine myself, in full detail, being in their shoes. I pictured myself owning fewer than 100 things. I saw myself in a foreign country, not knowing the language. I tried to experience the feelings I might be having in a place that I’m unfamiliar with surrounded by things that might make me uncomfortable. And you know what happened?

**I flinched**. **Hard.**

There was a gut-wrenching nervousness that came upon me as I pictured these scenarios. I, like most of you I expect, have always had a relatively comfortable life. We’ve got a home, maybe a car, and we have our routine. Now imagine having none of those. Imagine only owning what you can carry on your back. Imagine not having a job to go to every morning and instead you earn your money through a ‘non-traditional’ avenue. Imagine doing something you’re entirely uncomfortable with, just for the experience.

**Talk about scary.**

I did this exercise and felt so incredibly nervous! Nervous about something that I wasn’t doing. I’m not even planning to do these things and I reacted like this.

That should tell you something about how conditioned I am, and I suspect a lot of you are, to avoid something seemingly risky. I became afraid about something that I’m not even involved in.

This is a habit that needs to be broken. Make no mistake: it is a habit. It took years of practice to teach myself to be like this. It will take practice to reverse it. I believe my un-education can be quicker than my indoctrination was, and I aim to take steps to make it so that I’m able to face those fears with courage.

I will be able to see the flinch, recognize what it is, and pass through it on my way to something great. No more limits. **No more flinching.**

_What&#8217;s caused you to flinch in your life? How have you dealt with it?_