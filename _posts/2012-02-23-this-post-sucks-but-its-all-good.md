---
id: 67
title: 'This post sucks, but it&#8217;s all good.'
date: 2012-02-23T09:15:19+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=67
permalink: /this-post-sucks-but-its-all-good/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Future
tags:
  - destination
  - diligence
  - journey
  - practice
  - work
---
**At some point along a journey, a destination must be defined.** You may start out not knowing where you’re going, but that won’t last forever. The destination may change along the way, but instead of going away from your origin, you start heading towards the goal instead.

I’ll be the first to admit, that this blog is on a journey. Engineering the Uncommon is an extension of me and I’m on a journey as well. Ostensibly, life is a journey. If this is how we define life, then everybody’s journey ends at death. But within that greater journey, there are smaller journeys or adventures that are part of the overall experience leading up to our inevitable demise.

So if this blog is a journey, then there needs to be a destination at some point. Do I know what that is right now? Heck no. Am I cool with that? **Definitely.**

I have a rough sketch of what I want from this. It’s meant to be amorphous for a while. We’re going to have some growing pains. My posts may suck, or the writing is bad, or the design of the blog may be ugly. That’s okay. Life is sucky, bad, and ugly at times. It’s easy to look at other blogs (or lives) and see how much better they look. They seem to have it all together and their destination seems pretty clear. It’s tempting to become envious and want to have it all better right now.

That’s not going to happen. Things will get better, but it takes time. But the only way it will get better is through persistence. **Diligent practice in the craft is the only way to improve.** Most people don’t become successful overnight; they worked hard for a long time to get that way. I don’t really want overnight success anyways, I’d rather build strong relationships slowly so that each one means something. I’d rather write for an audience of 100 passionate followers than 10000 flakers. The connections are more meaningful and the value of each reader is greater.

So no, this blog hasn’t got it all together. It’s a record of the mistakes, successes, failures, and growing pains that happen when a person tries to change their life and engineer something uncommon.

I hope some of my writing either now or in the future resonates with you. I do have some fantastic ideas that I want to bring to life, but it takes time. The only thing I can promise right now is diligence. **I aim to be the hare in this race.**

_Will you join me?_