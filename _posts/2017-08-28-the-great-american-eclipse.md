---
id: 469
title: The Great American Eclipse
date: 2017-08-28T21:02:57+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=469
permalink: /the-great-american-eclipse/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Future
  - Learning
---
<p class="p1">
  On Monday, August 21, 2017, the USA experiences a total solar eclipse for the first time since 1979. I was lucky enough to be able to see it. Since then, I&#8217;ve been thinking a lot about the event and wanted to reflect on the experience.
</p>

<p class="p1">
  The eclipse, for me, started in the spring of 2016. I was a relatively new member of the National Space Society and had been meeting with the local chapter (Middle Tennessee Space Society). Most of the members at that time were older guys, engineers who were excited about space. These guys devote an inordinate amount of time to educating the public as much as they can about all things space. They recruited me by having a booth at a screening of the Martian; an interested audience if there ever was one.
</p>

<p class="p1">
  Soon after joining I found myself helping out as best I could. This typically meant showing up at movie screenings or events where they were already planning to be and simply being a gopher and ambassador. It was, and is still, great fun though. Talking to people about space and seeing their eyes light up when you tell them some cool fact about the universe doesn&#8217;t get old.
</p>

<p class="p1">
  We started getting people exciting about the eclipse that spring. One of the guys had an entire table devoted to the eclipse. Videos, maps, glasses, and pictures all tried to capture people&#8217;s attention. It wasn&#8217;t too hard though because we in Nashville were going to get front-row seating just by being in the city. A very lucky happenstance and a great opportunity. We felt obligated to spread the word.
</p>

<p class="p1">
  My only experience with an eclipse was a partial one that I saw when we lived in Chattanooga. At least, I think that&#8217;s when it was. Looking at the Wikipedia article for eclipses in the United States makes me think the only opportunity would&#8217;ve been in the early 90&#8217;s when we were in Florida. It&#8217;s possible I&#8217;m mis-remembering. Either way, I know for sure I hadn&#8217;t experienced totality before. Partial eclipses are cool, but now I know that nothing really can compare to totality.
</p>

<p class="p1">
  This meant that anything I told people about the Great American Eclipse was second and third-hand knowledge. Yep, you&#8217;ll be able to see the stars. Yep, 360 degree sunset. Yes sir, the temperature does drop quite a bit. Facts true enough, but the words don&#8217;t have the same intensity when they&#8217;re not supported with experience.
</p>

<p class="p1">
  As the months went on and 2016 became 2017 my involvement and excitement about the eclipse waxed and waned. I&#8217;d go to meetings and help out for a few months then stop helping out. We humans each have our own complicated lives, me included. Finally, in July I started to get my act together and started thinking that I need to get some solar glasses. One quick stop at Amazon and I had three fancy-looking ones on the way.
</p>

<p class="p1">
  Beyond that, I didn&#8217;t do too much planning. My hope and assumption was that I could basically drive a few miles North and basically stop anywhere to see the eclipse. Contrast this with my friends in the NSS who had hotel rooms in five different states and contingency plans if the weather was bad in Nashville. They were constantly checking the forecasted cloud cover. They knew what totality meant and what an opportunity it was to have it cross over your home town.
</p>

<p class="p1">
  I took fewer steps and was less prepared. It worked out in the end, but it could have turned out much worse. A part of me didn&#8217;t want to plan too much because my wife was out of town and was going to miss it. Deeply planning for the eclipse felt like a slight betrayal because I couldn&#8217;t share it with my best friend. Pretending like it wasn&#8217;t as a big deal was a concession I made to my own guilt of not being able to share it with her.
</p>

<p class="p1">
  It wasn&#8217;t until a few days before the eclipse that I decided to drive up to a coworker&#8217;s property just North of downtown with my family. No contingencies, no backup if there were clouds or the weather was bad. Barely prepared with solar glasses. (As it turned out, the ones I had bought were fake.)
</p>

<p class="p1">
  The day before the eclipse I volunteered for a bit at the Adventure Science Center. NSS had a booth and Chuck, our local chapter president, was able to snag his own booth next to the NSS one to set up a few of his models. He has this great set of 1/144 scale models of the ISS, satellites, capsules, Shuttle, a bus, and a tiny astronaut to really show the scale of everything we send to space. It always captures people&#8217;s attention. I took a few hours Sunday morning to stand in the sweltering heat and talk to people about space. From that event, I was able to meet a few new people from the organization and I also got a 4&#215;4 piece of solar film. I wasn&#8217;t sure how I was going to use it, but it was nice of them to give me a piece.
</p>

<p class="p1">
  I went home and met my family who had driven up from Chattanooga. Later that night I began really thinking about what I wanted to bring camera-wise to the event. I started plugging in chargers and clearing out memory cards.
</p>

<p class="p1">
  The morning of the eclipse came and I was really excited. That morning I decided to fashion a make-shift solar filter for my camera. I took parts of a small box and made a ring which could fit around the camera lens, then taped the solar filter to it. The next hour was spent futzing with the camera settings, convincing myself of the shutter speed, ISO settings, and aperture. It&#8217;s less than 3 hours to the eclipse and I didn&#8217;t even know what settings to use. But I figured something out, vowed to leave the settings alone, and finished packing.
</p>

<p class="p1">
  We loaded up the van with everything we&#8217;ll need for the day: food, water, chairs, beach umbrella, sunscreen, glasses, and more. I toss in my camera gear and off we go. We arrive about 15 minutes before the eclipse starts. We meet up with my coworker and start scouting a location on her land to watch from. The backyard seems most appropriate and we begin setting up everything there.
</p>

<p class="p1">
  After setting up, I take a few shots of the sun and then grab some food. Then the eclipse starts. The tiniest chunk of the sun is eaten by the moon. Snap, snap. Slowly over the course of the next hour and a half, I take more pictures. More people arrive and my family and I share glasses and the view through my camera. It&#8217;s quite hot and everyone is sweating, but as more of the sun is eclipsed we become more excited.
</p>

<p class="p1">
  When we arrived, the clouds were quite sparse, but as the day wore on they came more frequently. A worrisome trend. It was fun though, to see the partially eclipsed sun through the clouds. Made for some interesting shots when the solar filter was removed.
</p>

<p class="p1">
  As the clock rolled closer to 1:28, weird things started to happen. Almost imperceptibly the temperature started to drop. It wasn&#8217;t noticeably darker, but we could all tell we were sweating less. Our shadows started to become sharper. My sisters described it as being in a baseball stadium at night with only a few lights on. I took some more pictures.
</p>

<p class="p1">
  Just a few minutes before totality and a monster cloud covers the sun. The crowd groaned and worried. Would it clear in time? It seemed like it might but who knows.
</p>

<p class="p1">
  A minute and a half to go and it was noticeably darker and cooler. Partially because of the cloud. The bugs became a bit louder and the tension was building.
</p>

<p class="p1">
  Less than a minute to go. It looks like the clouds are just going to pass by. It starts getting brighter as the cloud clears but the lighting is even weirder than before. Everything has this sharp contrast to it and stands out. You can see the &#8220;sunset&#8221; all the way around us. Even with nearly all of the sun covered it&#8217;s still too bright to look at without glasses. I snap a few more photos while looking around.
</p>

<p class="p1">
  The seconds tick by and then the sun is entirely covered by the moon. With a satisfying crispness, we are in totality.
</p>

<p class="p1">
  I&#8217;m still processing the minutes of totality a week later. Watching the footage taken with my GoPro doesn&#8217;t do it justice. Even the photos I managed to take during the ~2 minutes we had don&#8217;t do it justice. No photo or video can give you the full depth and breadth of this experience. The difference between totality and not is, quite literally, night and day.
</p>

<p class="p1">
  That moment hasn&#8217;t left me and I hope it never really does. The beauty of the corona was incredible. The eerie feeling of seeing sunset all around you was like a high. You get a slight chill because the temperature drops and your mind is processing everything, trying to take it all in once. There were fewer stars visible than I expected, but the ones you could see were bright. Our planets, Venus and Jupiter, were very visible (Venus even so on film!). I took a few photos during totality but I didn&#8217;t want to spend too much time on the camera. I wanted to look everywhere all at once and capture it.
</p>

<p class="p1">
  As the end of totality was approaching you could see one side of the sky start to get brighter. With the solar filter off I took a few more pictures without really checking was I was doing. Then that first beam of light crossed the edge of the moon and it was over. The other side of totality played out like the first half in reverse. Weird lighting, rising temperatures, and quieter bugs. As the crescent grew, I took more pictures until we decided to leave.
</p>



* * *

<p class="p1">
  As I think back to the eclipse, there are several thoughts and feelings mixed in my head. I feel incredibly luck to have seen this event. Many parts of the country had cloud cover and couldn&#8217;t even see the partial eclipse. I took a gamble on having no backup plans and it worked out. It didn&#8217;t have to at all but somehow it did. The lesson I take from that is that I really should have planned better. A slightly different wind and moisture pattern and the cloud situation would&#8217;ve obscured everything. It was stupid how lucky I was. This lack of preparation is generally not how I want to live my life.
</p>

<p class="p1">
  Another is how thankful I am I was able to share it with friends and family. Not all my friends and family, but many of them. For all my misanthropic homebody feelings, sharing experiences is what it&#8217;s all about. The older I get the more I come to believe that beauty experienced in isolation is a fraction of beauty shared.
</p>

<p class="p1">
  But the big one is my sense of awe and wonder at the scale and beauty of our universe. Three hunks of matter aligned to give me and many millions an incredible experience. In my mind&#8217;s eye, I can see myself viewing this from the side. The sun on my right, the earth on my left, and the moon traversing between, casting a shadow on the planet. At it&#8217;s most simple, that&#8217;s all this event was. A shadow was cast on the planet. Shadows happen literally all the time. This was just a big one.
</p>

<p class="p1">
  But it was a transcendent one. A shadow that reveals and reminds us of our scale. Our utmost insignificance. It shows you just how fragile our planet is compared to many things in this universe. It connects us to the cosmos in an immediate way. Totality grabs you by the soul and says you are the universe made conscious! You are a part of all this! You are star stuff. Amazing, incredible, and insignificant.
</p>

<p class="p1">
  I feel overly dramatic saying it, but totality feels life-changing to me. Maybe I want it to be more life-changing than it deserves. Whatever the case, I do feel different after experiencing it. Nothing really has changed day to day for me, but I feel (I want to feel) like something deep inside me has shifted.
</p>

<p class="p1">
  Less than a week before the eclipse I went to dinner with some coworkers and a friend from a company we partner with. During the course of the meal, my coworker and our resident genetics expert talked about her love of genetics. I&#8217;m paraphrasing, but the gist was that she felt like she worked a lot, maybe too much, but that she simply loved what she did. She loved talking about genetics. If she could, that&#8217;s probably all she would talk about it. It was a very earnest and honest statement. Unintentionally, I think she showed me what&#8217;s possible in how a person approaches their work and life. She&#8217;s somebody I respect and look up to: a successful parent, a passionate professional, and a person who loves life.
</p>

<p class="p1">
  In that conversation and thereafter I&#8217;ve wanted to be able to say the same. I don&#8217;t feel that way about genetics, but I do a good enough job within the field. But I think I feel that way about space. It feels like within the past two years or so I&#8217;ve been given permission by someone or something (or myself) to actually go all in and care about something. To say that I&#8217;m interested in space and to work towards spending more of my life on it. Something about space generally captures my imagination. But the specifics of it capture me too. There are engineers and scientists that have to figure everything out to put people on the moon. That&#8217;s space to me. Astronomers have to look at the stars to figure out what&#8217;s happening out there; that&#8217;s space. The engineers who work on better software and data entry for tracking spacesuits, that&#8217;s space too.
</p>

<p class="p1">
  No matter how tenuous the thread, if it connects to space, then it counts. It&#8217;s exciting and it&#8217;s in the field. (Of course, you could say that as we&#8217;re floating on a rock in space that it all connects, but c&#8217;mon, you know what I mean.) And it&#8217;s okay to be excited about that. It&#8217;s not a unique interest in that I&#8217;m the only one. And it doesn&#8217;t cast aspersions on anything else a person might be interested in. You can care about genetics, film, architecture, medicine, fashion, geology or whatever and I can care about space. That&#8217;s what makes humanity so amazing is that people <i>do </i>care about so many different things. It&#8217;s ok to pick something and go all in on it, whatever all in means for you.
</p>

<p class="p1">
  Maybe this is all a lot to take from those two minutes in the shadow. I&#8217;m sure for many people it was a cool experience and nothing more. Like a fun roller coaster or shark encounter. Something to talk about over dinner but then to move past to more pressing concerns.
</p>

[<img class="size-medium wp-image-470 aligncenter" src="http://tayloramurphy.com/blog/wp-content/uploads/2017/08/IMG_0452-300x249.jpg" alt="" width="300" height="249" srcset="http://tayloramurphy.com/blog/wp-content/uploads/2017/08/IMG_0452-300x249.jpg 300w, http://tayloramurphy.com/blog/wp-content/uploads/2017/08/IMG_0452-768x637.jpg 768w, http://tayloramurphy.com/blog/wp-content/uploads/2017/08/IMG_0452-1024x849.jpg 1024w" sizes="(max-width: 300px) 100vw, 300px" />](http://tayloramurphy.com/blog/wp-content/uploads/2017/08/IMG_0452.jpg)

<p class="p1">
  But for me it was different. I look at the picture I took of the diamond ring, the moment just after totality has ended, and I&#8217;m filled with that full sense of awe, wonder, and excitement. I&#8217;m taken to that view of our sun, planet, and moon in alignment. The vast distances between everything and the fragility of it all. Maybe I feel that way because I&#8217;m the one who took the picture. I&#8217;m sure it doesn&#8217;t have the same gravitas for anyone else. And that&#8217;s ok.
</p>

<p class="p1">
  It&#8217;s ok to be crazy and fascinated by something. Our culture tends to admire those people who go all in on something. It makes for a good story. I don&#8217;t know what my story is. But I do know, that right now, I care a lot about space, space industry, and our place in the universe. It&#8217;s the only thing that captures my full imagination and then some. It&#8217;s the only thing I can&#8217;t fully comprehend or keep in my head and that makes me want to pursue it more. There&#8217;s so much more I&#8217;ll never know. I can spend my whole life learning and I won&#8217;t know it all. That thought is humbling and inspiring to me.
</p>

<p class="p1">
  I can&#8217;t wait for totality again. My hope is you get to see it too.
</p>

&nbsp;

P.S. I listened to [this](http://www.radiolab.org/story/sun-dont-shine/) episode of Radiolab today and it brought back a lot of those feelings from last Monday.

P.P.S. Related to that episode is [this NYTimes](https://www.nytimes.com/2017/08/03/magazine/the-loyal-engineers-steering-nasas-voyager-probes-across-the-universe.html) article about the engineers who keep it going.

P.P.P.S. I saw [this article](http://apenwarr.ca/log/?m=201708#27) on Hacker News this morning and thought it was interesting.