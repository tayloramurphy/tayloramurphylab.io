---
id: 376
title: Bonnaroo 2013 Momentos
date: 2013-06-21T18:05:47+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=376
permalink: /bonnaroo-2013-momentos/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Relationships
tags:
  - bonnaroo
  - camping
  - drugs
  - fun
  - in tents
  - macklemore
  - memories
  - music
  - party
  - reading
  - rock and roll
  - sex
  - showers
  - sweat
---
_This post is inspired by one of my favorite bloggers, [Niall Doherty](http://www.ndoherty.com/momentos-march-2013/). Bonnaroo seemed like the perfect fit for this kind of story telling._<span style="line-height: 12.986111640930176px;"> <em>These are in roughly chronological order.</em></span>

Pretty Lights was a lie. These weren&#8217;t just pretty lights, they were of an intensity unmatched by any other show. His DJ stand was set atop a ten foot platform that had a semi-circular array of lights surrounding it. There was a wall of lights behind him stretching over 50 feet. Lasers, gobos, and other various lighting littered the rest of the stage. This show followed The XX who, while having enjoyable music, never built to a danceable crescendo and constantly left me feeling anti-climactic. Pretty Lights was anything but. It was a seizure-inducing, near-deafening show that was fun as hell. We bailed relatively early compared to his finish time of 5:30 AM.

&#8212;

I knew that as the weekend continued we’d only become grosser and more grime-covered. It was at least three in the morning but we were both still awake and coming off the Pretty Lights buzz. Whether that buzz was second-hand drug fueled or simply due to the loud music and intense lights is up for debate. I rolled onto my side and started kissing her neck and ears. “Hey”, she said with a coy smile.

&#8212;

The people at Bonnaroo were always something to behold. It was probably the best place in the world to people-watch. Loud clothing, odd style choices, and a preference for scant fabric in the June heat were the norm. We saw one guy wearing a fur coat to the Macklemore concert, which Macklemore himself pulled up on stage. Many girls only had pasties and no tops. Skimpy bathing suits and fish net stockings were common on women while tank-tops were the norm for men. Watching how all the hairless primates chose to cover themselves, or in most cases not, was a treat to see just for the pure creativity and novelty of it all.

&#8212;

We waited for hours to get tickets to see Ed Helms. Our group was sure that the star of The Hangover and The Office would have a hilarious show. It turns out his show sucked, but we still had a good time. My tarp, which seemed like a terrible idea to bring to Centeroo, wound up being a hit because we were able to attach it to the wall and anchor it to some backpacks. The shade it provided was the envy of the 200 yard line. The <del>comedy</del> bluegrass show wasn’t enjoyable, but the surprise appearance by the Lumineers at the end made it all worthwhile.

&#8212;

Since Mumford and Sons’ show was cancelled, seeing The Lumineers became our number one priority. We succeeded in gaining a spot in the pit line and, after surviving the heat and the cheaters, we secured a spot on the center dividing wall of the pit. The concert was one of the best of our life and we both experienced that moment of full focus and lucidity where you realize these famous people rocking your face off are just like us: human. In every sense of the word we shared their humanity.

&#8212;

I don’t think we ever figured out who was starting this or what exactly they were for aside from looking beautiful. All weekend we would randomly see Chinese candles lifting into the sky. Sometimes it was a lone candle. Sometimes a group of them were released at a time looking like a flock of slow-moving birds struggling to get higher. We finally saw one being lit by a random group of people at the Tom Petty show. We watched it until we couldn’t see it any longer it; a transfixing sight making both of us happy in a way we probably couldn’t have articulated.

&#8212;

“Weird Al” Yankovic has always been on my lists of people to see live. It was never very high on the list but it’s always been there. I made it a priority Saturday night to go see him. This meant missing a few other potentially good shows (Billy Idol and R. Kelly) but I knew it would be worth it. His set was a mix of old and new stuff; some I’d heard and some I hadn’t. Every song came with a costume change and I laughed more during his show than I had at any other point during the weekend. Lindsay laughed because I knew all the words to “Amish Paradise” and “White and Nerdy” and I smiled because I unabashedly belted them out. It was a helluva good time.

&#8212;

The sun was beating down on our shoulders and three days of grime was beginning to feel oppressive. We decided to find the giant mushroom fountain and take a quick shower. Our bathing suits were back in the tent, but we didn’t care. The water was ice-cold and felt amazing as it soaked through all of our clothes. She tried to avoid the chill but I grabbed her and held her under the stream as we both laughed.

&#8212;

We were standing in line to see the documentary about The National. We arrived about 20 minutes before the start of the show so it wasn’t likely we were going to actually get in, but we figured it was worth a shot. Behind us were two people who were engaged in a “thoughtful” discussion that I aptly termed “Bro Philosophy”. “All this, it just makes you think of societal norms and how, like, people here just do whatever they want. It really makes you think.” “Yeah, totally.” Lindsay and I managed to not laugh out loud at the lack of awareness of everything that goes into supporting a festival like this, from the medic staff to the organizers to the ‘capitalists’ who might actually make some money on this to keep it sustainable. But I can’t really blame them, this was an insular event where the outside world didn’t seem exist. Like Disney World, it’s a magical place but it’s not the real world.

&#8212;

Our goal was to be in the pit for The National whose show started at 6:30 PM. We arrived at the What Stage field just as they opened and set up camp against the metal barricades opposite where everyone was queuing for Macklemore, Kendrick Lamar, and Tom Petty. It was a warm day and we did what we normally would do on a warm sunny day: read our books, together, in our own separate worlds.

&#8212;

There were close to 50,000 people in the crowd for Macklemore and Ryan Lewis. We had liked a few of his songs but wouldn’t consider ourselves fans. That changed after seeing him live. The guy is a great entertainer, storyteller, rapper, and has a message we could get behind. In an industry dominated by people who denigrate women and have crappy messages, Macklemore was pushing positivity, self-improvement, and love with his rap. The concert ended and we were converted. SharkFace Gang, baby.

&#8212;

We had formed the unofficial line for The National and had to stop reading because it was getting more crowded. We also wanted to witness the chaos that would be a crowd of kids crushing into the pit to see Kendrick Lamar. We weren’t disappointed. The first 12 or so people followed the instructions and held their hands up, but as the folks in the back, who hadn’t heard or didn’t care to listen to the instructions, started pushing forward, the barricades began bending against the crush of people. The security guards braced themselves between two barricades, pushing the crowd together yelling at everyone to walk and keep their hands up. We leaned over our barricade and helped bush against the metal barrier which was now at a 45 degree angle. Nobody was seriously injured but it did increase my doubt that a crowd of people could do the right thing.

&#8212;

Immediately following Macklemore’s uplifting set was the crap-fest of Kendrick Lamar. Without an ounce of self-awareness people in the crowd ignored Macklemore’s message and danced to the sounds of “Pussy and Patron”. I looked at Lindsay, saddened a bit that this guy was more popular than Macklemore. His songs had mediocre beats, terrible lyrics, and talked about some of the worst ways to live your life. Earlier in the day I had mistakenly thought this guy was a comedian and I was, technically, still correct at the end of the day: his music was a joke.

&#8212;

“Every time someone says ‘intense’ I smile and think ‘Yeah! You’re right! We are _in tents.”_ She said this with a huge smile on her face. I bust out laughing at the hilarious absurdity of the homonym. This girl is smart and silly and I fell in love with her more in that moment.

&#8212;

At a concert where everyone was smoking pot, everyone was drinking, and most people were asking for their friend Molly, I was the guy with the non sequiturs. Instead of offering drugs, I dug into my backpack and pulled out a container. I opened it and offered it to everyone around me. “You want some apricots?” Lindsay burst out laughing and told me she loved me and how awkward I was. I munched on the apricots with a goofy smile on my face.

&#8212;

I was miffed because another group was let into the pit before us and the rushing crowd pushed Lindsay and me off to the side. We weren’t near where we wanted to be in the pit but we managed to snag a spot on the rail near stage left. A few moments of silent focus and I cleared my head and came back to the moment, fully present and enjoying it. It turned out this spot was the best in the house because the lead singer decided to stand on the rails of the pit right in front of us. Lindsay grabbed his butt and kept him from falling. We both shared a look of pure amazement at what had happened.

&#8212;

Both of us had been trying to follow the Paleo diet for a few weeks before the festival. The food we could bring to a festival like this isn’t exactly Paleo-friendly, but we managed as best we could. We considered the whole weekend to be “Cheat Weekend” and weren’t too hard on ourselves. The last night we decided to really splurge and buy corn dogs and a funnel cake. In our minds we imagined this food would taste amazing and be a joy to eat. We were astonished when it tasted even better than we expected. One of the few times our high expectations were greatly exceeded.

&#8212;

The drive home was full of smiles. Most of our stuff was with our good friend who was the only other person who participated in our unintentional Soberoo experience. Our car was in the perfect position to make an exit. Less than ten minutes of waiting and we were on the road to Nashville. The weekend of a lifetime was over and as we held hands in the car we relaxed into the peace of denouement.