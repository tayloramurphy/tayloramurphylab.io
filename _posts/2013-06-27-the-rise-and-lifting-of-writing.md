---
id: 381
title: The Rise and Lifting of Writing
date: 2013-06-27T19:49:16+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=381
permalink: /the-rise-and-lifting-of-writing/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Learning
  - Personal Development
tags:
  - art
  - communicating
  - craft
  - creating
  - humanity
  - learning
  - reading
  - speaking
  - writing
---
A [recent article](http://www.nytimes.com/2013/06/23/opinion/sunday/the-decline-and-fall-of-the-english-major.html?ref=opinion&_r=3&) in the New York Times highlighted some of the reasons I fell in love with writing. The author teaches nonfiction writing to graduates and undergraduates at several prestigious universities. Each year she fears she’ll be out of a job because the incoming crop of students will already know how to write. Each year they don’t.

She laments the fact that the study of the humanities is in decline, particularly the English major. From personal experience I can recall many moments where family and friends would mock others who were getting an English degree. “What are they doing to do with that?” is a common refrain.

Our society, and the students it produces, places a heavy demand on learning immediately applicable skills. Studying the Humanities doesn’t provide that, at least not in an obvious way. What can you do with a great knowledge of Humanity’s writers, musicians, painters, artists, and thinkers? It’s not empirical like the natural sciences and it rarely generates new business ideas. It seems like a privileged way to spend one’s time that doesn’t have an immediately apparent contribution to society.

The author of this article, however, flips the concept of the humanities. She says that when students are older and have more life experience, they come to the humanities with a new verve and desire to learn. They want to know how to write better. Writing with “clear, direct, and humane” language is a skill that comes through the study of the humanities and it’s one that fewer people have.

Almost any college student can write. They have some command of the english language. But their application of it is clumsy.  Every page is filled with jargon and unnecessary words. Students are attempting to reach a word or page count, not a specific clarity of thought. A 500 word essay is stretched until it’s nearly over 10 pages. They learn how to meet an arbitrary requirement, not how to communicate their ideas better. It’s repetitive and useless. (I hope you notice how I extended a very simple thought into an entire paragraph. An example of this kind of writing.)

I fell in love with writing primarily because I enjoyed the act of creating. Writing was a direct transmission of my ideas and thoughts onto a page where other people could read and ponder them. I was creating material for my favorite hobby: reading. I fell in love with immediacy of writing and the realization that it could be a lifelong gift. The books and authors I read and and admired have inspired me to learn how to become a better writer, an endeavor for which death is the only end.

Like the college students I mentioned, I wrote during my school years for many the same reasons. Writing was a requirement and a byproduct of the classes I was taking. It was a necessary evil to earn my degree. I was aware only of its utility, not of its humanity. I’ve always enjoyed reading but I never made the connection between my writing and my reading.

That changed last year. I got a taste of how the writings of the dead and all the writings of the living are connected. I was now on the “open deck of a ship moving along the endless coastline of human experience”. I saw the greats as I passed along the shore and I wanted to contribute something to that coastline, even if it was small and barely noticed.

My family doesn’t write. Aside from a thesis, I’ve never been handed a book and been told your mother, father, grandmother, grandfather, or anybody wrote this. And that, to me, is sad. Writing is truly one of the few ways we have of communicating with the dead. It’s a direct link to what was happening in their mind at the time. I wish the great-grandparents I barely knew had written something to pass on. That wish inspires me to write.

I’m enamored with the idea that my future progeny will be given a book and told “your Dad / Grandpa / Great-Grandpa wrote this. These are his ideas. These are his suggestions for living a good life. These are the stories he told.”

It will be a way for me to talk with them when my body and consciousness are no longer around. It’s one of the few ways our true inner being can exist beyond the grave. And it’s a damn good way to do it too.