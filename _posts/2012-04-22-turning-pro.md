---
id: 121
title: Turning Pro
date: 2012-04-22T20:33:16+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=121
permalink: /turning-pro/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Future
tags:
  - amateur
  - future
  - goal
  - godin
  - goins
  - journey
  - lazy
  - lost
  - pro
  - resistance
  - writing
---
> I haven’t written because I don’t want to write shit.

I wrote that a few days ago in my moleskine as a rationalization about why I haven’t been keeping my 1000 words/day goal. There’s some truth behind the statement, but it’s not the whole story. The truth is that I’m a bit lost with this blog and it’s easier to do nothing than it is to keep stumbling around.

Engineering the Uncommon (EtU) was started as an outlet for my writing and also as a chronicle of my journey to change my life. It’s not meant to be a journal; I have a separate place where I keep some of my more personal thoughts. But it is a place where I am honest and open about the progress, or lack thereof, that I’m making. So it is a journal in some sense.

One of the bloggers I follow closely, [Jeff Goins](http://goinswriter.com/), is a prolific in his output and a great inspiration. He ships content regularly and is an excellent motivator for both my writing and for my desire to change and grow. I don’t aspire, necessarily, to be like him with this blog. I believe that I have a lot to learn about turning pro as a blogger, and I’m just improving my chops right now. I’m okay with that.

I have other ideas for what I want to do when I do go fully ‘pro’. Whether that’s after grad school or some time before, I’m not sure. But I know I’m not a pro on this blog. I’m cool with that too.

This site is my home base. A place where I can post random things and feel comfortable doing it. The URL is my name and that, by definition, means the content gets to be random because I’m random. But I don’t want to just make noise. For it to be interesting to whoever reads it, and for me to write it, it needs to add some value to the world.

My journal can be solipsistic, this can’t. Eventually I want to have influence in the blogosphere, but for now I’m okay floating along at a lower level. But it still has to add value. In that I’m certain.

There’s some ideas floating around in my head about how to improve this blog, but it’s a process I want to do slowly. Like most people, I have a tendency to take on too much at once. When that happens, I start rationalizing why I’m not doing things, like writing. For this to become a habit, small steps are needed. Trying to be [Seth Godin](http://sethgodin.typepad.com/) right off the bat is a recipe for disaster.

A year from now, things will be very different. A lot happens in a year. But things only happen when shit gets done day by day and bird by bird.

Even if you can’t see beyond your headlights, you can still make the journey by going where you can see. (Paraphrased from Anne Lamott’s _Bird by Bird)_