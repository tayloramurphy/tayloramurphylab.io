---
id: 398
title: Viva Colombia!
date: 2013-08-26T20:21:50+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=398
permalink: /viva-colombia/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Relationships
tags:
  - colombia
---
_For about a month now I’ve thought about how I want to write about Colombia. Originally I tried to do it in the form of momentos, similar to what I did for Bonnaroo. Then I tried to parse out particular stories that might be worth sharing in a longer format. Failing those ideas I simply didn’t write about it at all._

_The question of what was worth writing kept coming to mind. What do I want to share with people on my blog and how do I want to share it? Do I go through every detail or just some of the highlights? Would it be interesting to do a little research and share some of that information or is plain storytelling better?_

_Plagued by these questions I did what any lazy person does: something else. Eventually that nagging feeling of needing to write something about my experience became too much and so here I am writing that proverbial something about my time in Colombia. What follows may or may not be interesting but at least it’s written and available to you, dear reader. _

&#8211;

Typically when you hear about Colombia your mind creates pictures. These often take the form of white powder and violence. Jokes abound about the prevalence of drugs in Colombia and the violence that has plagued that region for some time. Up until recently, the FARC was a major force in the country. Peace has started to spread throughout the area and Colombia has been claimed to no longer be the greatest producer of cocaine in the world.

So it’s to be somewhat expected that my experience with the country is far from the rampant fantasy of uninformed imagination. My girlfriend, Lindsay, and I went on July 1<sup>st</sup> to Bogota. 5 days later we would be attending the wedding ceremony of two of my best friends and 4 days after that we would be heading home.

Upon landing, the immediate feeling I had was “I’m in a different world.” Indeed, entering another country is like going to another world, albeit one where everyone pretty much looks the same but speaks in a different manner and has different customs and beliefs. Travel, for me, is a great joy because of the newness of everything. The people, sounds, smells, and sights are completely foreign. It even feels different because you’re not yet comfortable in the new place, with established routines and ways of doing things.

The feeling of going to a new country is distinct from traveling to a new city within your own country. There are different sights, sounds, smells, and new people in a different city but it’s still within the boundaries of your “homeland” and is objectively different than traveling to a foreign country. No passport is needed to go two thousand miles in the United States and the security is less stringent. (This is one reason why travel in the United States is great and is still among my favorite things).

For being a world away, the travel time to Bogota is short. It is still in the same time-zone as Nashville and feels more like a hop and a skip than a jump away. Even with extended layovers and arriving late in the evening, it wasn’t a relatively easy trip.

We met the bride and groom and some of her family outside of baggage claim and were whisked away to her childhood home. The car we took, driven by her sister’s boyfriend, was adorably small. It is in this first encounter that we were reminded of the differences in our countries. Countries other than the United States are acutely aware of the their own space limitations. Even if we exclude Alaska, the US of A is still a large country and it shows in how we view space. We drive large vehicles, have huge homes, and our portion sizes are similarly massive.  Space is at premium in Colombia and so their creations are similarly aware of the limitations. (Though, the influence of our size fetish is evident everywhere.)

Her home accomadates her parents, sister, and two brothers and seems to be more of an apartment than a separate home, though that is not a knock against it in the least. With the space they have the family has created a warm and welcoming atmosphere. Lindsay and I were welcomed as family and fell asleep on the couch and floor feeling safe and comfortable.

&#8211;

A new day brought with it new excitement: we moved into the apartments where we were staying with 7 other friends. For about $30 per person per day, 9 of us stayed in fully furnished apartments. Lindsay and I quickly claimed the “Master” bedroom which had a large walk-in closet, generously sized bathroom, and more than enough comfort for the two of us. This was our home for the rest of the trip and it served as a great base for every excursion.

The apartments were in a nicer neighborhood within Bogota. The ‘Chico’ area has many amenities within walking distance including a grocery store, which we utilized heavily, and a Burger King which was visited a few times (more for the fountain soda than anything). On the first full day in the city, we walked with our friend Stijn to find a very Colombian restaurant for a snack. Our snack ended up becoming a full-fledged meal whereupon I ordered a Bandeja paisa which consisted of some kind of sausage, fried pork skin, rice, beans, plantain, avocado, ground beef (or pork?), and an unscrambled egg. It was delicious and, along with the Coca-Cola Light, served as a baseline for the kinds and size of meals we should expect while in Colombia.

A note on the portion sizes in Colombia. To me, the portions weren’t large in the same way they are in the United States. We in the USA are obsessed about larger portions that are homogenous in nature. Bigger burgers, bigger fries, bigger drinks, and even bigger salads. Take what you want and magnify it by 2-3 times. In Colombia I found there were several times when I couldn’t finish all that I was given but it was due to the huge variety of food present and not the portion of each offering. I wasn’t given a 24 oz steak, but I was given 8 ounces each of beef, ribs, and chicken. It was similar and yet wholly different.

Later that evening we met up with the rest of the group, Margarita, her siblings and their significant others, to go out on the town. We went to a bar-club combination for which I lack the word to singularly describe it. It was three or four stories of restaurant with different bars and dance areas on each story. Each floor had different themes related to heaven and hell (purgatory, etc.). Randomly throughout the place were buckets with fruit in them and we were encourage to take what we wanted. It was an odd thing to see but everybody in our group immediately loved it.

The place wasn’t crowded and we made it a party. We danced, drank, and laughed and had a raucous good time. Normally, the place would have been crowded with bodies wall to wall but that night it was just our group. We had the floor to ourselves and made the most of it. Not caring what anyone thought and living that moment as best we could.

&#8211;

The next day four days were jam-packed with activities leading up to the wedding. Jason’s family was extremely generous and took us in a large bus to numerous tourist-y activities throughout the city. If the previous night was any indication about how much time the group was going to spend together, these next few days confirmed it.

On Wednesday we took a mini-bus and gathered both families for a giant meal. We were headed to the Cathedral de Sal (Salt Cathedral) which is on the outskirts of the city. Again, I managed to order the bandeja paisa, but it was even better this time because we enjoyed numerous juices and a local music group. The cathedral surprised me because I didn’t realize how much of a church it would actually be. They had several areas where mass would be held along with the stations of the cross lining the main hallway. Thankfully, it was underground so Lindsay and I didn’t have to worry too much about being struck down by lighting.

We finished the tour and spent some time out in the main area looking over the city and watching the numerous homeless dogs. After a bouncy bus ride home, the men got ready for the bachelor party.

By any account this was a very tame bachelor party but it was still a lot of fun. We went bowling, made Jason do some silly things, and had good food and beer. It was quality time spent with friends and family and it suited Jason perfectly.

Thursday was a day full of activity. Our first stop was one of our favorites: Monseratte. This is the highest point in Bogota and, at over 10,000 feet, it provides a panoramic view of the city. There’s a church on top of the mountain and it is a popular destination for both pilgrims and tourists alike. Interestingly, some of the locals believe there is a “Curse of Monseratte” whereby dating couples who visit are cursed and the relationship will not last. Fortunately for me, superstition is not something I subscribe to.

Lunch that day was supposed to be an enticing restaurant known as Crepes Y Waffles. Unfortunately, it was crowded (because it was probably so damn delicious) so we had to settle for the food court. After being ushered from the food court we then went to the Museo del Oro (Gold Museum) to check out some of the artifacts of the city. At this point, Lindsay and I were feeling tired of being shuttled around like middle schoolers and decided to bail and find our own way back to the apartments.

For us, travel is about exploring and finding your own things to do. Tourist attractions do interest us, but less so than the location itself. Some of the most fun I’ve had traveling in the past has been when I’m lost in the city trying to find my way back home. This was true in China and France and it was true in Bogota. Thankfully, I have a girlfriend who agrees with me and likes to explore. Had we been in Bogota for no other reason than just to visit we probably would have seen some of the tourist attractions (namely Monseratte) but the urgency wouldn’t have been there. The fact is, you can’t see and do everything when you visit a new destination. The tourist attractions are fun but they are ultimately manufactured, curated, and limited (to an extent). Creating your own experiences by exploration is what makes travel fun. Misadventure is still adventure and is nothing to shy from.

Friday was a day of rest for Lindsay and me. We were tired of shuttling around with the group and we wanted to relax in our own way. We found a little coffee shop to have breakfast in and spend some time reading (one of our favorite activities). We then relaxed at the apartment for a bit and watched _The Emperor’s New Groove_. We needed a relaxing day to gear up for the main events: the rehearsal dinner and the wedding.

The rehearsal dinner was a prelude to the enormous fun that would occur the next night. It’s not a tradition in Colombia to have a rehearsal dinner, but since the groom was American they imported it. Jason’s family wanted to have one and so they made it happen in one of the nicest areas of town. We ate at Case Vieja and had an extraordinary meal which was punctuated with a Mariachi band, beautiful speeches, and great conversation.

&#8211;

This good time naturally extended to the main event of the trip on Saturday. I&#8217;m not engaging in hyperbole when I say that this wedding was the best one I&#8217;ve ever been to. There are more things than I can mention about why this wedding was awesome, but I&#8217;ll try and go through some of them.

The location was perfect. The property was well-manicured and the weather was better than you could have asked for. It was comfortable with just a slight chill to the air; partly cloudy with not a drop of rain during the day. The church was a cozy, one-room affair that couldn&#8217;t hold everybody that wanted to attend. Our group of friends was involved with several parts of the wedding meaning we were all able to sit inside. The bride wanted us to sing a song as she walked down the aisle. We were more than happy to help in this way and worked hard all week on singing Train&#8217;s &#8216;Marry Me&#8217;. I played guitar with Margarita&#8217;s brothers and everybody sang.

Concurrently, Lindsay and I were also the unofficial videographers for the event. I managed to attach our video camera to one of the chandeliers to capture the entire ceremony. Lindsay took video at the beginning and end with her phone. All in all, it made us feel like we were a part of the happy moment and contributed something positive.

After the vows and the lengthy mass, the two were married and we started with the festivities. By far, this was the best wedding reception we had ever been to. Before the dancing and the food, the bride and groom and family all have to take pictures before makeup and outfits get mussed up. It&#8217;s a tradition to have your guests lounge about while you get all the pictures you need before doing your first dance as a married couple and all the traditions of Western marriage.

After about 30 to 45 minutes we made our way inside and got the real party started. The wonderful thing about a wedding in Bogota is that it really is a _party_. After the bride-groom, mother-son, and father-daughter dances were done, we were all given tiny shot glasses with Aguardiente in them. Then we put the attached string over our head, like a necklace, and started acting a fool. The next few hours were then a wonderful, joyous blur of movement, song, and laughter.

Lindsay and I were working on getting short, personal statements for Jason and Margarita from as many people as we could throughout the wedding. There were probably some people we missed, but we got the important ones: family and close friends.

In between we also boogied on the floor. The band that was playing had about seven people, an array of lights, real instruments, and a smoke machine. They turned off the lights in the hall and everybody, and I do mean everybody, got up. Some of the dances, well, _most_ of the dances, were unknown to us Americans. There was one dance where all the girls got on one side and the guys got on the other. Each side took turns singing and dancing and it was a competition of sorts. The girls would advance on the guys then the guys would advance on the girls. It was fun, provocative, and we had no clue what we were doing.

At some point, waiters came through the crowd handing out party favors like hats, glasses, face ties, and leis. Almost everybody grabbed something and now, in addition to the dancing and singing, people were wearing ridiculous accouterments. Periodically we had to step a way to get rest and drink water (though in my case, when I asked for water I was given whiskey). There was _a lot_ of dancing and we partied hearty.

Eventually it was time for the bride and groom to leave and we saw them off with a shower of bubbles. Acting as cinematographer once again, I was running around making sure I had the shots I wanted, including them getting into the car. Jason had never driven in Bogota, which meant Margarita had to drive which was hilarious with her dress and all it’s fabric. I can’t even imagine working the pedals and shifting around all that fabric.

The bride and groom were gone and everybody went back to the hotel. The next day the rest of the group left after hugs and goodbyes. Then it was just me and Lindsay for the next few days.

&#8211;

Sunday came and Lindsay and I decided to take our time. After 6 days of hustle and bustle, we wanted to go at our pace and make our own decisions. She and I like wandering a city and finding things that you might not normally see as a tourist who’s main goal is to ‘see all the sights’. We made it our mission to find the Crepes Y Waffles restaurant again. We knew it was downtown. But where exactly was another matter.

We spent several hours tracking this place down. Lindsay thought it was near an Exito superstore (it wasn’t) but we were within about thirty blocks of it. We knew if just kept heading in the direction where the road numbers decrease, we’d find it. At some point we stopped and asked a random guy “Donde esta crepes y waffles restaurante?” He told us back at the Exito. (It still wasn’t there).

Committed to our dream we continued searching. We walked and walked, saw some beautiful architecture, noticed a few dogs, people-watched as folks went about their business on a Sunday morning / early afternoon. We finally got to an intersection that looked almost familiar. We wouldn’t have been able to tell you what felt familiar about it but somewhere in our unconscious was a little niggling feeling that we were close. We decided to cross the street and go up some steps. Lo and behold! We found the mall and entered the glorious air conditioning. We sat at a table and proceeded to order the most decadent looking crepes and waffles for each of us. In total we ordered close to four full entrees and we ate every bit of it. Nutella crepes were just as good as they sound.

After lunch-breakfast we walked around the city a bit more. We found a park close to the mall and explored it. People were watching their kids play soccer, others were racing their tricycles down a hill. Still more were just laying on the grass enjoying the beautiful weather. We found a nice spot and read for an hour or so, only moving to get more sun. Eventually we headed back to the apartment

We had decided to cook dinner that evening and have a nice date on the roof. A short trip to the local grocer and we came back with stuff for spaghetti and wine. Italian style food is quick and easy and very satisfying. We made the food, grabbed some glasses, wine, and our Jambox and headed to the roof.

The air was cool that night and the city was alive. The lights obscured some of the stars but the city itself seemed to be a star glowing from all directions. We had the roof to ourself that evening and made the best of it. We sat on the edge of the building and let our feet dangle over the sidewalk. We danced to music, drank the wine, and held each other. It was a sublime evening that hasn’t been topped. We both went to bed that night happy and filled, ready to explore the country some more.

&#8211;

The next morning we decided to rent a vehicle. We enjoyed the Transmilenio and we knew a bus could take us pretty much anywhere we wanted, even outside the city, but there&#8217;s something extremely freeing about having your own transportation. Buses won&#8217;t stop on the side of the road and wait while you take a picture or explore some new avenue. They&#8217;ve got a schedule to keep and your shenanigans don&#8217;t mean anything to them.

As it turned out, renting a car was the biggest hassle of the trip, thought it wasn’t the fault of the company. Naively we had assumed that we could walk into any rental place and pick up a car right then. Not realizing that Bogota is a _large_ city, the first place we went to told us that they didn’t have any cars for rent today at their location. Or in all of Bogota.

Well crap.

So we went back to the hotel and tried to find something that we could pick up in the next hour. Avis had a few cars at the airport we could pick up which, actually, would work out well because we could just drive the rental to the airport and drop it off right before our flight home. Great plan, right?

Not so fast. As it turns out, the Avis isn’t actually at the airport. It’s on the same road as the airport, probably a 10-minute walk from the actual terminal. We didn’t know this and evidently neither did any of the taxi drivers. Our driver dropped us at the international terminal and we wandered around for a bit trying to find the Avis with no luck. At some point we found a very friendly guy, who I nicknamed Miguel, who was very happy to help us. He told us approximately where the place was and said any taxi driver should know where we’re going.

False. The next taxi we took dropped us at the domestic terminal which, while closer to the Avis location, wasn’t where we needed to be. We again walked around and asked people if they new where the Avis was but nobody was able to help us. None of the other car rental places knew where Avis was and nobody seemed to understand our ‘aquilar de coche’ requests. We managed to get an English-speaking Avis representative on the phone just as we ran out of money for the payphone. Unsure of where to go and unable to get a wifi signal, we proceeded to walk back to the international terminal to speak with Miguel. Along the way I stepped in a puddle that appeared to be forming from the runoff of a port-a-potty which instantly soaked through my shoe and sock, coating my foot in a sort of shitty glaze.

As we approached Miguel he turned to us with a smile on his face, ready to help us. Once he recognized who we were his face turned to shock and he grew concerned. _What happened?_ We relayed our story and he incredulously asked “Why would they DO that?” We, of course, didn’t know and said as much.

Once more we got in a taxi but this time Miguel came with us and translated our request. This time it seemed to stick. The taxi driver took us right to the random gas station where this Avis was located and we finally started the process of getting our car. Overall, from the time we first left our apartment to head to the first rental place to the time we actually sat down in our new car and got on our way was about three hours.

It was one of those times when Lindsay and I were both alternately frustrated and had to help the other calm down. As Neil Peart, one of my favorite writers and _the_ best drummer in the world, says: “Adventures suck while you’re having them.” And this one did kind of suck. Eventually, though, it ended and we got the car and went on with our day. Now it’s a story that we can laugh at. The key for us to get thru that annoying time was to realize that yes, what was happening was shitty, but that we’d eventually get through it and would be able to laugh about it later.

I navigated the car to the Bogota streets and we set out on the adventure we had originally planned.

&#8211;

Our first goal was to get to the town of Suesca to visit the ‘Rocas de Suesca’, or the ‘Rocks of Suesca’. There was supposed to be some rock climbing there and beautiful scenery. It was maybe an hour outside of town and seemed like the perfect day trip. Of course, before we could get there I had to quickly learn how to drive in the city. I’m naturally a somewhat aggressive driver and I quickly figured out that if I drive aggressively all the time and have a high expectation that _everyone_ would cut me off, then I was ok. Horns were almost always used as a signal to say “I’m here” or “I’m coming over”. If people cut you off or if you cut someone off it was ok. That’s just how it was done. So I quickly became aggressive without the anger and learned to go with the flow while still pushing my car to places it needed to go. It was a weird experience that made me feel like I could drive anywhere else on the planet.

Eventually we made it out of the city after a few tolls, a bunch of potholes, and some close calls. The car we had was a tiny Chevy Spark that had 5 gears and about four horsepower. I was actually surprised it had five gears (six including reverse) because many of the cars I saw in the city only had four. Most of them probably never got out of the city, especially if they were taxis, so it made sense that they wouldn’t have a higher gear.

Once we were out of the city the scenery changed dramatically. The hustle and bustle of the city was replaced by a more bucolic mood. Buildings were smaller, dirtier, and further apart, but people were still out and about. The geography changed as well. The mountains which dominated the east side of the city were replaced by smaller ranges on either side of the highway. We saw more farmland and greenery. The roads were very well paved and it made for an enjoyable trip.

One of the best experiences of our driving was realizing how disconnected we were. In the United States Lindsay and I both have 3G service on our phones. This means we can pull up in an instant where we are on a map and get directions to where we need to be. We didn’t have that here. I had drawn out a map as best I could before we left, but once we got into the thick of it we were on our own. This was both fun and frustrating. Frustrating because we never actually found Rocas de Suesca. We drove around the tiny little city but couldn’t find it, whether through a lack of markings or a lack of understanding about where it actually was. But we still made the best of it.

We took the main road leading into Suesca which dumped us into a criss-cross, checkerboard pattern of roads. There were probably 10 streets going north and south and 10 going east and west. Probably way less than that. Leading out of the city on the opposite side of where we entered were three roads. We decided to take them and see where they went. Road one led us to road two which circled back to the city. Road three led us on an adventure in the truest form of the word.

At this point, the roads weren’t paved. At all. We were in this tiny-ass car offroading like we were in a 4&#215;4. It was a lot of fun and the entire trip we somehow managed not to blow a tire or lose a piece of the car. This second road we tried led us on a 3 hour journey to some hidden locales that aren’t on any tourist map. Without the aid of any maps we managed to drive about 100 miles of backroads to the middle of nowhere. We found hidden points where we would stop the car to take in the view. We drove around a huge lake  surrounded by mountains. We nearly jumped the car over some bumps because, as we realized, if you don’t pay attention to the roads in Colombia, then Colombia fucks you.

After driving for a few hours we decided to turn back around and head back home. We stopped at a small gas station in some random town and asked if the cashier had any maps.

“La mapa?”

“No entiendo.”

We weren’t really sure how we could screw up asking if she had a map but somehow we’d managed it. We bought some Coca-Cola light and headed back the way we came, over boucing roads, winding climbs, and scenic vistas. It was night when we were heading back and something about the beauty of the day inspired me to randomly stop on the side of the highway. Traffic was a bit heavy but we climbed the embankment and laid on the grass, looking at the stars. The air was chilly and we held each other as we looked up on the stars reflecting on all that had happened during the day. The traffic roared by as if a million miles away and we felt like we were floating in space.

&#8211;

The next day our car was under restriction. Bogota has an odd policy about when people can drive in the city. In an effort to curb congestion and pollution, certain cars are not allowed to drive within the city during certain days. For example, cars with a license plate ending in an even number might not be able to drive between 7 and 10 AM nor 3 to 7 PM on Tuesdays and Thursdays. Odd plate numbers would be other days. This does several things that perhaps the planners didn’t expect or just didn’t care about.

One, tourists are hit with these restrictions too. Our car was restricted on Tuesdays so if we wanted to get out of the city that day without getting a ticket we would have to do so very early in the morning. It’s probably a small number of tourists who are renting cars in Bogota, but we were definitely affected and I’m sure we weren’t alone.

It seems like businesses who need to transport good and taxi drivers would be affected by this as well. These are people that earn their living by driving. To curb their activities for a good portion of the day seems like theft. I’m sure a common way around this, particularly for the wealthier companies and people, is to just buy two vehicles. Operate which ever one is allowed to freely roam the streets that day. It doesn’t curb pollution or congestion any and it seems like it would put more money in the pockets of people who would stand to profit from the sale and registration of more vehicles.

Crazy enough, people who have these cars are still driving, especially outside the city. But because they’re barred from entering city limits prior to the appointed time, people will just park on the side of the road and wait for 7 PM to strike. Then, everyone floors it at once and tries to get onto the highway. On Monday, Lindsay and I were driving back into the city we were in the thick of the cars as the clock struck 7. It was absolute chaos. Folks were going highway speeds and having to swerve and slam on the brakes to allow people to jump on the interstate. It was nuts.

So, it was amidst this odd policy that Lindsay and I found ourselves awake at 5 in the morning on Tuesday, leaving the city. Our destination for the day was the Lugana de Guatavita which is home to the story of El Dorado.

Similar to our travels to Suesca the previous day, we caromed along to the town of Sesquile on poorly paved roads and streets without markings. It was particularly gorgeous because of the early morning dew and fog that hadn’t been burned off by the sun. Fortunately for us, the road leading to Guatavita was better marked than for the Rocas, so we were able to drive directly to our destination. At 7:30 in the morning we parked the car and went to the front gate only to find it barred. Some soldiers came out of the woods, looked at us, laughed a bit, and then said “nueve”.

Awesome. Another consequence of Bogota’s restrictions: we were super early. We did the only sensible think we could think to do: read in the car and sleep a bit. I managed to finish a wonderful book (Lexicon) and get a little shut eye.

At nine o’clock we were able to enter the park. We were the first and only ones there which made for a very unique experience. The path to the lagoon was less than a mile, though much of it was uphill. The sun was still low in the sky and it was overcast with a slight drizzle. Lindsay and I love the outdoors and were unperturbed by the excess moisture in the air. We climbed and climbed, got out of breath, stopped to rest, climbed some more and then finally came on the laguna.

It’s a breath-taking site. High up in the mountain is what appears to be a crater. Inside this crater is a a lagoon. It’s probably a couple hundred yards across and the water level is quite low compared to where the edge of the crater is. History reports that several people have attempted to drain the lake to find hidden gold. In the 1500s a group succeeded in lowering the level of the lake by about 10 feet using a human chain. Later that same century, a massive crevice was cut into the side of the crater which drained about 70 feet of water. Up until the 19<sup>th</sup> century folks were still trying to get at the alleged treasure and built a tunnel that would drain the lake. Out of all these expeditions only a small amount of treasure was found and certainly not enough to warrant the amount of destruction that occurred.

What we found was a crater that had flora on every available surface, right up to the water’s edge. The crevice is clearly visible and it most likely acts as a temporary drain when the water level gets too high. It’s not clear if the water level is regulated by someone, but it is obvious that people are working to protect this natural wonder. There are several viewing platforms along the edge of the crater which made for awesome views. Because we were the only ones there, Lindsay and I broke the rules a few times by hopping the fence and sitting on the slope of the crater. It was where we like to be: out in nature being fully present in the moment.

We followed the path and eventually were stopped by a guard who popped out of the bushes like a weasel. Immediately we were sure we had gotten caught and were about to be taken to Colombian jail. He told us to turn back but his reasoning wasn’t clear. This dude was grumpy for it only being about 9:30 in the morning. He gesticulated aggressively and ushered us away. We knew he radioed down to his buddies about our transgressions. Consigned to our fate we leisurely made our way down the mountain and back to the main area. A large group, which appeared to be a class trip, had arrived via bus and was now taking up most of the entrance space. Nobody accosted us on our way out and all the guards ignored us, so we booked it to the car and sped away, sure we had just escaped prison time. Clear of the are we laughed for a good while and made our way down the mountain.

Since we were quite tired we decided to head back to the city and get a nap. On our way home the previous day we had seen in the distance what appeared to be the Taj Mahal. There was also a statue of a dude and the whole area seemed to be walled off. On a whim that Tuesday, I took a wicked turn and we decided to go check it out. The road parallel to the amusement park (we didn’t know what else to call it) was terribly full of potholes, but the wall was wrought iron and we were able to see inside. It indeed looked like an amusement park. There appeared to be a train that circumnavigated the boundary and very colorful buildings, rides, and decorations were everywhere. There appeared to be no unifying theme, just some haphazardly thrown together things that looked neat.

With a little Googling you can find out that this place is called Parque Jaime Duque, built by, who else, Jaime Duque. Basically, the guy was one of the first pilots for Avianca and deciced to build a theme park where families could have a lot of fun and be exposed to cultures from around the world. It wasn’t open when we passed by, but it sure did make for an interesting view. Perhaps if we’re ever in Bogota we’ll have to go there.

On our way back to the city we had to stop at the largest fast food restaurant we’ve ever seen: El Corral. This is basically the Colombian equivalent of McDonald’s or Burger King and you can tell the owner of this company has taken a few trips to the United States to see how we fat Americans do fast food. It was an odd experience because it’s a uniquely Colombian experience shoved into an American mold. The food was good, if a bit fatty, and we can now say we’d had local fast food. Combine fatty foods with an early morning and we were in a food coma back at the apartment

Since it was our last night in Colombia, Lindsay and I decided to go on a fancy date. There were several restaurants near where the rehearsal dinner was held that looked absolutely amazing. We both were dolled up and secured a taxi from our friend at the security desk downstairs. The ride to the fancy district was one of the scariest of my life. This dude, we knew, was fucking with the silly Americans by zipping around corners, cutting everybody and their mother off, and generally trying to kill us by inducing cardiac arrest. We got out of the taxi and I felt like leaping to the ground while shouting “Land! Land!”. We grudgingly paid our fare and went on our way.

After a bit of indecision we decided on an Italian place for dinner. It was one of the best dinners we’d ever had for several reasons: the best bottle of wine we’d ever had, the most decadent bread with impeccable olive oil, an amazing entree for both of us, and Lindsay’s favorite artist, Ryan Adams, was playing on the radio. Pictures were taken, the conversation flowed, and what had started as the scariest night ever ending up being the most perfect.

&#8211;

Our flight the next morning was at 1 PM and it should have been a leisurely day: pack, drive to a car wash, drive to the airport, and say goodbye to our home for the past few days. It was anything but.

Originally we were going to walk around the city and find a nice place to have breakfast. Something in both of us hinted that might not be a good idea, so we just finished what food we had at the apartment, packed, and loaded everything in the car by 10 AM. We had three hours until our flight lifts off, so that should’ve be good enough to make it in time. Of course, the reality of things is sometimes far from the theory.

Immediately we were lost. I had printed out some directions for us that included a detour to a car wash that was “on the way”. We never made it to that car wash and instead had to abort and pull into some random one. But you can’t just pull into a place as easy as you want. It’s never like that.

The thing about Bogota is that making a left turn anywhere in the city is nigh impossible. Perhaps this just my confirmation bias coming into play, but I’ll be damned if I took more than 2 or 3 left turns the entire time I was there. Because of the Transmilenio, which runs not only parallel to the major roads but _in the center of the major roads,_ you very often can’t turn left. There are very few protected left turns in the city. Otherwise, if you want to make a left turn, you have to make three rights. Every. Single. Time.

This was the case for many of our mishaps on the way to the airport. We passed by this random car wash and so had to make some wicked Nascar maneuvers to get to the place. 3 right turns. But we made it and then proceeded to have the most detailed carwash a car has ever seen. Because most people in Colombia don’t speak English (and rightfully so) I couldn’t communicate what sort of wash I wanted. I knew we had about 31k pesos and there was a wash that cost 25k pesos. Perfect, I thought. So I kept telling the guy vente-cinco! Vente-cinco!

But he was indicating that was the one where they raise your car to the heavens to wash underneath it. I didn’t want that. But I kept saying vente-cinco anyways. Tired of arguing with my ignorant ass he finally just took my money and told the guys to get to work.

Remember, we’re driving a rental car that’s about the size of a matchbox. And we’d taken it off roading. They proceeded to put this car on a rack and lift it about 10 feet in the air. A gaggle of employees rushes over and pounds the car with soap, water, and hand rags. Once they’ve scrubbed the exhaust pipe, alternator, and oil filters to a squeaky finish, they drive the car over to another area where they proceed to clean the _interior_ of the car. By hand. They washed the gaskets and got in between the seats. Somebody polished the gear shifter. The product of my _vente-cinco_ request was a car that was cleaner than when it came out of the factory. Lindsay and I were laughing the entire time at our folly. At least we wouldn’t have to worry about the Avis rental place wondering what we did with their car. Nothing fell off while it was being cleaned so we considered it a successful day.

Back on the road, in probably the cleanest car in Colombia, we tried to make our way to the airport. Normally, at least in the US, the signs hanging over the roads tell you where you’re going. This isn’t always the case in Bogota, at least that’s how it felt to us. We’d be looking for Calle 26, for example, and see signs that say Calle 43 and then Calle 39. Perfect, we’d think. We’re headed in the right direction. The it would jump to Calle 18 and then Calle 16 and we’d somehow missed our exit. Then it was a series of three right hand turns for us to get back going the way we came. In some cases it was a highly illegal “fuck this, I’m turning left” maneuver that would get us back on track. It took us over two hours to get to a place that should have taken about a half hour by taxi.

We return the car (after a little brouhaha with the Avis guy for a missing gas cap) and make it to the airport. We narrowly make it on the plane after almost missing it because our food was taking too long. Finally sitting on the plane together, Lindsay and I laugh from the hilarity of the day. Too many things conspired to keep us in this wonderful country. As the plane lifted from the runway I broke the rules and filmed our ascent, one last view of a place we had fallen in love with.

&#8211;

Bogota, like many places in the world, is a city full of life and love. Rich and poor, fun and scary. Our time there was the experience of a lifetime, filled with more memories than I can write about (and that’s saying something for a story which has nearly eight thousand words). It is the home of some of my best friends and it feels like a second home to me. The people we met were nothing but nice and generous and the city and country was good to us. It was with a heavy heart that we left, having to say goodbye to something we loved. It was the trip of a lifetime that lasted forever and was over in a moment. Sure to be overshadowed by the wonder of the future but never forgotten as a joy in the past.

Viva Colombia!