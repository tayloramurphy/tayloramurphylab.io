---
id: 395
title: How to Save a Life
date: 2013-08-09T12:02:47+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=395
permalink: /how-to-save-a-life/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Learning
---
<p dir="ltr">
  The other night I was up way too late wading through the miasma that is the internet. In some random subreddit on Reddit.com, I found an old post about a guy who has muscular dystrophy.
</p>

<p dir="ltr">
  In the video, he had just fallen from his chair and was stuck on the ground. Then he peed himself. It wasn’t one of his finer moments.
</p>

<p dir="ltr">
  While laying on the ground he decided to record a <a href="http://www.youtube.com/watch?v=h90ONt1Q2eI">video</a>. For someone who was stuck on the ground covered in his own urine, he had a remarkably upbeat attitude.
</p>

<p dir="ltr">
  This is the life <a href="http://www.mikeywheels.com/">Mike Oliveri</a> chose. He wanted to live on his own in LA. There was always a possibility that this was going to happen. But he chose to live instead of languishing in some assisted living facility.
</p>

<p dir="ltr">
  I went to his <a href="http://www.youtube.com/user/mikeyoliveri?feature=watch">YouTube page</a> to see what else he has to say. As it turns out, the guy has been documenting the past 4 years of his life on YouTube. He has close to 300 videos on his channel but each video probably averages about 300 views each. In other words, not many people are looking at his stuff.
</p>

<p dir="ltr">
  His most recent video was for an <a href="http://www.indiegogo.com/projects/trip-to-minnesota-clinic-to-save-a-life">Indiegogo campaign</a>. He refuses to give up and just wait until his body gives up. He’s actively trying to seek help and he only found one place in the country that would help him: the MAYO clinic in Minnesota.
</p>

Because he’s not able to do a lot of work, and due to his disease worsening over the years, he turned to the internet to ask for help to get to Minnesota. He’s asking for $25,000 dollars to get from California to Minnesota. It’s part of a flex fund campaign so no matter how much money is donated, he’ll get it. Part of the money is going to travel, and part to the people who are helping him along the way.

<p dir="ltr">
  I checked out his other videos and this guy is legit. At first when you see something like this your natural inclination is to be skeptical. We’ve all heard the stories of people scamming others and having fake diseases. Well fortunately (sort of) for Mike, he’s got a very visible disease. There’s no faking the fact that he has almost no muscle left in his body.
</p>

<p dir="ltr">
  Many of his other videos are just him talking to the camera and being real. Talking about the struggles, the sickness that comes from being weak, the daily challenge of remaining positive. He made a choice to live on his own because he wanted to live and not just exist.
</p>

<p dir="ltr">
  I noticed he’s also religious, which bothers me not a bit. I’m as atheist as they come but I don’t think it’s a bad thing for him to have a belief in some deity. He’s the kind of religious person I wish more people were: extremely positive and pushing to make his life better. His belief gives him strength and who am I to judge him for that? His resiliency and commitment to living a good and strong life is very admirable.
</p>

With my fears of scamming assuaged I decided to make a donation. His story moved me to tears because he gave me a reference point that I had lost.

<p dir="ltr">
  Here is somebody who has a better attitude than I do and is dealing with more challenges than I am. It gives you some perspective on daily challenges. You realize how freaking fortunate you really are and you become acutely aware of how much you’re pissing away your potential.
</p>

<p dir="ltr">
  So with that, I donated some money to get this guy to Minnesota. And I decided to write a blog post about it to help spread the word. Soon after donating I got a message from a friend of his (maybe his girlfriend?) thanking me for the gift. They truly appreciate it. I <a href="https://twitter.com/Very_accessible">tweeted</a> back and forth with Mike too and he sincerely expressed his thanks.
</p>

<p dir="ltr">
  Thinking about someone else and helping other people changes the way you perceive things. Instead of being such an egoist you start to look outwards at how you can help other people. It’s too easy nowadays to constantly think about what’s wrong in our life. What am I lacking? Why am I sad? Sometimes, you just need to roll your sleeves up and help some other people. Many of these questions just disappear when you start helping others.
</p>

<p dir="ltr">
  I encourage you to make a donation to Mike. 5, 10, 50 dollars. Any amount is helpful. Subscribe to his YouTube channel and check out what he has to say. Something tells me he’ll be able to teach you a thing or two.
</p>

<p dir="ltr">
  <a href="http://www.youtube.com/watch?v=-BcxAVv9LH0">Help Me Save My Life</a>
</p>

<p dir="ltr">