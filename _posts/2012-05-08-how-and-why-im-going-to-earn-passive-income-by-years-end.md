---
id: 142
title: 'How and why I&#8217;m going to earn passive income by year&#8217;s end'
date: 2012-05-08T21:21:19+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=142
permalink: /how-and-why-im-going-to-earn-passive-income-by-years-end/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Future
tags:
  - bgs
  - blog
  - development
  - dreams
  - earn
  - future
  - goals
  - grad
  - income
  - passive
  - pavlina
  - personal
  - steve
  - student
  - trailblazer
  - work
---
Are you familiar with [Steve Pavlina&#8217;s blog](http://www.stevepavlina.com/)? No? Well you should be.

He&#8217;s a bona fide personal development guru and he&#8217;s been a huge source of inspiration and motivation for me.

I recently finished reading his book &#8220;Personal Development for Smart People&#8221; and found myself nodding along with everything written. His blog is a veritable font of information dating back to 2005 and he still publishes regularly.

Currently, he’s doing a series of posts on [passive income](http://www.stevepavlina.com/blog/2012/05/commit-to-your-passive-income-goal/). You may be wondering what passive income is and the definition is quite obvious: income that is earned without any regular effort. For income to be deemed passive, it has to roll into your bank account with out active effort on your part.

This sounds very appealing, doesn&#8217;t it? Who wouldn&#8217;t want money just floating in without any effort on their part? This is possible, but I&#8217;m going to clear up some misconceptions first and then explain why this is something I want to achieve.

## **Passive income seems like an oxymoron&#8230;**

How can you earn income without doing any work? The answer is you can&#8217;t. Work is definitely involved. But it&#8217;s when you do the work that matters.

Active income is the trading of your time for money. Think of going to your job where you have set hours you have to work. While at the job, you are actively trading your time for the weekly paycheck.

But with passive income, that work is batched. You work very hard at the start to create something, or set up a system, that adds strong value to people&#8217;s lives. Then you unleash it on the world. People will then find your product or service and purchase it without you having to be involved. The money goes into your bank account, the customer is happy with their purchase, and all parties involved leave happier.

This concept was popularized by Tim Ferriss in the [4-Hour Workweek](http://www.fourhourworkweek.com/) and many people have had success earning a living entirely through passive income.

But make no mistake, every person who succeeded at this had to work it at it to create their situation. There&#8217;s no magic button that can be clicked to suddenly have money flooding your account. Life doesn&#8217;t work like that.

So are you still with me? Does passive income sound pretty appealing? I&#8217;ll bet it does, but if you&#8217;re like me, then you really have no clue how to make it a reality. This is where Steve comes in.

## **Follow The Leader**

Steve has offered to take hold us by the hand and show us how to do this absolutely free. He’s doing this through several posts on his blog. But this isn&#8217;t any traditional set of posts where he&#8217;s going to list bullet-style how to do it. He&#8217;s actually going to create a new source of passive income for himself from scratch.

He sincerely wants every person that reads his blog to achieve their goals of creating a new stream of low-maintenance income. The stream that he plans to create will be done in a transparent manner so as to help as many people as possible.

The first few posts have been about clearing up some of the misconceptions about what passive income is. Then we&#8217;ve moved on to the first step: declaring your goal.

## **What Do You Want?**

The difficult part of achieving a dream for many people is that there are no specifics attached to the dream. You may be saying, yea, “I’d love to have a passive stream of income”. Or maybe it&#8217;s even more vague, “I want to be earning more money by the end of the year”. Well how are you going to do that? How are you going to know when you&#8217;ve actually achieved your dream? Without very specific desires, you won&#8217;t be able to.

Steve has been adamant about getting us to be very specific about our financial goal. He even suggested some numbers for us: Create a passive stream of income that earns $500/month for a minimum of 5 years by the end of this year.

Now how&#8217;s that for a specific goal?

Originally, I was thinking smaller. Maybe $100. But when I got down to the part of the article where he suggested $500, I was like, well hell. Why not? It&#8217;s not that much more money. I fully believe it&#8217;s within the realm of possibility. It&#8217;s also not too outrageous. Saying I would want to create a $10000/month stream of income right off the bat would be very difficult. This first goal can be a stepping stone to greater earnings, but it&#8217;s always that first step that&#8217;s the hardest.

We were encouraged to post our goal in a visible place. For me, I actually wrote out the goal (according to his suggestion) and pasted it in front of my desk at home. That way every morning and evening I will see what I&#8217;m working towards.

<div id="attachment_145" style="width: 310px" class="wp-caption aligncenter">
  <a href="http://tayloramurphy.com/blog/wp-content/uploads/2012/05/photo2.jpg"><img class="size-medium wp-image-145" title="Passive income goal" src="http://tayloramurphy.com/blog/wp-content/uploads/2012/05/photo2-e1336530049983-300x224.jpg" alt="Audacious is sexy" width="300" height="224" srcset="http://tayloramurphy.com/blog/wp-content/uploads/2012/05/photo2-e1336530049983-300x224.jpg 300w, http://tayloramurphy.com/blog/wp-content/uploads/2012/05/photo2-e1336530049983-1024x764.jpg 1024w" sizes="(max-width: 300px) 100vw, 300px" /></a>
  
  <p class="wp-caption-text">
    My passive income goal
  </p>
</div>

The other part of making this public is writing about it on my blog. I could, theoretically, just work on this by myself and not tell anyone. But where&#8217;s the fun in that? By posting it on my blog, I&#8217;m bound to get a few questions. Maybe some people will support me. Maybe some will tell me to stop wasting my time. Either way, at least there&#8217;s a conversation about it that will give me some accountability.

This goal works well with the Trailblazer program I&#8217;m in as well as my goal to restart Better Grad Student by June 1. I think all of these can be bundled together and worked on concurrently. This is just extra motivation to make my dreams a reality.

So there we have it. I want to earn $500 per month in passive income by the end of this year. Crazy? Yes. Possible? Hell yes.

_**What about you? Do you want to earn some passive income? What&#8217;s keeping you from doing it?**_