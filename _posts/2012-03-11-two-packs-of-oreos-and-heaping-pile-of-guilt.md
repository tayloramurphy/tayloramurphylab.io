---
id: 85
title: Two packs of Oreos and a heaping pile of guilt
date: 2012-03-11T18:13:18+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=85
permalink: /two-packs-of-oreos-and-heaping-pile-of-guilt/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Emotions
tags:
  - negative
  - oreo
  - overeat
  - positive
  - recommit
  - refocus
---
Two. That’s how many packs of Oreos I ate, by myself, in the past week. I’m not proud of it. In fact, I actually regret it.

I went and bought those Oreos because I was feeling depressed and I thought that food would cheer me up. Intellectually, I knew it wouldn’t. I knew exactly how I would feel during and after eating those cookies. But I did it anyway. And, like I had predicted, I loved eating them, but felt like shit afterwards.

After the first time, I beat myself up about it. I reinforced the idea in my mind that I was weak and pathetic for succumbing to the base allure of gluttony. I pushed myself hard during the next workout to ‘make-up’ for my lapse in judgement. But then I did it again.

A few days later I bought another pack of Oreos and ate most of it too. But instead of beating myself up about it, I let it go. In the middle of munching I realized what I was doing and how this was going to make me feel. Instead of chastising myself, i just let it go. I threw the rest of the cookies in the trash, drank some water, and moved on with my life.

The next day I didn’t punish myself with an agonizing workout. I just made a note of how crappy I felt and recommitted to eating healthier and not buying that artificial food again. I did work out, but it had the goal of being for heatlh instead of for punishment. The next time I felt that urge to get some cookies, I told myself I would drink some water and have a piece of cucumber or tomato instead.

It seemed to work. Instead of scolding myself for screwing up and trying to completely suppress that desire, I tried to acknowledge it and refocus the energy.

Constantly pushing back on some negative behavior can have the unfortuante tendency of making it worse. It’s like constantly compressing a spring: you can push all you want, but all you’ve done is store up a ton of potential energy that’s waiting for a moment of weakness. When it finds that weak point, all that energy comes barreling out. Instead of pushing back, I pushed sideways into a new direction.

This has helped me to, first, not feel so guilty about screwing up. It happens, it will happen again, and making myself feel terrible isn’t going to keep it from happening. Second, it’s refocused the energy in a more positive direction. It’s a small step toward changing the habit by replacing the triggered action, eating oreos, with something better, eating cucumbers.

We recieve enough messages in life that we’re not good enough. Plenty, in fact, that we don’t need to add to it ourselves. I’ve learned to acknowledge what happened, try to adjust to avoid that behavior, and then go on with life. Life should be enjoyed and berating yourself all the time has no place in a joyful life.

_What are some behaviors that cause you to chastise yourself? Has that been effective?_