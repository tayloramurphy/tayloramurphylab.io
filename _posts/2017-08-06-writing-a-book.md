---
id: 466
title: Writing a Book
date: 2017-08-06T15:22:30+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=466
permalink: /writing-a-book/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Future
tags:
  - future
  - writing
---
At some point in my life, I&#8217;m actually going to write a book. I&#8217;ve said that for a while and it&#8217;s always felt nice and easy to say, but I mean it, I really want to.

I love reading books. Basically any time I finish a book, particularly if it was a good one, I think about whether I could have written that book or not. With a lot of fiction books there&#8217;s that feeling of, yeah, I could have written that. Many non-fiction books are seemingly out of reach if they&#8217;re tied deeply to personal experience or if an intense level of research went into writing them. Inevitably following that thought is my inner critic saying, well why haven&#8217;t you?

A lot of it is laziness. Some of it is busy-ness from my day job. But most of it is fear-based. Fear of failure. Fear of succeeding. Fear of the work involved. Fear of looking stupid. Fear of treading an unknown path. That fear sucks. A lot.

Especially now that I&#8217;m 30, that fear is closer and more menacing. When you&#8217;re in your 20&#8217;s and you&#8217;re not doing what you thought you&#8217;d be doing there&#8217;s that little part of your brain that&#8217;s like, &#8220;Well, I&#8217;m still in my 20&#8217;s, so there&#8217;s time.&#8221; But then you&#8217;re 30 and it&#8217;s &#8220;Well, shit. There&#8217;s still time, I guess, but I&#8217;m definitely no longer in my 20&#8217;s.&#8221; I&#8217;ve weirdly gained a lot of confidence in myself turning 30 (&#8220;fuck you, I&#8217;m 30&#8221;) but it&#8217;s also throwing into sharp focus the gap between what I say I want to do and what I&#8217;m doing. 30+ means your options are narrower. You really have to choose things and be pickier. You can&#8217;t do it all.

And you couldn&#8217;t ever do it all anyways, it just felt like you could. It felt like you could quickly do all the things you wanted to do. Like, you had the energy and time to make it all happen but let&#8217;s just watch this one show on Netflix first and, you know, I&#8217;m tired, so I think I&#8217;ll call it a night and then boom, you&#8217;re 30. You&#8217;re 30 and you&#8217;re looking back at all the choices you&#8217;ve made and it&#8217;s like, yep, I made those choices. And that&#8217;s ok. It&#8217;s not a bad thing. You clearly didn&#8217;t want those things enough to _really_ go after them. That&#8217;s ok too, just don&#8217;t blame anybody but yourself for your reality.

That&#8217;s kind of where I&#8217;m at. Over the course of my 20&#8217;s I did try and do a bunch of different things. Made a podcast, tried a few blogs, did a freelance thing, got married twice, bought and sold a house, bought another one, got a PhD, got a real job, became SCUBA certified, went skydiving, and on and on. My 20&#8217;s weren&#8217;t stagnant, that&#8217;s for sure. Nothing professionally just ever really &#8220;took off&#8221; the way I&#8217;d fantasized about. Looking back I&#8217;d have to say it&#8217;s because I didn&#8217;t really stick with anything long enough to actually make anything happen. I wasn&#8217;t in love with the process. And I never really accepted that I have limits.

Looking back through my evernote notes (evernotes?) I have several on the topic of rank-ordered goals. At the top of every damn list is this interview with my Grandfather. I interviewed him in October of 2015. And it&#8217;s taken me almost 2 years to make meaningful progress on that project. Why so long? Reasons. Excuses. I just didn&#8217;t prioritize it. Distraction was easier and more enjoyable. The truth is, editing those videos and writing transcripts is hard and tedious. But there&#8217;s just no other way to do it. It didn&#8217;t get done while I was doing other things.

I&#8217;ve actually been making solid progress on it lately. I&#8217;ve published five videos to Youtube (six if you include the intro) and I&#8217;m working on the transcripts for the videos as I post them. (Side note: writing transcripts is tough. I listen through the videos at 0.6x and even then it&#8217;s too fast some times. Then I have to listen to the video again at normal speed to correct all the mistakes I made during the initial pass. For a 30 minutes video, that means I&#8217;m spending close to 2 hours transcribing it. Which doesn&#8217;t include any of the time it took to edit it. 6+ hours of footage is pushing 30 hours of work.) Good progress, in this instance, means putting out a video about once every 2 weeks or so. In my head I can crank through all of this stuff in a weekend, in reality, it&#8217;s 30-60 minute chunks of work as I can grab them. Progress happens in fits and starts, not all at once.

But again, that&#8217;s not a bad thing. I&#8217;m limited by all the other things I have to do in my life. And they&#8217;re good limits (sleeping, eating, sexing, working, exercising, family timing, etc). My limits aren&#8217;t very unique, but they&#8217;re real and I have to accept them and make the best of the time I do have. And, in general, I&#8217;ve not been the best at making best use of the time I have. I&#8217;m really trying to change that. Some of the things that have helped is having a friend keep me accountable, thinking about what would make my wife proud, thinking about what would make 10 year old and 80 year old Taylor proud, and remember &#8220;memento mori&#8221;. Memento Mori deserves its own post, but just Google &#8220;memento mori ryan holiday&#8221; for some good articles and links. Also, being kind to myself when I do mess up or choose to do something that in a better state of mind I wouldn&#8217;t choose to do is very helpful. No need to beat yourself up any more than necessary.

Anyways, getting back to the main thread of this wall of text. I want to write a book. I want it to be about my Grandfather. I believe he has some fantastic stories and lessons that should be shared. I&#8217;m working to make that happen. Step 1 is working on these videos. Step 2 is more interviews. Step 3 is research. And the underlying step is writing when I can, building up an audience any way I can, and actually just doing the damn work. I&#8217;ve been talking about this book idea for so damn long that I just have to make it a reality at this point. I&#8217;d be deeply regretful if nothing ever came out of all this talk. I don&#8217;t want to be that guy that just talks and never delivers. The image of old wrinkly Taylor being regretful of the choices he&#8217;s made is terrifying to me. And motivating. So there&#8217;s really no other option but to keep moving forward.