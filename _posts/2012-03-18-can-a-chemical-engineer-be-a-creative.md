---
id: 101
title: Can a Chemical Engineer be a Creative?
date: 2012-03-18T19:44:50+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=101
permalink: /can-a-chemical-engineer-be-a-creative/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Future
tags:
  - artist
  - change
  - chemical
  - creative
  - eat
  - engineer
  - gilbert
  - love
  - pray
  - writer
---
This is a question I’ve been pondering for several months now.

Up until recently, my worldview was quite narrow and those that felt outside of my bubble weren’t ‘on the right path’. I had little personal experience with creative people and it was difficult for me to understand that mentality and lifestyle.

As I’ve learned more about being creative and the realities that exists around me, I’ve been drawn to expressing myself in a more creative manner. So drawn, in fact, that I’ve been working really hard to become a writer. Just typing that statement feels exhilarating and terrifying.

It was fortuitous timing, I believe, that led me to this TED talk. Here, Elizabeth Gilbert, author of _Eat, Pray, Love_, discusses genius and how to relate to our creative center. I connected with this talk because she specifically mentioned that her dad was a chemical engineer. She is dead on when she says nobody every asks a chemical engineer if they’re on a safe path.

[Elizabeth Gilbert TED Talk](http://www.ted.com/talks/lang/en/elizabeth_gilbert_on_genius.html)

The more I’ve learned, the more I feel like perhaps there’s another side to who I am. I’ve been scientific and analytical for so long that my creative side has atrophied. I’ve been working hard to workout my creativity muscle (it’s next to your flactoid _har-har!_) and expand my experiences.

Can a chemical engineer be a creative person? I’ve never met one that was prolific in the arts. Da Vinci certainly was and look at what a badass he turned out to be. Maybe engineering the uncommon for me means learning to become a creative person, whether that’s as a writer, painter, sculptor, designer, or whatever.

It’s absolutely foreign to me, and yet it’s fiendishly fascinating. I’ve said for a while now that I would have no problems graduating with a Ph.D. and then never using it again. Perhaps this is a step in that direction. Is it the right one? Who knows. But it certainly feels like it