---
id: 113
title: 'One thing ends, another doesn&#8217;t, and something else begins.'
date: 2012-04-04T12:17:48+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=113
permalink: /one-thing-ends-another-doesnt-and-something-else-begins/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Emotions
  - Future
tags:
  - challenge
  - development
  - divorce
  - eating
  - emotion
  - improvement
  - personal
  - relationship
  - vegan
  - vegetarian
---
I had a different sort of post in mind for today but life has a way of shoving monkey wrenches into the gears of the best laid plans.

My effort at living in complete congruence with my physical and digital selves is going to be put on hold for another week. Evidently there was a problem with the notary public’s signature on the MDA and the divorce can’t be finalized until next week.

At first I was about to get really upset about this, but I managed to avoid that intense anger. Some things are entirely out of my control and losing my temper about it is not going to make anything happen better or faster. Instead, I just smiled, sighed, and made plans to get it fixed.

So all that stuff I talked about Monday? We’re gonna push it back until next wednesday. So it goes.

Switching gears now (after I took that wrench out of them), today is the end of my 30 days of no internet at home! It actually blossomed to almost 40 days, but who’s counting?

Overall, this experiment was a resounding success. Limiting my access to the black hole of the inter-tubes has changed my relationship with the web. I’ve reached a place where it no longer controls my attention like it used to. My ability to assess the value that a certain site offers to me has increased and I’m able to cull the unimportant noise and keep it from stealing my attention.

I am glad to have it back though. Updating my blog was a bit tedious, and there were times when I needed to do some specific tasks (purchase plane tickets, plan a weekend, etc) but it wasn’t difficult to solve those problems. I still had my phone for absolute ‘necessities’.

Now that I’ve accomplished that experiment I want to move right on to the next one. I want to change my relationship with food. Lately, I’ve had a tendency of snacking quite a bit when not hungry and eating too much candy. Some might argue that it’s actually not that much, but I know how I feel afterwards.

For this next 30 days I’m going to do two things. First, I’m going wholly vegan. This isn’t as drastic a change as it may seem. When I eat and cook at home, it’s almost entirely vegan. I don’t consume dairy, and haven’t for over 6 months, because it just tears me up. I’m lactose intolerant (only took 20 years to figure out) and so the choice is easy when it comes to milk products. I know, living without cheese seems impossible, but I promise, you’ll survive.

The challenging part of eating vegan will really be when I go out to eat. I enjoy a good burger every so often and it will be difficult to make the proper substitutions, especially at some of these good ‘ol southern restaurants that ask if you want chicken when you tell them you’re vegetarian. Seriously. As a bonus, my office is having an Eat Like a Vegan Week at the end of April so I’m just extending it to a full month because I’m evidently hard core like that. Or as my friend would say, pretty fucking metal. PFM for short.

The second challenge for this 30 days is to not eat after 7pm. Some of my heaviest snacking (damn you peanut butter and raisins!) has come in the evenings when I was feeling particularly tired or stressed. I’m quite aware that not eating right before bed can help you to rest better, which in turn makes you feel better. Therefor it&#8217;s a logical choice to try and not eat right before bed.

I say 7 pm, but I am going to be a little flexible on that. A better way to state it would be that I’m not going to eat 3 hours prior to getting into bed. Most nights that’s between 10 and 11. So my window to eat can shift a little bit. I’ll reassess this soft rule as the month goes on.

My expectation with these challenges is that it will be difficult at times, but not overly challenging. I’m 80 &#8211; 90% vegan already, so I don’t have far to go there. Not eating in the evenings will be tough, but I expect to be able to adapt to it quickly. If it looks like this is going too well for me, I’ll consider adding something else on top of the these two challenges.

I’ll update my progress like I did the previous month and hopefully hear from some of my readers, few that there are, about your eating habits. Here’s to a successful internet challenge and to another month of personal improvement!