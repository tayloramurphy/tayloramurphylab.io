---
id: 4
title: The Beginning
date: 2012-01-18T02:16:33+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=4
permalink: /the-beginning/
brunelleschi_featured_post_position:
  - left
brunelleschi_featured_post_width:
  - 1/4 Width
categories:
  - Beginning
tags:
  - Beginning
  - first
  - new
  - starting
---
I&#8217;ve delayed long enough. It is time for me to live. It is time for you to live.

I spent too long putting this off. Just starting this filled me with fear, but the mere thought of doing it, filled me with excitement.

What if I fail? What if they don&#8217;t like it? What if it really sucks?

Who cares. At least I will have done something.

I hesitated to start this tonight. I hesitated because I was making excuses. I don&#8217;t have a good title. I don&#8217;t have a good theme. I&#8217;m not a good writer.

Screw that. Here it is. It&#8217;s out there for the world to see and I&#8217;m going to work hard, live passionately, be creative, and add value.

I&#8217;ll polish it up later.

It&#8217;s not perfect, but it&#8217;s out there.

Expect more.