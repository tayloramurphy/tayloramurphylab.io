---
id: 460
title: Viva Las Vegas
date: 2017-01-16T20:15:21+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=460
permalink: /viva-las-vegas/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
tags:
  - gambling
  - learning
  - vegas
---
For 4 days I went to Las Vegas to spend time with my Dad and Uncle at the 2017 Consumer Electronic Show (CES). The show and the city itself left me with conflicting thoughts and feelings and I wanted to unpack these to see what&#8217;s really going on.

CES is an overwhelming experience. There are over 25000 products showcased across greater than 2 million square feet. It&#8217;s impossible to see everything the show has to offer. Initially, the awe of seeing all of these new, shiny products is great. You come to the steps that lead onto the main floor and you see so many booths with names you do and don&#8217;t recognize. Each one promises to show you something you&#8217;ve never seen before and astound and entertain in myriad ways. And for a while it does. You see the drones and cameras and cars and TVs and speakers and headphones and it&#8217;s all wonderful and shiny and amazing.

Then you wander for a bit more and see the massive installations by the likes of Samsung, LG, Sony, Canon, Ford, and on and on. They&#8217;re each little worlds unto themselves. You walk through, transported to a place where everything works and promises to make your life so much better and entertain you to the max. Then you walk back out of that world into the world of way too much carpet and even more products. You realize that what you thought was the back of this hall was only about half of the entire space, and it&#8217;s only 1 of 6 halls in the entire place. You walk a little quicker and see more headphones and TVs and speakers and gadgets and you wonder what makes them different at all. There are brands you&#8217;ve never heard of copying products you&#8217;ve seen and maybe own. There are some true innovations but those are harder to see unless you know what you&#8217;re looking for.

Perhaps it&#8217;s because I was an intruder in this place, but it was less enjoyable that I hoped. My Dad and I had two identical badges with my Uncles&#8217; names on it, so already we felt like imposters. Talking to anyone, for me, became impossible because I felt like the person who I was purporting to be was not me at all in every sense. I feared that talking with someone would lead to inevitable questions that I wouldn&#8217;t be able to answer. This mean that with every booth I walked by I would avoid eye contact and purposely take routes that moved me around the company reps.

That behavior was in conflict with the person I like to think I am: somebody who can talk easily with people and is genuinely interested in many of the products. instead I felt like a shifty imposter who was a voyeur of everyone&#8217;s wares.

This discomfort was alleviated when my Uncle was able to get us into semi-private viewings of products for Samsung and HTC Vive. These places were much better for viewing products and had a limited attendance because it was mainly for people in the know. The Samsung event was weird because there was almost no-one in there expect for a few Korean reps, but we still managed to play with all the VR equipment even though we were like three American wrecking balls in there. The Vive event was even better. You&#8217;re supposed to have an appointment, but my Uncle was able to talk us into there. He&#8217;s the VP of Digital Marketing for Disney and simply stated that he was told to come to this and that there were things we needed to see here. He introduced my Dad and I by our real names and said we were consultants for Disney. They gave us permission to check things out and I felt free to actually talk to people and ask questions. As it turned out, Andre dove into a deep conversation with some of the guys there and set up a meeting the next day to demo more of the equipment.

Watching him work was fascinating. Andre is at the top of his game when it comes to marketing and networking. His ability to talk with anybody and generate a real connection was clearly a skill he developed over several years. It&#8217;s hard not to be envious of his ability to disarm people&#8217;s initial skepticism over an interaction and get them to share and open up. Shadowing him taught me some things about interacting with people and getting what you want that I wouldn&#8217;t have learned without a ton of time and experience. Seeing what&#8217;s possible can help to level up your game. It also showed me that the really cool stuff isn&#8217;t what you see on the show floor. It&#8217;s in private areas and has limited access to the public. Getting to see and do the cool things is more about who know than anything.

Outside of CES, was the rest of Las Vegas. My Dad and I spent most of our time on the strip, but didn&#8217;t really do anything of note. We wandered through several casinos and saw more gambling machines than I could count. Each casino has a different vibe whether it was catering to higher or lower limit gamblers. We learned to play craps from a free lesson but never partook of any actual gambling. The thrill of winning at fake craps was enough to scare me off actually gambling with real money. I could see the addiction to that thrill manifesting in my brain and I didn&#8217;t want to feed it anymore. There were also, of course, tons of sensual visuals. Men and women alike dressed to be seen and entice. Women had tight dresses, high heels, and flashy jewelry. Men dressed in well-fitted clothing with fancy shoes and styled hair. These people are easy to look at and it certainly is a distraction from the other people who are dressed down, staring at a slot machine, burning their cigarettes and money. I felt like a country bumpkin wandering around these places and was very aware of my lower status and value from these people&#8217;s perspective. I didn&#8217;t care much, but it&#8217;s hard not to when everyone around you seems to care so much.

The disappointing part of the strip is how everything is meant to part you from your money. There&#8217;s nothing there for you to do for free or to create any sort of real experience for yourself. Playing a game of pool wasn&#8217;t an option because there wasn&#8217;t a billiards table around for miles. To play a game you had to pay some coin. The stores all along the strip are all about selling you a story and lifestyle of something you&#8217;re not but may want to be. Buy this, wear this, and you&#8217;ll finally be what you wanted. Everything is so expensive and everything requires you to spend something.

But I suppose I can&#8217;t fault Vegas for going all in on what it aims to be. Vegas is consumerism and hedonism cranked to 11, and it is what it is. I suspect I would have enjoyed going to a concert or a show (like Penn and Teller&#8217;s), but again, there&#8217;s a fee for those things. Were I to go to Vegas again I would do it differently, and probably with some friends of my own. Knowing how expensive everything is, I&#8217;d save up some money and then earmark a certain amount for gambling a bit, most of it for food and drinks, and then see a show or two. Hedonism can be fun for a bit, especially when you truly love the life you go back to at the end of it (like I&#8217;m fortunate enough to have).

I don&#8217;t think I&#8217;d enjoy going to Vegas again with my Dad because the intersection of our common interests and what Vegas has to offer just aren&#8217;t there. We&#8217;re more suited to an outdoor adventure than the bright lights of the city. Andre and I would certainly have some fun, but the behavioral limit mismatch of a single vs married guy might be flirting too close with trouble for me. I do regret, though, not going to a private Chainsmokers show that Andre was able to get us into. I was tired and decided to go to sleep instead of rallying for some fun. &#8220;Old man Taylor&#8221; won that battle.

Overall, Vegas was a fun and interesting trip. I learned some things about the city and myself and was able to reconnect with my Dad and my Uncle each of whom I hadn&#8217;t seen it about 1 and 18 years, respectively. It was fun to relax a let loose for a bit and it made me even more thankful for the life I have back home and a partner who I share so many interests with.