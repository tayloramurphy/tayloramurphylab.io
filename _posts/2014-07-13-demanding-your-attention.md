---
id: 416
title: Demanding your Attention
date: 2014-07-13T11:33:50+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=416
permalink: /demanding-your-attention/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Personal Development
  - Relationships
---
Recently, I was made painfully aware of the need to be conscious about who is demanding your attention. In the span of 16 hours of wakefulness, I had a slew of emails, 3 phone calls, a solicitor on my porch, numerous coworker requests, and random texts from friends.

None of these events are remarkable on their own, but their coincidental timing made me conscious of the large number of people who want my attention for their own purposes. Two of the phone calls were surveys demanding my opinion about their own service (car dealership and rental car). Vanderbilt called me asking for money (even as little as $25 helps!). The solicitor on my porch was hocking an alarm system. At one point, he tried to come into my home, ostensibly to inspect what kind of system I currently had, but all so he could put his system in (for free!). My boss periodically comes to my cube to ask a question about something semi-related to what I&#8217;m doing in the moment. And friends, being the good friends that they are, text to see what&#8217;s up.

All of these events require my attention and energy. Most of them, I could do without. My friends have carte blanche to contact me whenever and their requests for my attention are always welcome, but everybody else is a significant drain on my internal resources. When the dude came to my porch, I was in the middle of folding laundry while listening to Star Talk radio (how nerdy!). It brought me out of the zone I was in and I had to have a 10 minute conversation about something I didn&#8217;t want to talk about while my dogs freaked out from inside the house, worried if their Dad was going to be taken away by the man in the blue shirt.

My boss, and anybody at work, may need my input for something at any point in the day and it&#8217;s good to be in a position where people need your skills and expertise. The problem, though, is if you happen to be in a flow state, the interruption can be devastating to your productivity. It can take upwards of 15 minutes for me to get back into a state of flow.

Interruptions are a common and vital part of life, and they point to a simple truth about humans: we are all selfish.

We&#8217;re all wrapped up in our own heads trying to deal with the myriad things we have to do. The car needs to be serviced, I&#8217;m out of bananas, what&#8217;s the status on this project, I need to book this trip, how long until this vacation is here. These thoughts are in everyone&#8217;s head. When somebody is asking for your attention, it&#8217;s rarely in the service of your needs. Generally, they are trying to meet one of their own needs. (Significant others are generally the exception to this rule. They&#8217;re hopefully thinking about your needs some, but rest assured they&#8217;re more concerned with their own needs than yours.) This isn&#8217;t a bad thing. After all, nothing would get done if we weren&#8217;t able to gain access to other people&#8217;s attentions at different times.

The point I&#8217;m trying to make is that some requests for your attention are worth your time and some are not. In fact, I&#8217;d venture that most are not worth your time. It wasn&#8217;t worth my time to answer the phone three times the other day. Nor was it worth it to stand and talk about security systems. Most of my email could&#8217;ve waited until the end of the day. Only the interruptions by my boss and friends (and girlfriend!) were welcome and useful.

It can be challenging in this connected world we live in to protect your attention. Anywhere you go on the internet there&#8217;s some banner flashing at you, or a pre-roll ad is playing in front of the YouTube clip you want to watch. Everybody wants everybody&#8217;s attention indiscriminately. It&#8217;s more challenging and more important than ever to work against these forces to save your attention for the things that truly matter to **you**.

Be aware and push back. Not all things that demand your attention deserve it.