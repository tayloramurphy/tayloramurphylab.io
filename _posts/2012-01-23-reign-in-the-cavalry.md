---
id: 25
title: Reign in the Cavalry
date: 2012-01-23T22:00:34+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=25
permalink: /reign-in-the-cavalry/
sharing_disabled:
  - "1"
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Emotions
tags:
  - anger
  - control
  - emotions
  - power
  - reclamation
  - response
---
I recently received a very mean text message from a blocked number. This person told me their exact thoughts on my actions during my divorce. It was a very short message, but it was hurtful and rude.

This immediately got my blood pumping and I became quite angry. **How dare they**, I thought. Who is this person to tell me what kind of person I am?

I ripped off a scathing text message in reply telling them to keep their mouth shut about things of which they know nothing. I hit send and felt self-righteous for a few minutes. I felt like I&#8217;d won some battle and shown them who&#8217;s boss.

But then that anger started to creep in again and it was starting to affect my behavior. I was driving home and I could tell by my aggressiveness that these emotions were controlling me.

At that moment of realization, I saw how I was acting. I became aware of the fact that I had let this anonymous person have the power. I gave them control of myself and I immediately regretted it.

**I then started to reclaim my power**. As angry as I was, I smiled. Just that simple act of forcing my muscles into a shape that is greatly associated with happiness calmed me down a bit. Then I started talking to myself.

I told myself that I am in control of my emotions. That anonymous person accomplished exactly what they wanted because I responded wildly. I said aloud that I don&#8217;t have to convince anyone else of the correctness of what I&#8217;ve done and I wasn&#8217;t responsible for making anybody else feel better about the situation. I can&#8217;t control other people, but I can control myself.

With that little pep talk, I was able to calm down a bit and go forward in a positive manner. I don&#8217;t know who sent me that text. I&#8217;m fairly certain my reply never actually got sent. I do know that I learned a great lesson in responding to difficult situations:

**You can&#8217;t control everything about a situation. Control yourself, and you maintain the balance and power in your life.**

I urge you to think about how you respond to certain situations. Do you give the other person the power? Or do you acknowledge their feelings and then respond in a controlled manner? Never let anybody dictate how you feel. Control what you can, and let the rest go by.