---
id: 228
title: Advice on Writing
date: 2012-10-15T22:13:50+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=228
permalink: /advice-on-writing/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Personal Development
tags:
  - author
  - better
  - books
  - deliberate practice
  - practicing
  - quora
  - writing
---
Over the past two weeks I’ve come across several good pieces of writing advice that I had to share. Part of my desire to share is selfish: I wanted to put these pieces of information in a more permanent place because they’re that good.

The first chunk of advice comes from [Quora](http://www.quora.com/Writing/As-a-21-year-old-aspiring-writer-with-negligible-insignificant-literary-background-what-are-the-steps-I-should-take-to-achieve-my-goal-of-being-a-published-author-in-the-next-five-yearshttp://www.quora.com/Writing/As-a-21-year-old-aspiring-writer-with-negligible-insignificant-literary-background-what-are-the-steps-I-should-take-to-achieve-my-goal-of-being-a-published-author-in-the-next-five-years). A 21-year-old man asked what was the best way for him to become a published author within 5 years. One answer in particular resonated with me because it clearly advocated the idea of deliberately practicing the craft. It was the perfect answer to the question “How do you practice writing?”

This had been a question I’d been pondering for some time. With other activities, it’s clear how you can practice. If you play basketball, you shoot a zillion free throws. Any sport or activity where success and improving can be clearly enumerated has better defined ways of practicing.

But what about writing? The art of creating story and stringing words together into coherent sentences is not something that is easily definable in terms of quality. It may not be immediately apparent whether you’re improving or not. In fact, it can quite often feel like you’re making zero progress at becoming a “better writer”. It seems like the process of becoming a best-selling author goes like this:

  1. Write
  2. ????
  3. Profit from best-selling book

Not exactly clear.

Now, perhaps some of this advice is obvious to those of you who majored in writing or more creative endeavors, but as an engineer who’s only had a few semesters of English and Composition it was less than evident.

These suggestions have solidified in my mind ways to stretch my ability and get outside my comfort zone with writing.

## **Practice With Words**

The author of the answer I’m highlighting suggested spending a good amount of time doing some of the following activities:

**Rewriting books and novels that inspired you**. I took this to mean rewriting as in trying to rephrase and make it your own, but another thought came to me is that it might actually be good to physically copy a good book. Hunter S. Thompson retyped The Great Gatsby just to know what it felt like to write a good book.

**Add personal details to a dictionary: notes, context, and expanded definitions.** Exploring the varieties of the language and coming to appreciate words can only help what you have to say.

**Describe scenes from pictures that you gather around the net.** A good place to start might actually be Pinterest. The whole focus of that site is images. Gather a few and write some stories.

**Meet interesting people, interview them, and learn about altering points of views.** The idea here being that by exposing yourself to the rest of humanity you will gain more perspective and insight into the human condition.

**Travel. A lot.** This is all about gaining experiences. One of the amazing aspects of travel is the opportunity to meet people. This goes back to the previous point: stories that we connect with are about people and their struggles. Your individual story is great, but there’s so much more out there too.

The author then goes on to suggest that at the end of each month you go through everything and scrap what doesn’t resonate with you or meet your taste. Keep things that make you smile. Then start the process over again.

## **Create the Story**

This bit of advice is aimed at helping you make progress on your goal of writing a book. The author suggests that for every idea generated, you take some paper and do the following on separate pages:

  1. Summarize the story in a few sentences.
  2. Create detailed character sketches including details of psychological aspects and connecting it to as many keywords as you can.
  3. Detail the environment. Draw maps, outline neighborhoods, get into the nitty gritty of the space you’ll be working in.
  4. Write the backstory. Describe the events that have to happen before the story starts. What’s going to influence the events you want to describe.
  5. Write as many passages as you can for the story.

Each time something is added to one of the pages, you should update all the other sheets for the story. This, according to the author, will allow you to see if your novel is going the right way.

He also ends up his advice with three simple ideas: write as much as you can, figure out what works best for you in terms of a writing habit, and let people read your stories.

Or even more simply: Write. Write some more. Get feedback.

## **Writer’s Circle**

The second chunk of advice I want to share came from this past Sunday when I attended the Writer’s Circle at the Nashville Public Library. There were two authors having a Q&A session: [Adam Ross](http://www.amazon.com/Adam-Ross/e/B002T3MQIK/ref=sr_ntt_srch_lnk_1?qid=1350356602&sr=1-1) who wrote Mr. Peanut and [John Egerton](http://www.amazon.com/John-Egerton/e/B000APSG5G/ref=sr_ntt_srch_lnk_7?qid=1350357090&sr=1-7).

What follows is a collection of short, pithy ideas that I thought were worth recording.

  * The best way to practice a skill is to ship a product.

_On writing a story_

  * Why is _this_ night different than any other?
  * Where does a character start and do circumstances force them to get off that position? This isn’t a spatial question, it’s an existential one.
  * You need to punch through from the particular to the universal.

<div>
  <em>General Advice</em>
</div>

  *  Writers need to write stuff that sounds good when read aloud.
  * Style and substance: what else is there?
  * Surprise has an antecedent and is based on reader expectations. Create surprise by anticipation and building pleasure along the way.
  * Character means to sharpen: sharpening your idea of who this person is.
  * Writing is the same for someone who’s just starting or someone who has a book in 60 languages: it has to work on the page.
  * If you keep coming back to it and can’t let it go, the there’s probably something there.
  * Never underestimate the power of a finished thing.

The bookends of these bullet points are saying the same thing. What’s interesting is that the first one I wrote down just as I sat down in the room. The authors and host and were talking about their books and this though came from the recesses of my mind. It’s based on a lot of other material I’ve read about writing, marketing, and business, but it applies so well to nearly every aspect of art.

Then, when the Circle was nearly over, they come back with the same thing I wrote at the start. The draw of having a completed book, of being able to ship it out in the world, is powerful and it’s the really the only way to get better.

## **My advice**

All of this is truly great advice. I’ve already had some of this advice in practice. I write for my 642 Things project. I post the stories daily to [Tumblr](http://needsomerope.tumblr.com/) which I consider to be a place for me to ship, or make public, my short stories. I also update my personal blog frequently and am now a contributing writer to [another project](http://empoweringwomenproject.org/) (which I&#8217;ll detail in a future post). The other actionable pieces of advice are useful as well and I plan to incorporate them into my practice in the future.

Becoming better at anything really is about practice. Put in the time and you’re guaranteed to improve. Hopefully these tips will help you, and myself, improve and flex our writing muscles.