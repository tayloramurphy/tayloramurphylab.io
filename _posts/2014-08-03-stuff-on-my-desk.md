---
id: 419
title: Stuff on My Desk
date: 2014-08-03T15:41:19+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=419
permalink: /stuff-on-my-desk/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
---
I have a fair amount of stuff on my desk. Far more than my coworkers at least. For someone who at one point claimed to be a minimalist, it would seem I&#8217;ve denounced my former beliefs and become drunk on consumerism.

Currently, I have a Burnie Burns bobblehead, a poster from Rooster Teeth, a small print of &#8220;The Little Girl and the Hill&#8221;, pictures of my family, two joke posters, and a deck of Red vs. Blue playing cards. There are some books, headphones, pens, and random pieces of paper, but those are accumulated through the normal flow of work.

So why do I have all this crap on my desk? It&#8217;s a fair point to say that they take up space which could be used for other, perhaps more important, things. Some of these items don&#8217;t seem relevant to work at all. However, all of the so-called &#8220;clutter&#8221; on my desk serve as very specific and personal reminders for me. They keep me grounded in my daily working life and are touchstones, or talisman if you wish, of things that I want to keep fresh in mind.

The [Burnie Burns bobblehead](http://store.roosterteeth.com/products/burniebobblehead) and [Rooster Teeth poster](http://store.roosterteeth.com/collections/posters/products/rooster-teeth-decade-print-11-x-11) are reminders for several reasons. I&#8217;ve been a big fan of Rooster Teeth for a long time and even more so since they&#8217;ve seen such great success in recent years. Burnie is the Founder and Creative Director of the company (formerly the CEO). He&#8217;s also the voice of Church in the popular web series Red vs. Blue. The poster was made by Miles Luna who is one of their writers/directors. His artwork has bits of every part of Rooster Teeth&#8217;s history. These items serve two purposes: the first is to always strive to have fun in life and the second is to always create things and share it with the world.

The &#8220;[Little Girl and the Hill](http://wecraftstories.com/books/little-girl/)&#8221; print, which is a children&#8217;s book written by my good friends Brett and Amber, is a reminder not only to create, but to create things in your own way and to not be afraid to ask for help along the way. It&#8217;s a reminder to put every bit of yourself into your work and then ship it to the world.

Pictures of my family (Lindsay, Luna, Riley, Mom, Dad, and Sisters) serve as reminders of love. Love is why I work hard and try to be the best man I can be.

The joke posters are another reminder to not take life so seriously. It&#8217;s easy to get caught up in the business of being serious and doing serious business. These irreverent posters take some of the hot air and posturing out of &#8220;Big Business&#8221; and remind me (and hopefully my coworkers) to laugh and have some fun.

The playing cards are probably the only thing that is a bit redundant. I bought them because they&#8217;re Red vs Blue and I thought it&#8217;d be good to have some cards in the office in case an intense game of Texas Hold &#8216;Em was necessary. I have yet to use them as intended, but they&#8217;ve still been useful.

My coworkers tease me about all the stuff I have on my desk. In such a logical and formal place as a startup, it probably seems weird that I would decorate my space. After all, we&#8217;re at work when we&#8217;re working and our focus should be on the work. However, I never let their criticism bother me because I know the purpose each of these objects serves in my life. It&#8217;s easy to get so focused in one&#8217;s work that you never take the opportunity to look up and enjoy life. Each of these things achieves their purpose and brings a smile to my face.

I hope you&#8217;re not afraid to make your space, and all the things in it, how you want them. Other people may make fun of you, but who cares? It&#8217;s your space and it&#8217;s there to serve you. I&#8217;m always pushing the invisible boundaries of what&#8217;s &#8220;business appropriate&#8221;. After all, what&#8217;s labeled as such is simply something someone made up. I&#8217;m all for challenging the boundaries of the things people make up.