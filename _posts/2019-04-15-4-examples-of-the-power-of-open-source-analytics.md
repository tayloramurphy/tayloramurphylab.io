---
title: 4 Examples of the power of open source analytics 
date: 2019-04-15T13:21:00+00:00
author: Taylor Murphy
layout: post
permalink: /open-source-analytics/
---

I wrote a post on the GitLab blog talking about the benefits of being transparent and open on the data team.

Read about it [here](https://about.gitlab.com/blog/2019/04/15/open-source-analytics/)
