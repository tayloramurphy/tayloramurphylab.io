---
id: 36
title: Think About the Future
date: 2012-02-01T20:38:04+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=36
permalink: /think-about-the-future/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Future
tags:
  - future
  - health
  - prepare
  - ready
  - unexpected
---
I sat in the waiting room chair contemplating life. My white blood cell counts were low and the doctors didn’t know why. Being honest, I was a little nervous, but outwardly I projected confidence and a nonplussed attitude. (Depending on your definition of nonplussed that statement could have two completely opposite meanings.) Either way, I sat there wondering if I had done what I could to avoid this situation.

Nobody likes going to the doctor. When you’re at the doctor, it means something is abnormal and you want it to become normal again. With leukopenia, you’re at a greater risk for infection. There are many reasons why this condition could present, none of which I thought applied to me. I wasn’t sick, I ate right ,and I had no obvious symptoms. The only way I had discovered this troubling fact was via a routine physical. I told the doctor everything I’ve been doing lately and anything that could possibly be out of the ordinary (I drink a lot of water, I donate platelets, I’m training for a half-marathon, etc.). Nothing seemed to trigger a response, so they took some of my blood, told me they were going to run a few tests, and said to expect a call.

## **Making Preparations**

Many of the events that occur in life have a reasonable probability of occurring. It’s reasonable to expect that at some point you will be in a car accident, you will injure yourself somehow, and some appliance in your home will break. Of course knowing when these things will happen is impossible. All we can do is to prepare for the eventuality and hope for the best.

The best-practice for any situation is to prepare _before_ the event occurs. This in the hopes that you can prolong the time between now and that unknown moment as well as to help the recovery go as smoothly as possible. It’s ridiculous to try and prepare after something has already happened. Only management of the situation can occur after the fact.

Preparation is an important aspect of our everyday life. We prepare for the inevitable with insurance. We study for tests. Exercise and proper eating habits help us to prepare for sickness. Each of these situations requires conscious thought and effort. Without that exertion, life is just something that catches you completely off-guard and unable to handle. Being prepared means you’ve done everything you can to either avoid it or handle it as smoothly as possible.

The consequences of under-preparation can be drastic including significant disruption in your daily life, massive financial burdens, and, in extreme cases, loss of life. There’s a reason the Boy Scouts have the motto ‘Be Prepared’. To do otherwise is to create unnecessary risk in your life. Some risk is necessary, it helps you to grow. The other stuff could kill you.

## **The Verdict**

I went about my day, putting the tests out of my mind. My phone vibrated with an unrecognized number who I knew immediately to be the doctor.

Good news: I was perfectly unremarkable. No infections, no presentable symptoms, and the fact that I had low counts was just part of the fact that at least a few people are going to fall outside the bounds of their “normal” confidence interval.

In this instance, my preparation and good habits had paid off. I worked hard to be healthy, and so I am. Truly, the entire time I was confident that this was nothing to be worried about. My unhealthy habits are kept to a minimum and I value my health greatly. It’s an excellent feeling to know that the hard work pays off and results in a clean bill of health. This lesson is valuable in all areas of life including your life’s mission, your finances, your relationships, _and_ your health.

_How have you prepared for life’s unexpected events? Anything you think should probably change?_