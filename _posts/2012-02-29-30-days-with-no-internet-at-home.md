---
id: 75
title: 30 Days with No Internet at Home
date: 2012-02-29T22:55:27+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=75
permalink: /30-days-with-no-internet-at-home/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
tags:
  - challenge
  - experiment
  - failure
  - internet
  - success
  - test
---
Ever since I was about 10, I’ve always had access to the internet. It started with America Online. That mechanical, digital ruckus signaled the connection to the rest of the world was complete and I had access to nearly anything.

Fast forward to today, and my use of the web is more abundant than ever. I have high-speed internet at home and at work. I can go to Starbucks, Krystal’s, Kroger, Whole Foods, or the library and be connected in a matter of seconds. My phone has 3G wireless internet so I’m never too far from the information I might need.

**This constant access can be an amazing thing.** The ability to look up factoids on Wikipedia, or check the status of the world is truly astounding. And let’s not forget how GPS apps help the forever lost find their way home.

Of course the dark side to this is the distractions. For all the positive and productive places the internet provides, it has ten times as many distractions. It’s these distractions I want to avoid for a while.

So starting tomorrow, March 1st, I’m turning off the internet at my house. It’s an experiment in productivity and mindfulness. I spend large amounts of time on websites (I’m looking at you Reddit) that distract me from what I really want to be doing. I want to be reading and writing. I want to be creating and improving. I can’t do that when I’m watching stupid videos.

I figured an experiment was in order. 31 days of no internet at the house will give me the opportunity to learn how to be unconnected  from the morass that is the internet.

## 31 days of focus and minimal distraction. Here are my expectations

I expect that it will be easy some days and difficult others. I have plenty of books I wish to read (4 checked out from the library at the moment). There are many things I want to write. I have speeches to write for Toastmasters. These are activities I want to do and being free from the internet for a while will help me.

I expect that if I truly do need to use the internet, that it will be more purposeful. I will not reconnect at the house, rather, I will travel to one of the many places within walking distance that has free Wi-Fi. This will make getting online a conscious decision and not something I default to doing when I get momentarily bored.

I expect that I will dig even deeper into exploring my thoughts, actions, and beliefs. A friend recently stated that people are afraid of silence. I very much agree with her. **Silence can be hard to find**. So I’m forcing my self to be in a place where silence will rule. With nothing to passively distract me, I will face my thoughts and emotions in a very raw state. This has the potential to be very therapeutic (or it could drive me insane).

I don’t expect this to be the hardest thing in the world. As a part of my research, I have to have access to the internet. There’s no way around it. I also have to be able to communicate via email whether it’s with my advisor or other collaborators. One concern I have about this is that instead of spending time at home, I’m going to spend more time at work where I have unrestricted internet access. It seems likely that I would pack in all of my browsing when I first get to work in the morning and right before I leave at night. I’ll keep track of this behavior and adjust as necessary.

I can imagine some people saying, well this isn’t really that big a deal. It’s not really that challenging. I would actually agree with them. This is but a small challenge. But I live in a world where I’m not faced with many real challenges. Any perceived challenges really aren’t that hard and are blown out of proportion.

To be able to test myself, I have to deliberately choose to be put into difficult situations. This is why people run marathons and fast from eating. **Some tests of character won’t come to us unless we seek them out**. That’s what I’m doing here. This first challenge may be small, but it’s the beginning of something grand.

I will learn some things about myself and I will grow because of it. I will be able to look back on this experiment and see how I changed and what I learned. Knowing that I came through this will help me get through the next challenge and the next one. It will become part of my catalogue of experiences that I can draw upon when I am placed (or when I place myself) in truly challenging situations. It may seem small, but the journey has to begin with something.

**_Have you tried living without internet access at home? What little challenges have you chosen to experience? Please share in the comments your challenges, success, and failures._**