---
id: 62
title: Decisions on Stuff
date: 2012-02-20T09:35:05+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=62
permalink: /decisions-on-stuff/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
tags:
  - belief
  - change
  - minimalism
  - stuff
  - value
---
Somewhere in the trails of my past are evidence for countless decisions. Every time I did something, it left a mark on me either, phsyically, mentally, emotionally, or monetarily. These decisions culminate in the day to day of who I am as a person. A choice I make every day is to not fill my life with _stuff_.

Currently, I am attempting to embrace minimalism with as much gusto as I can muster. Going through a serious life-changing event has a way of shifting priorities and catalyzing certain behaviors. For every person, the reaction is different. None are necessarily right or wrong and I’m here to offer no judgment on others. I ask no judgment from you.

Essentially, minimlism is the philosophy that you can live a meaningful life with less crap. A lot less crap. Less than you think. People have lived, and currently still do, with fewer than 100 things (some even fewer than 50). It’s not the things you own that make your life, it’s the things you do and who you do them with that make your life.

In trying to embrace the best I can, I am purging the excesses of my previous lifestyle. When I moved back into my place, there wasn’t much left. I had no dishes, very little furniture and the walls were bear. It was depressing to witness. But I had a choice to make. I either can have remorse over the objects that are no longer in my life, or I can move forward and find meaning in more valuable ways.

This is a decision I make every day. The things we own, the pictures, furniture, toys, movies&#8230; everything does not intrinsically have value. Certainly people value certain items more than others, and we ascribe value to things that are rare, but somebody who had no knowledge of this informaiton would not know just by looking at something that it had value.

People _do_ intrinsically have value. It’s a part of every persons being. They have value, and they are valued. Family, friends, coworkers, and strangers, they all mean something and they are all worth something. I realized this and made a switch in my mind.

Now, I have decided to value people more than stuff. I value shared experiences more than solo ones. The conversations I have, the memories I make, and the stories that are shared are more meaningful than anything I could ever buy.

Ever since I committed to this way of life, and truly embraced it, I have felt more at peace with the people and things around me. Everything I own, and I do mean everything, could be lost and I would be ok. That’s only possible when your value system is based on people and not on things.

So yes, I’m getting rid of my Star Wars Collectibles (okay, they’re _toys_), my golf clubs, my Xbox, and eventually my car. Removing these distractions allows me to make meaningful connections with real people. I’d take great conversation over more stuff any day.

**_What are your experiences with minimalism? Are you trying to get rid of something but can’t seem to do it?_**