---
id: 137
title: Getting started is scary as hell, but it needs to happen.
date: 2012-05-01T22:28:21+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=137
permalink: /getting-started-is-scary-as-hell-but-it-needs-to-happen/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Future
tags:
  - beliefs
  - blog
  - challenge
  - comfort
  - dream
  - emotion
  - fear
  - future
  - goal
  - grad
  - lazy
  - school
  - student
  - work
  - zone
---
I was called out on my behavior recently by two different people.

I&#8217;m stalling. I&#8217;m waiting. I&#8217;ve been wasting my time.

Talk about a punch to the gut.

Over the past few months, I&#8217;ve taken the time to invest in myself. I&#8217;ve been writing nearly every day, Invested in the Trailblazer course, read well over 30 books this year alone and countless blog posts. All of this knowledge and experience boils down to this simple statement:

It&#8217;s time to start.

For too long I&#8217;ve been biding my time. Deep within my brain there&#8217;s this belief that I&#8217;m not good enough. That I don&#8217;t know enough and that if I just read a little more I&#8217;ll be prepared.

This realization was instigated by two separate people: Seth Godin and Cal Newport. I finished rereading Seth&#8217;s book Poke the Box yesterday and the entire book is about starting. He presents the case, quite compelling, that it&#8217;s the initatiors who get things done. Those who ask questions and poke the box, as it were, to see what happens. No longer do the people who sit idly by and never start anything create amazing things. Only those who are willing to ask questions, take risks, and actually initiate new ideas make a difference.

In a similar vein, Cal Newport&#8217;s blog &#8216;Study Hacks&#8217; talks about pseudo-striving. In his experience, most people do things that make them feel satisfied like they&#8217;ve done something, but it actually doesn&#8217;t get them any closer to their goal.

One example he used was an aspiring writer. It&#8217;s great that this wannabe can sit down and bang out a few hundred words each day. But that&#8217;s not going to improve that person&#8217;s chops if they&#8217;re not actively seeking publication. It&#8217;s only through the rejection that comes from submitting one&#8217;s work that true learning and growth can take place.

Boom. Talk about a slap.

From two different people I&#8217;ve been indirectly told that I&#8217;m not doing what I need to do to actually achieve my goals.

Part of the problem is that I&#8217;m not 100% sure what my goals are. The other part is that it \*does\* feel good when I achieve my pseudo-striving goal. I get a little buzz knowing that I was able to write 1000 words today. Nobody will ever see those words, but in my head I&#8217;ve convinced myself that I&#8217;ve improved my writing chops and done something worthwhile.

I&#8217;m not saying writing things that you don&#8217;t plan to ship is bad. On the contrary, journaling is exactly that and it is a huge benefit to the writer. But everything you write cannot follow this paradigm. There needs to be a constant push to get outside of one&#8217;s comfort zone and poke that box.

How do you know if you&#8217;re works any good or if you&#8217;re actually improving if you never put yourself out there? Getting rejected is a good thing because it allows you to test, get feedback, refine, and then go out there again.

This goes hand-in-hand with starting. It takes initiative to take your work and put it out there for the world. Initiating that step takes a lot of effort. It&#8217;s hard. But it&#8217;s worth it. It&#8217;s the only way that epic shit gets done. You have to put it out there and see if it resonates with anyone.

I&#8217;ve been wanting for a while to restart my Better Grad Student blog. But I haven&#8217;t for many reasons.

I believe I didn&#8217;t have enough experience.  
I wanted the blog to look &#8216;perfect&#8217; before I get started.  
I desired to improve my writing to some undefined level.  
I thought I didn&#8217;t know enough about blogging or business or even grad school to make it work.  
I felt like I would get in trouble or be ridiculed for doing it.

In fact, none of these are true. When I take the time to really examine them, they&#8217;re all based around fears that aren&#8217;t grounded in reality. I&#8217;m creating reasons why I shouldn&#8217;t start this blog again instead of facing the fact that I&#8217;ve just been scared and lazy.

It&#8217;s easier to feel like I&#8217;m doing something (pseudo-striving) than it is to actually do the thing. But no amount of pseudo-striving is ever going to get me to my goals.

With all that said, I&#8217;m about to say something that scares the crap out of me. On June 1st, 2012, I&#8217;m going to relaunch Better Grad Student.

I know it&#8217;s not going to be perfect. Nothing ever is. But it will be started. There will be time to correct the course and polish things up once things get rolling.

My goal will be two posts per week. That&#8217;s an actionable goal that I know is achievable within the framework of my current studies. This is going to require a lot of hard work on my part. A lot. But it&#8217;s worth it. If I can help one person, if I can add value to one individual&#8217;s life, then it will have been worth it. Do I want more out if it? Perhaps. But I need to focus on adding value and making it happen.

And I do that by starting. Push it out the door and give it to the world.

31 days. It audacious. It&#8217;s bold. But it&#8217;s just right.