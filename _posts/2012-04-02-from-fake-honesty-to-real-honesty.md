---
id: 107
title: From Fake Honesty to Real Honesty
date: 2012-04-02T15:38:36+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=107
permalink: /from-fake-honesty-to-real-honesty/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Future
tags:
  - artist
  - creative
  - desires
  - divorce
  - dreams
  - duality
  - engineer
  - honesty
  - lies
  - living
  - omission
  - success
  - truth
---
With as much emphasis as I place on honesty, you think I’d do a better job of it sometimes.

My life is somewhat of a lie right now. It’s not a lie in the sense that I’m intentionally deceitful or manipulating people to get what I want. Rather, I’m guilty of the lie of omission.

I’m living a duality at the moment. On the one side there’s Taylor the Engineering Graduate Student. People at work know some of what I’ve been going through and they know what I do pretty much every day at school. But when I get home, I take off that hat and become Taylor the Creative. Indeed, it is like removing a hat because when it’s off, I don’t put another one back on. The grad student hat is a part of a character I feel I have to play.

My changed values have been separate from where I spend a majority of my time. Nobody really knows I want to be a writer. Very few people know I’ve been blogging and want to restart my Better Grad Student blog. Some folks know I’m an atheist, minimalist, mostly vegan, and barefoot runner, but even more don’t. Everybody still thinks I’m on track to graduate in a year and go take a high paying job that makes me halfway miserable but comes with a good paycheck.

They don’t see the Taylor that wants to live on his own terms and not do the 9-5 thing.

But that’s going to change very soon. On wednesday, my divorce is finalized and I become a single person in the eyes of the law. The next chapter, if you will, of my life begins as another closes.

Soon after, I plan to eliminate the gap between my personas. I will merge my old twitter account with my new one. I’ll share my blog with others. My profile pics will be updated and my physical self will be congruent with my digital self. I’ll stop living in shadow and being afraid of how others will perceive who I’ve become.

People will know that I want to write and be more ‘artsy’. They’ll know that I want to earn money online in a location-independent manner. It will become readily apparent that I have no intention of finding a traditional job right out of grad school.

It’s scary, but it needs to happen. To be able to achieve my dreams and really be fulfilled and successful I need to be honest about who I am. I can’t pretend to be one thing at the office and another online. It’s just not going to work. And it stops this week.

Over the past few months I’ve built up the courage to first define better who I am, what I want, and what fulfills me. I’ve also built up the courage to say those things out loud to myself and close friends. Now’s the time to share it with the world.

Honestly, though, it scares me. There will probably be some people that don’t get it. That’s okay. It’s not their life. As long as I’m confident in my beliefs and choices, then who cares what others think? Will it be uncomfortable and awkward at times? Yes. Is it worth it?

Hell yes.