---
id: 352
title: You Have Permission to Fail
date: 2013-04-10T19:19:21+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=352
permalink: /you-have-permission-to-fail/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Comfort Zone
  - Learning
  - Personal Development
tags:
  - fail
  - getting shit done
  - growth
  - writing
---
You have permission to fail. You have permission to not be good. You have permission to try.

For anyone starting a new venture this should be a mantra. **It’s okay to suck**. If you’re sucking at something that means you’re _doing_ it. And sucking at something is better than not doing it at all.

As I enter the next journey/phase/whatever of my life this belief is something I realized I&#8217;ve never had. Up to now I believed that I had to succeed. I had to be perfect and do it right. I believed I couldn’t fail or let anyone down or look bad. If I did that then everyone would see right through me and find out I&#8217;m a fraud.

But that’s not the case. The truth, frankly, is that no one cares. Yes, your parents and close friends care about you and love you. But they have their own things to deal with. Everyone has their own thing to deal with.

What’s on your mind more: your success or the success of someone else? How often do you actually care if somebody else does a bad job? Only if it influences your life, right? So what’s the harm in trying something and giving it your best go? What’s the worst that could happen?

The desire to do well, to do something perfect, can be crippling. We see where we want to be. Our Point Z, if you will. We have good tastes and know the type of thing we want to create. But we know that our current abilities, our Point A, won’t match up to our tastes. So we wind up doing nothing. We don’t create. Or if we do create, we don&#8217;t push it out to the real world. Better to not do anything at all than to deal with the scrutiny that comes from comparing the reality of what you created to the fantasy of what you had imagined.

That line of thinking gets us no where. It doesn’t help you and it doesn’t help anybody else. And it doesn’t get you closer to Point Z. For a while it seems like it’s easier not to do anything. There are a million and one things on the internet that can distract from the worry and anxiety of not doing what you want to do. And that placates us for a while.

But the desire doesn’t disappear. Your creativity, good taste, and imagination don’t die. They’re just buried behind layers of fear and doubt. We don’t want to do anything to bring those insecurities to light and so we end up sabotaging ourself in various ways.

Those feelings never go away. I can guarantee you everybody from Tim Ferriss down to a wannabe writer you’ve never met has the exact same fear and doubt about their work. The only difference is Tim is the guy who pushes through those feelings and does it anyway. Very few people know about Tim’s failures. But they sure as hell know [about](http://www.amazon.com/The-4-Hour-Workweek-Anywhere-Expanded/dp/0307465357/ref=sr_1_3?ie=UTF8&qid=1365639497&sr=8-3&keywords=tim+ferriss) [his](http://www.amazon.com/The-4-Hour-Body-Incredible-Superhuman/dp/030746363X/ref=sr_1_2?ie=UTF8&qid=1365639497&sr=8-2&keywords=tim+ferriss) [successes](http://www.amazon.com/The-4-Hour-Chef-Learning-Anything/dp/0547884591/ref=sr_1_1?ie=UTF8&qid=1365639497&sr=8-1&keywords=tim+ferriss). He gave himself permission to fail. Often. And every once in a while he didn&#8217;t fail.

You have permission to fail. Go do it.