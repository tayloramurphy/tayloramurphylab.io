---
id: 403
title: 'How to Survive When You&#8217;re Getting a Divorce'
date: 2014-06-16T17:30:57+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=403
permalink: /how-to-survive-when-youre-getting-a-divorce/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Learning
  - Personal Development
  - Relationships
---
Here is a [link to the audio version](https://soundcloud.com/tayloramurphy/how-to-survive-when-youre-getting-a-divorce) of this post.

&#8212;

Getting divorced is something that no metaphor can succinctly describe. The amount of loss inherent to the act is astounding and hearing about it pales in comparison to the actual experience. One of the first things I had to learn once I moved out was how to be okay being alone. The person I had spent a large part of my time with for the past several years was no longer there. The support structure that we&#8217;d made had its foundations destroyed and I was left floundering in a sea of confusion and sadness.

The immediate desire when you&#8217;re placed in this situation is to distract yourself. It&#8217;s so easy to seek the comfort of distraction. Whether it&#8217;s drugs, porn, sex, TV, video games, cutting, or any other vice, they all offer something in common: for brief moments you&#8217;re able to forget what your life is like. You&#8217;re able to float above the sea and imagine a life where none of the shit you went through actually happened. That moment is short. Soon you&#8217;re falling down, back into the water, back into your life. But now you&#8217;re coming in so fast that you crash under the surface. You&#8217;re ten feet under, then twenty feet, then thirty feet. At some point it feels like you&#8217;re never going to make it back up. But you do. You take a deep breath of air, look around and see that you&#8217;re right where you started. Only it&#8217;s a little darker. The water a little murkier, and you&#8217;re hurting just a little more. The distractions are still there. They call your name and beg you to come back to them. They promise that moment of bliss where you forget it all. But they don&#8217;t remind you of the fall. Just the flight.

And so you repeat these distractions over and over. Knowing it doesn&#8217;t get you anywhere, but going back to them because it feels good for a moment. And all you want is to just feel good. Happy. Better.

My friend is going through this right now. I went through it years ago. It sucks. A lot.

But it doesn&#8217;t last forever. Every once in a while you&#8217;re able to reach out and ask for help. Friends or family offer to help and you may or may not take it. But you know and they know that, ultimately, you can&#8217;t be brought out of the sea by someone else. To extricate yourself from the miasma is something that you have to choose to do on your own. You have to be willing to face the pain. You have to deal with it and you have to deal with it alone.

So you start swimming. You pick a direction and start going. Stroke after stroke you make progress. Eventually you find an island. It&#8217;s small, only enough room for one person. But it has everything you need. You get on the island and dry your clothes. Wash your face, eat a meal. When you&#8217;ve taken care of yourself you&#8217;re then able to look around at the sea surrounding your island. The painful memories. The tears, the words, the broken dreams. You sit down on the sand and just look at it all. You cry. You laugh. You think and you sleep. You sit there and just be.

At some point you look around and notice that the water has receded a little bit. You have more space on the island. With this extra space, maybe you take up a new hobby. Inexplicably, there&#8217;s a movie theater on this island, so you go see a few shows. After some more time passes a strip mall appears. All your favorite shops and activities are available to you. You do some of them. Some days you don&#8217;t. Some days the tide comes back in a little bit, but it eventually recedes.

More time passes and you realize that you&#8217;ve been alone for quite some time. But you also realize that you&#8217;re OK. You&#8217;re, dare you say it&#8230; happy. You&#8217;re taking care of yourself and things are looking brighter, as if a filter was removed. You notice the sun and the clouds. The breeze feels good on your face. The moon looks more and more beautiful.

The water has almost fully receded now. You look out and see a lot of land surrounding your island. So many different ways you can go. So much possibility. So you start walking. Quickly you get tired. Blisters appear on your feet and your knees hurt. You turn around and go back to your island. You rest for a while. Then you go back out. You make it a little further before you start to hurt. Weeks pass. Each day you go a little farther, careful not to over do it, but far enough that it&#8217;s a challenge.

One day, after walking for a while you look up and see all new scenery around you. There are other people and buildings and creatures. Everything seems different. You look at yourself and notice you&#8217;re different as well. Your back is straighter, your skin is a little tanned. You hold your head higher and you feel sexier. You spend some time in this new place and it feels good. Better than the distractions made you feel. And the feeling persists. It stays with you. You want to stay but you eventually have to go back to your home. The walk feels shorter this time.

You get home and lay in bed and you realize you&#8217;re OK. You&#8217;re happy. You&#8217;re still you. The next morning you wake up and look out the window. In the distance you see the small pond into which your sea of darkness has gathered. For a while now it hasn&#8217;t changed size. It&#8217;s out in the distance but not over the horizon. It reminds you of what you went through and what you struggled with but it doesn&#8217;t have any more power over you. You&#8217;re not defined by it anymore. When you invite friends to your house if they look out your window they can see your pond of darkness too. Sometimes they&#8217;ll comment on it. Sometimes they don&#8217;t. Maybe you sit on the porch with a friend and talk about it. But maybe you won&#8217;t. Maybe you go on walks around the pond if you feel compelled. But maybe you won&#8217;t.

After all, there are so many other places to walk.

&#8212;  
I wrote this to share two points about going through a divorce. First, you have to learn to be OK by yourself. And second, you have to take small steps to improve. This process takes a unique amount of time for each person. There is no right way to do it but there are definitely wrong ways. Each person is different. To my friend: I hope this helps in some small way.