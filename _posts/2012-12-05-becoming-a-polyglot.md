---
id: 253
title: Becoming a Polyglot
date: 2012-12-05T20:12:29+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=253
permalink: /becoming-a-polyglot/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Learning
  - Personal Development
tags:
  - esperanto
  - french
  - languages
  - polyglot
  - spanish
  - travel
---
Avant mes 30 ans je vais apprendre le français, l&#8217;espagnol, et l&#8217;espéranto.

Antes de que mi 30 cumpleaños que voy a aprender francés, español y esperanto.

Antaŭ mia 30a naskiĝtago mi lernos la franca, hispana, kaj Esperanto.

Before my 30th birthday I will learn French, Spanish, and Esperanto.

Some of you may know of my desire to learn multiple languages. It’s one of those things I’ve always had a passing interest in but never seriously believed I could do. So why, seemingly all of a sudden, do I believe I can learn 3 languages in the next 5 years? And what the heck is Esperanto?

One of the many bloggers I follow is Benny Lewis from [Fluent in 3 Months](http://www.fluentin3months.com/). He’s a polyglot who speaks 9 languages and who’s current mission is to learn Egyptian Arabic, while in Brazil, using French language books. Keep in mind he’s a native English speaker and just a few years ago only knew a single language. He, along with many other online personalities, has convinced me that I can do this well _and_ do it in less time than I had previously thought. It simply takes focus and dedicated practice. All that crap I did in the past with learning grammar and memorization was mostly useless. How did I learn English? By struggling through speaking it. Then came the grammar and polish. My approach to other languages will be much the same: lots of awkward practice speaking it with less tedious memorization of rules and vocab.

So why this choice of languages? French and Spanish probably are obvious choices. I’ve been to Spain, Mexico, and France which are, obviously, the best countries for using those languages. The [UN](http://en.wikipedia.org/wiki/United_nations) lists French and Spanish among its official languages. There’s a large percentage of the population in North America who speak Spanish and French. I don’t think I need to convince you why it would be useful to learn these languages.

But what about [Esperanto](http://en.wikipedia.org/wiki/Esperanto)? This is a language invented in the late 1800s and it was intended to be easy to learn, politically neutral, and bring about world peace through a common language. It’s estimated there are between 10,000 and 2,000,000 Esperanto speakers in the world. Wikipedia even has well over 150,000 articles in Esperanto.

It seems like an odd choice for a language, but there’s something about it that’s intriguing to me. First, it’d be like a special club. Since so few people speak it, it’d be a relatively unique skill, but there are enough people who do speak it to make it worthwhile and fun. I have a higher chance of running into someone who speaks Spanish and French, but there is still a good chance of meeting someone who speaks Esperanto (particularly in Europe, South America, and East Asia).

Second, from what I can tell, it really ease relatively easy to learn. Wikipedia claims that native French speakers who attempt to learn multiple languages can reach a respectable level of fluency in Esperanto in 150 hours (compared to 2000 hours for German and 1500 hours for English). If I’m learning French and Spanish, it seem that Esperanto won’t be too taxing.

Finally, it seems like it’d be a hell of a lot of fun. If any of my friends were to learn it (nudge nudge) we’d have our own pseudo-secret language to communicate in. I suppose we could learn [Klingon](http://www.youtube.com/watch?v=EMipQm5JZgI) or [Dothraki](http://www.youtube.com/watch?v=bhWpNJgT9DI), both very cool languages, but Esperanto has more real-world use. (And it has a really cool [flag](http://en.wikipedia.org/wiki/Esperanto_symbols).)

Learning three languages is not a simple task but the payoff would be amazing: the ability to communicate with a larger portion of the world. English is a seemingly universal language and arguments could be made that it’s not necessary to learn another language. To me, that’s a very narrow and selfish point of view. I believe those who are willing to learn how to communicate with others are far better off than those who choose not to. Being a polyglot is something to be praised, not condemned.

Hura por lingva lernado kaj la persekutado de scio!