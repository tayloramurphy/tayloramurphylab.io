---
id: 12
title: 'Let&#8217;s Be Realistic'
date: 2012-01-19T22:44:12+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=12
permalink: /lets-be-realistic/
brunelleschi_featured_post_position:
  - left
brunelleschi_featured_post_width:
  - 1/4 Width
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Dreams
tags:
  - "2012"
  - limits
  - mediocrity
  - realistic
---
A coworker of mine said that to me the other day, &#8220;C&#8217;mon Taylor, be realistic. You can&#8217;t do that. Be realistic.&#8221; I don&#8217;t even remember what we were talking about, but that phrase stuck with me.

**Be realistic.**

I&#8217;m here to say that is utter bullshit. I am done being realistic. I have lived my entire life as a realistic individual and it has taken me to a place of which I wanted to part.

Being realistic means you are artificially setting limits for your self. Why would _anybody_ want to truly be realistic?? Being realistic means you are consciously choosing to limit your imagination and your potential.

I never realized the truth in that mindset before. Being realistic about anything you do, sets yourself up to be happy with mediocrity. I&#8217;ve seen it over and over again in my own life, but I&#8217;ve never paid attention to it. You know why? Why would I pay attention to something that is so common to me?

When you&#8217;re going about your day, do you notice the feeling of the clothes that you&#8217;re wearing? Do you notice every time you blink? When you&#8217;re comfortable, do you notice the air that surrounds you?

Of course not! You&#8217;re comfortable! Why would you pay attention to something that is such a common occurence?

Being realistic has the exact same tendency. Working as hard as you can for mediocrity is something that a lot of us do all the time. Everybody hears the platitudes: shoot for the stars! and  Aim high! But the reality is that almost nobody is willing to do that. Why shoot for the moon when I can just aim a little lower and have a much easier trip?

Our society supports, and practically encourages, this type of thinking. When we don&#8217;t pay attention to the pernicious attitudes of mediocrity, it creeps into our lives and infects our mentality. Only by paying attention to this sort of thinking, acknowledging that it exists, and then pushing it the hell away from us can we truly start to be _un_realistic and do something awesome with your life.

**Be UNrealistic!**

2012 is going to be a year where I am not realistic. I want to be as unrealistic as I can be and live a life that is freaking epic.

What are you doing in your life that is realistic? How are you, or the people around you, keeping your frame of mind in a state of mediocrity?