---
id: 115
title: How I Almost Gave Up Before Even Starting
date: 2012-04-07T23:01:56+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=115
permalink: /how-i-almost-gave-up-before-even-starting/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
tags:
  - belief
  - fear
  - grad
  - happiness
  - happy
  - hopeless
  - message
  - realization
  - share
  - story
  - student
  - studyhack
  - unique
  - value
---
I recently discovered the blog StudyHacks and was immediately overwhelmed with a wave of anxiety doubt.

Study Hacks is all about trying to decode patterns of success in both the student and working worlds. It was started by a guy in graduate school and has turned into three books and a seemingly lucrative career for him.

When I started reading through his posts, that niggling voice in the back of my head became a loud siren. Here is somebody that’s doing basically what I want. He helps students figure out their shit through his writing.

For about an hour I was having a mild panic attack. My opportunity was gone. It was gone 5 years ago when he started this blog. I might as well give up trying to do this and just get a regular job. There’s no point in trying because somebody’s already closed up the market. Now I’ll have to be miserable in my job for 40 years and retire with an ulcer and a life I don’t want.

Crazy, right? Once I started to breathe again and my mind came back to me, I started to analyze where this emotion was coming from and whether it was based in any sort of reality.

Going through it I realized I was afraid of several things. To me, it seemed like this guy had a corner on the market. He has 3 books and several years worth of posts. What could I possibly hope to add? He also writes very well and seems to have many connections within academia from which to draw experience.

From this vantage point, it does seem like he’s got it on lockdown. But then I realized, he is missing one thing: me. My story is completely unique. _Everyone’s_ story is completely unique. What I have to offer the world is completely special because my experiences and personal twists are all my own.

That right there is the value I have to offer the world.

I’ve recently come to the almost-conclusion that nearly all we need to know to live happy and fulfilled lives is already known. I’ve been reading a lot of books lately and, while it’s admittedly small compared to the whole body of knowledge in the world, I keep running into very similar themes.

Many people are rehashing the same ideas over and over in different ways. Each iteration has a little twist, but they’re essential based around similar ideas. Honesty, love, power, understanding why, psychological barriers, and on and on.

The more I read, the more I think it’s a good thing that people keep sharing the same stuff repeatedly. For one thing, each story has its own unique angle. That unique value proposition is what keeps the themes fresh.

For another, people aren’t getting the message. There are so many folks in this world who just don’t get it because they’ve never heard it. With every generation we need more and more people to share how to live a better life and be a better person. Even if that person is saying the same thing as someone else, perhaps it will resonate with someone new because of their unique point of view.

And so I applied this same line of thinking to my Better Grad Student idea. Sure, this dude has some books and a few years of blog posts. But he doesn’t have my story.

That’s what I’m bringing to the table. That’s where I can add value and make a real contribution. Clearly I’ve never heard of him, and neither have most of the people I work with. So there are still people that need help and haven’t been reached. There’s room for me. There’s room for a lot of people.

So he can have his audience. I’ll build up my own. People that resonate with my story and find value in what I have to say. I believe we all have a story worth sharing that many of us don’t ever actually take the time to voice. I’m going to change that. My story will get out there. I can help people with my words. And I can make a living doing it.

It just takes time.