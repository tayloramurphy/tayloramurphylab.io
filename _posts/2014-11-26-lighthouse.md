---
id: 427
title: Lighthouse
date: 2014-11-26T18:32:26+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=427
permalink: /lighthouse/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Future
  - Learning
  - Personal Development
---
<p class="p1">
  I don’t know what I’m doing with my life professionally. You probably don’t either.
</p>

<p class="p1">
  Well, maybe you do. Perhaps you’re one of the lucky few who came out of the gate knowing precisely the direction they wanted to head. You know those people, the ones who had a clear vision of where they wanted to be as an adult, even if some of the specifics weren’t nailed down. The woman who’s known for a long time that she wanted to work with the poor in another country. The man who knew he wanted to direct movies since he was 3. The one who wanted to fly planes in <i>any </i>capacity.
</p>

<p class="p1">
  These are not the people I’m talking about. I’m talking about the rest of us. The ones who are kind of adrift and maybe have some kind of idea about what we want but aren’t really sure because there are just so man things that look appealing. You could start a business, you could write, you could make movies, you could be an electrician… There are so many options.
</p>

<p class="p1">
  But really there aren’t. At least from my perspective there aren’t. As the days and years tick by, the options that are available to me slowly decrease. It’s unlikely that I’ll ever become an astronaut. The requisite training and the prior certifications would take too long.
</p>

<p class="p1">
  It’s unlikely that I would become a very successful politician. I don’t really have the background or interest.
</p>

<p class="p1">
  It’s highly unlikely that I’ll become a star athlete. I’m not very good at many sports and I haven’t been doing any one sport since I was a kid. Plus I hurt my shoulder and I have a bum wrist.
</p>

<p class="p1">
  There are still a huge number of things that I <i>could</i> do. I could write. I could try to become an actor. I could sign up for a trade and work with my hands. I could go back to school and get a law degree. Or an MD. Some are more likely to happen than others and some are more interesting than others, but that’s due to my own unique proclivities more than the fields themselves.
</p>

<p class="p1">
  The truth is that I’ve acted without knowing what I wanted. Many of my decisions have been based on <i>avoiding</i> something rather than heading towards something. I suspect that this isn’t a terrible path to live. It’s turned out quite well for me so far. I have a great and interesting career that I suspect some are envious of. But I am painfully aware of how I’ve only been half-way pursuing it for some time. Not all my eggs are in my career basket. And maybe they don’t have to be.
</p>

<p class="p1">
  I’m fortunate enough to have a job that gives me a significant chuck of free time. Perhaps not as much as I would like, but it’s enough to do something with. Enough for me to write this and then some. And I got here because I was mostly lucky but also because I was just moving. I kept trying things and once I picked a direction I saw it to the end.
</p>

<p class="p1">
  Halfway through my PhD I didn’t really want to continue with it. I wanted out but I knew that I would hate myself if I never finished. So I stuck it out, got those letters, and then high-tailed it out of academia. Next was that whole freelance thing but that didn’t work either. I was lonely and depressed and broke. Now I’ve got a job that’s allowed me to claw my way out of debt, save for my health and wealth (HSA and 401k, respectively) and it’s given me some spending money on the side. It’s not a bad gig.
</p>

<p class="p1">
  But it’s also not completely fulfilling. I fell into it and got lucky. I didn’t pursue it with dogged perseverance and then get lucky. There’s a critical difference in my mind. The former isn’t bad and it’s nothing to be ashamed of. The latter is glorious and worthy of admiration. At least to me.
</p>

<p class="p1">
  I admire those people that know, specifically, what they want in their work. They know they want to be writing, directing, plumbing, flying, leading and they go for it. Or at least they know they want to work with people at Disney, Pixar, Tesla, NASA, ESA, UN, or Doctors without Borders. They have a direction and, while there are many unknowns that they have to navigate, they have a lighthouse that keeps them on course even if the path is dark.
</p>

<p class="p1">
  Even more admirable in my mind are the people that are doing exactly what they want every single day. They don’t go to work and think about doing something else. They are doing precisely what they want. They had their lighthouse and they’ve now arrived. I suspect this is a rare thing, but it happens. I met a voice actor who loves what he does completely. He had a very strong lighthouse and forged a path to it.
</p>

<p class="p1">
  I don’t have that lighthouse in my career. (I’m very fortunate that this didn’t hold true for relationships. I had a clear lighthouse in mind for the type of woman I wanted to be with and it just so happened that Lindsay was the one standing there when I arrived.)
</p>

<p class="p1">
  I think most of my lighthouses used to be centered around religion, but once that was revealed to be a bunch of smoke and mirrors, I had no more strong lighthouse out in the distance. So I started moving away from things I didn’t want. This isn’t an all too bad way to live (evidenced anecdotally by me) but it feels like I’m always looking in my rearview mirror trying to make sure that what I don’t want is definitely behind me. My eyes are barely on the road and the things I don’t want have most of my attention.
</p>

<p class="p1">
  I’d like to change that at some point. I want to break off that rear view mirror and throw it out of the car. I’m tired of looking behind me. My past is there and it always will be. So where am I heading? I don’t know. I can’t see the road beyond my headlights. I don’t know if I’m heading for disaster or the promised land.
</p>

<p class="p1">
  But I have to keep driving. I have to keep moving.
</p>

<p class="p1">
  It feels like I’m starting to see a light in the distance. The more I move the brighter it gets. There might even be a few lights out there and I’ll have to pick which one to head towards.
</p>

<p class="p1">
  I need and want that lighthouse in the distance. If you’re lucky enough to have one, go towards it. Steel yourself for the challenges and put pedal to metal. The rest of us? Keep moving. Your lighthouse is out there.
</p>