---
id: 195
title: Defining Success
date: 2012-07-19T22:05:47+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=195
permalink: /defining-success/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Dreams
  - Future
tags:
  - desire
  - dreams
  - effort
  - goals
  - hard
  - mediocre
  - minimalism
  - success
  - successful
  - travel
  - work
  - writing
---
A 6 figure home. A luxury sedan. 2.5 kids. 0.8 acres in the burbs with neighbors you only sort-of like. A large TV with hundreds of movies. Garage and closets jammed with stuff. Thousands in debt. Crammed schedules. Lack of sleep.

Is this what success is?

Ask any of the over 300 million Americans and they probably can’t tell you what success is to them. Nobody has ever really thought about it and so they fall into the easiest pattern there is.

After all, this is what we’re supposed to want. When you can check everything on this list, that’s how you know you’ll have made it. At least that’s what we believe.

The media, friends, family, and your own inertia all reinforce the belief that success is narrowly defined. That, whether you like it or not, you need that home, car, kid, and stuff to be considered a success.

### **What happens when you ask a person what success is to them?**

Honestly, I don’t know. I can only answer for myself.

My answer? _I’m still defining it_.

Throughout my academic life, also known as my entire life from age 4 until now, I have been slowly lurching toward a narrow view of success. I don’t believe there was a conscious decision to head towards the picture I described above, but looking back it sure seems like there was a choice.

That’s because I never took the time to sit back and think about what being successful meant to me.

Is it a certain amount of money in the bank? What about the things I own? The experiences I have? What was the purpose of all of it?

These sort of existential questions are difficult. And because they’re difficult, people avoid them. They choose the myopic route that leads to quick pleasure but eventual despair and unrest.

It is much easier to work towards a fantasy than it is to be specific. Wishing that you had the lifestyle of so-and-so and dreaming about all you could do if only somebody would just give you the chance takes zero effort.

### Climbing a Mountain

It takes nothing to stand at the base of a mountain and imagine yourself at the top. It takes everything to make it to the top.

Some people may be happy at the base, doing what they do, and glancing up every once in a while to the summit to enjoy the scenery.

Some repeatedly try to climb but aren’t fully prepared for the challenges and so they slide back down.

Others climb hills and proclaim loudly that they’re on the mountain.

Even more don’t see the mountain.

That mountain is your personal definition of success.

If you’ve defined your own version of success, then the mountain is well-formed. But there’s a distance between where you are and where you want to be that requires sustained effort to overcome.

Some people glance at their success and never chase it. Others keep trying but fall into insanity because they keep doing what doesn’t work. They’re probably afraid of actually achieving their dreams. Still others do something else and convince themselves that was their dream all along, ignoring the shadow cast by where they could have been.

The worst position to be in is to not even see the mountain. That’s a crowded place to be in. The majority of people aren’t on their mountains and so they occupy flatlands.

If you don’t have your own definition of success, then you’re competing with everyone in the world for the same patch of land. It’s hard to be mediocre because everyone is doing it.

By creating your own mountain, you separate yourself from the rabble and move into something out of the ordinary. It’s lonely being a wolf at the summit, but it’s better than being a sheep in the flock.

Your mountain, your success, is your beacon in the night. It’s a guiding compass that helps you correct course. It can change appearance, but it will always rise above the noise.

### My Journey to Definition

Determining what my success looks like has been a slow process. It’s not something that I claim to have completely figured out, but I am making progress.

My current definition of success includes these traits:

  * Freedom to travel
  * Location-independent living
  * Financial abundance
  * Writer
  * More free time

These are 5 traits that, admittedly, aren’t specific at the moment. I’ve got some more detailed descriptions in my head, but for now, they’re a good starting place.

I know that having more specific goals will help make these traits actionable, but for the time, I’m still buiding up a solid framework.

My mountain is taking shape and I’m gearing up to climb it.

The only thing standing in my way is me.

**What’s your definition of success?**