---
title: Prepping for a Pandemic
date: 2020-05-14T14:21:00+00:00
author: Taylor Murphy
layout: post
permalink: /prepping-for-a-pandemic/
---

On March 1st, GitLab decided to cancel the Contribute conference due to concerns about COVID-19. This was the first major event cancellation I'd heard of in the United States. 10 days later the NBA cancelled their season.

Before either of these events happened, I'd heard rumblings about the novel coronavirus, mainly on Twitter and Reddit. A large part of me had assumed nothing would *really* happen in the United States and so there wasn't much I should do. Plus I had a lot on my mind with my new son coming in a month and my plans for wrapping things up at work to go on an extended parental leave.

I wish I had done more before everything went crazy to be better prepared for my family.

In the moment, it's difficult to separate the signal from the noise. I missed the Bitcoin signal way back in the day, even though I again had heard rumblings on it. You can't take action on everything you hear. To do so would be folly.

But in hindsight, particularly with COVID-19, I believe I could have done more. If I could go back in time, here's what I would have done differently.

First, I would have stocked up a bit on some basic food stuffs. Rice, dry beans, some canned food, water. Nothing too crazy but enough so that when there's a run on the grocery stores I don't feel that sense of panic. 

Second, I would have bought some masks and hand sanitizer to have for my family and for neighbors that needed them. I have no interest in reselling them, but again, the last time you should be trying to buy something is when everyone else wants it. 

Third, I would have come up better plans for child care for our toddler. We knew we were going to have our second son in either late March or early April (due date April 9). It was a bit of a scramble for us when he was born and it added to our overall anxiety.

Finally, I would have better tuned my information diet to filter out the panic-inducing sources. Perhaps I should do this generally, but being more selective about when and who I listened to about the coronavirus would have put me in a better and more controlled position.

---

Looking forward, I still need to do most of these things to better prepare for other situations. I don't intend to go full prepper on everything, but having some kind of plan in place is better. It will enable me to not panic and to feel more confident in what I should be doing.

I do know that in the future there will be more pandemics, power outages, floods, storms, car accidents, etc. I don't know *when* they will occur, but it's not a question *if* they will occur. I'd like to have a plan for each of these so I can feel confident in my response.

---

This post was partially inspired by the [Reasonable Persons Guide to Prepping episode](https://www.iheart.com/podcast/1119-worst-year-ever-49377032/episode/the-reasonable-persons-guide-to-prepping-59450250/) of Worst Year Ever.
