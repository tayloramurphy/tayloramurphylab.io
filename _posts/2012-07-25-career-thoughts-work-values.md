---
id: 205
title: Career Thoughts and Work-Values Inventories
date: 2012-07-25T09:52:20+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=205
permalink: /career-thoughts-work-values/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Learning
  - Personal Development
tags:
  - anxiety
  - career thoughts
  - creativity
  - excitement
  - fear
  - freedom
  - helping
  - knowledge
  - learning
  - personal development
  - pressure
  - stress
  - values
  - writing
---
I had mentioned in my last post that there were five different tests that I had taken as a part of my vocational assessment. I covered the Myers-Briggs Type Indicator in some detail which, I hope, gave you a better idea of my personality.

I’m going to cover two tests in this post: Career Thoughts Inventory (CTI) and Work-Values Inventory. I’ll cover the remaining two tests in two additional posts later.

## **Career Thoughts Inventory**

The CTI measures dysfunctional thinking with regard to career problem solving and decision making. When I took this test, my first thought was of how depressing the test was. Each question was formed around a negative question with very little positive language in use.

There were about 50 questions if I recall correctly, and all of them were depressing. An example would be “I find I often experience stress when thinking about my parents’ view of my career choices.” That’s not an exact quote from the test, but you get the idea. Sad city.

Overall, however, it gives a good indicator of the negative thought patterns and harmful beliefs that exist. The test produces four different scores which I’ll cover briefly.

**Decision-Making Confusion**

The first score is the Decision-Making Confusion (DMC) score. This is a reflection of the degree to which I experience confusing emotions and/or lack of knowledge that could potentially interfere with decision making. My scores indicated significant confusion about how to go about selecting a career. I have a lack of clarity regarding how to pursue the career decision-making process and this causes indecision and further stress.

This is a somewhat humorous notion considering how far along in my PhD work I am. With less than a year to go before graduation, here I am worrying about my career. Shouldn’t I have a pretty good idea of what I want to do already? Well the truth is, no, I definitely don’t. I readily admit that when I entered grad school I did it as a way to avoid the real world. Sure, the research was interesting and I wanted to learn more, but the underlying motivation was that I didn’t believe that I was a good enough engineer and so I had to get more education to be “good enough”. Whatever that means. So it’s only natural that, 4 years later, I would be facing those same decisions, just with more stress and pressure added onto it. Hopefully with some more wisdom and experience as well, but time certainly hasn’t softened the edges of those difficult questions.

**Commitment Anxiety**

The next score is called Commitment Anxiety (CA). It’s a measure of my ability to make a commitment to a specific career choice. This score was also significantly elevated indicating that I experience considerable worry or tension associated with the ultimate outcome of the decision-making process. This worry comes about from the need to choose the “best”, “right”, or “perfect” career for me.

What’s very interesting about this score is that my other tests indicate that I’m in a career path that’s actually a good fit for me. At least, people with my personality type and interests have indicated higher levels of satisfaction and fulfillment as a chemical engineer (if you’ll recall my previous post). My scores suggest, then, that what I’m doing may be a good fit for me, as long as I can create a situation that aligns better with some of my values. There seems to be some key traits that are missing from my work environment that are causing a sense of ennui.

**External Conflict**

The third score in the CTI test is called External Conflict (EC). This score speaks to the level of resistance that I may be experiencing in taking responsibility for decision making. Simply put, I experience a large amount of pressure, real or perceived, to please others around me as opposed to focusing on my own needs and wants.

This is a behavior that has been recurring throughout my entire life. It’s a learned behavior that partially stemmed from my parents’ divorce as well as from teachers and other mentors whose praise and admiration I desperately craved. This created situations where I would sacrifice my own happiness for the sake of others’. Sometimes it was a conscious choice, at other times is was unconscious. In the moment, it seems like the right thing to do. I want other people to be happy and a good way to do that is by doing whatever is necessary, even if it means sacrificing my own happiness. This can work for a while, but eventually feelings of resentment and bitterness arise and things start to fall apart. This is part of the reason my marriage ended and it’s part of the reason why I’m feeling so dissatisfied with my current work situation.

The pressure I’m feeling may or may not be real. Sometimes we tell ourselves something for so long that we finally accept the ‘truth’ of it. The belief that I’m expected to stay in science or academia might have a nugget of truth to it, but it shouldn’t affect my decision. I personally don’t have that belief and I shouldn’t care if other people _do_ have that belief. I make my own decisions about my life and can choose to take input from other people, but these others can’t control the choices I make. My intention is not to sound like a rebellious teenager. On the contrary, my aim is to be a man who is in control of his own destiny. This necessitates the clear statement to other people that you don’t control me. I am in charge of my thoughts, emotions, actions, and beliefs. I, and no one else, am responsible for my life, health, and happiness. The choice is mine what pressures to accept into my life. Now, that’s easier said than done, especially when certain behaviors have been life-long. But my standard for what a competent man is is quite clear. The only pressure that I have to live up to is my own.

**Total Score**

The final score is the CTI Total Score. This is an amalgamation of the other scores and the results are pretty obvious based on the other tests just discussed. My report indicates that I experience a significant level of difficulty in negotiating the career decision-making process. It goes on to list several themes that emerged in my responses of which I’ll highlight a few:

  * Views of important people in your life that interfere with you choosing a field of study or occupation
  * Interests always changing
  * Can’t think of fields of study or occupations that fit you
  * Feeling upset when people ask what you want to do with your life
  * Great deal of worry about choosing the “right” field of study
  * Settling on one field of study or occupation
  * Fear that you will not be successful if you enter your chosen occupation

These themes are pretty evident when reviewing the results from the other tests. I have some significant mental blocks when it comes to deciding what I want to do with my life and my career. I fully believe that these can be over come, but much like AA, the first step is admitting that you have a problem. Well I know I have that problem, the next steps are figuring how to deal with it and where to go from here.

## Work-Values Inventory

In the Commitment Anxiety part of the CTI test, I mentioned that there might be a misalignment between my values for work and the values of my current work environment. The Work-Values Inventory test aims to highlight which values I do and do not believe to be important.

It’s a very simple test wherein I list each value as Very Important to my choice of career, Reasonably Important, Neutral, Not Very Important, and Not Important At All. Here are the results of each category:

  * **Very Important**: helping society, helping others, friendships, making decisions, knowledge, creativity, excitement, moral fulfillment, and time freedom
  * **Reasonably Important** &#8211; public contact, working with others, competition, influence people, work alone, intellectual status, artistic creativity, change and variety, adventure, independence, location, community, and physical challenge
  * **Neutral** &#8211; power/authority, aesthetics, security, recognition
  * **Not Very Important** &#8211; affiliation, working under pressure, supervision, precision work, fast pace
  * **Not Important At All** &#8211; stability and profit/gain

Each of these had a blurb next to them fleshing out what each value represents, but I think most of these are obvious. At the end of the test I was asked to rank the five work values that are most important to me. From most important to least they were:

  1. Time Freedom
  2. Helping Others
  3. Excitement
  4. Knowledge
  5. Creativity

In mulling over these choices over the past few weeks, it’s become quite apparent how these values are either not present in my current environment or they’re not as fully realized as I would hope.

**Time Freedom**

The cool thing about being a grad student is that, for the most part, we can set our own hours. I can come in at 10 if I want to, or 6 if I need to. Lunch breaks are cool and leaving early on a friday is fine. The main thing is that the work has to get done. Where I feel like I’m lacking some time freedom is in the nitty gritty of my research. Taking care of cells is like having children. I have to take samples at 4 in the morning sometimes and I can’t ever leave for more than 3 days without either killing off my culture or getting somebody else to worry about it for me. There’s also the stress that comes from having a boss. I worry about whether going out for coffee with a friend in the middle of the afternoon is going to look bad if the boss comes looking for me. Or what if I stay real late one night and decide not to come in until late the next day? I’m sure most of the pressure I’m feeling is solely in my head, but not having a boss (I.e. Being an entrepreneur/self-employed) would alleviate these stresses. Of course it comes with a whole host of other stresses, but perhaps those are more favorable for me.

**Helping Others**

This is one that I get into disagreements about with my friends. I feel like I’m not helping other people as much as I can. When some of my friends hear this, they look at me like I’ve just told them I want to get ‘Awesome sauce’ tattooed on my forehead. They believe that because I’m doing research on cancer metabolism that I’m am very much helping people. I would agree with this statement… but only a little. The work I do is novel and is definitely contributing to the body of knowledge that will help us treat cancer. However, the impact that I have is very far removed from the people that actually have cancer. I’ve discovered that I need to have a more direct impact on people’s lives with my work. Sending my research in the scientific asteroid field where it joins it’s thousands of other siblings in the hope that it will come crashing down and make some impact is not good enough for me. I need a more direct approach. This is one of the reasons, I believe, that I’ve fallen in love with writing. With words and the stories they tell, there is an intimate connection between reader and writer. Personally, my life has been changed by words written by somebody else. I may never meet those authors, but they have had a _direct_ impact on my life. I want to do that for other people. I don’t believe that my research will have that effect. I may be wrong, but I doubt it.

**Excitement**

The plain truth is that excitement is few and far between in science. Perhaps it’s that way in many jobs. However, I equate excitement with new people, things, and experiences and changing circumstances. Science, at least in my experience, has a tendency to be very monotonous with only very brief flashes of excitement. This may be enough to sustain many people. I believe I need more excitement. Maybe when I get older I won’t need as much, but for now, in my mid-20’s, I need more excitement. And for me, that means meeting new people, going new places, doing new things, and reaching new heights.

**Knowledge**

Again, it may seem counterintuitive that I am lacking in this area in graduate school. After all, isn’t grad school where you go to learn more than you ever have before? Yes, that’s true, but there’s one piece of information missing from that previous statement. You learn more than you ever have before, but it’s narrowly focused into your specific field. From broad to narrow I am a chemical engineer who studies cancer metabolism in c-Myc-driven cancer by using metabolic flux analysis whose results are supported by techniques such as Western Blot, qPCR, O2 measurements, and cell assay analysis techniques. This is a very focused area in which I am considered an ‘expert’. This sort of focus is essential to fully understand the field and to make significant progress. I equate this level of expertise with Mozart. He spent his entire life on one thing: music. He was dedicated and focused and his contributions during his short life are still evident today. Many would probably argue that to make any sort of contribution to this world you have to have that sort of dedicated focus. Ever heard the phrase “jack-of-all trades, master of none”? It’s a pithy cliche that’s supposed to make the other person feel bad about having so many interests. But have you ever heard of a guy named Benjamin Franklin? The dude had about a zillion interests and we’ve elevated the status of the man to one of the Fathers of our country. I see myself as a kind of Ben Franklin, a ‘Renaissance Soul’ if you will, that has many interests. I get bored when I get too focused. I want to keep things interesting and learn about a great many things. Digging deeper and deeper doesn’t interest me when the walls of the hole are crushing me. I’m doing a disservice to my field and to science in general when I’m doing something I’m not that suited to. I’m meant to explore a broader range of interests and not get so focused on one particular thing.

**Creativity**

This value also stems from a dissatisfaction with science in general. The current state of funding is pretty dismal. A very small percentage of submitted grants actually get funded and those that do tend to propose research that is incremental. Things that are too creative or aren’t ‘proven’ probably won’t get funded. More results with less money is key. Speculative work that may not pan out just isn’t marketable in this economic climate. This creates a situation where creativity and ingenuity is stifled. Being creative in science is a big risk, big reward game. Trying something new and untested may lead to some great discoveries, or it may lead to nothing. It’s the fear that nothing will come out of it that keeps people from trying and it keeps agencies from supporting those that might. Science is very much under the purview of the old guard. The rapid changes and advances of the internet and this new digital age are slow to appear in the ivory towers of academia. Old mentalities and business as usual folks dominate the marketplace. Slowly this is changing, but it’s hard to bring about this change from within. I want to foster the types of environments that encourage creativity. Creativity breeds insight. Insight leads to new discoveries. These new discoveries change people’s lives, increase their well-being, and generally make people happier. When the focus is just on churning out more papers, the real goal of science, that pursuit of knowledge and the betterment of humanity, is lost. Or at least pushed to the side. I need an environment where more creativity is encouraged and supported.

**Overview**

These five values were chosen somewhat hastily as I recall. I did this because I didn’t want to give myself a chance to second-guess and put down what I think I ‘should’ve’ put down. The more I cogitate on these values, the more I find myself resonating with them.

These are pillars upon which I can make decisions and create a life that is fulfilling and has deep meaning. Is it possible to live and work without these? Absolutely. But would I be living up to my full potential without them? Definitely not.

I would encourage each of you to take some time and figure out which values are important to you. Use the values listed above (the ones ranked Important to Not Important) and find which ones resonate with you the most. If you find a mismatch between your stated values and the values that are present at work, perhaps it’s time to do something about that. Bring about the change that will help you live and work to your potential.

**_If you’ve taken the time to figure out your values, please share them in the comments. I find it fascinating to see what other people value the most. _**