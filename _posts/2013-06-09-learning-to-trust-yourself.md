---
id: 367
title: Learning to Trust Yourself
date: 2013-06-09T21:53:26+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=367
permalink: /learning-to-trust-yourself/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Learning
  - Personal Development
tags:
  - 80/20
  - hard
  - how to not suck at life
  - motivation
  - pareto
  - trust
  - work
---
For the first time in a long while I’m beginning to trust myself. It’s been about six months that I’ve struggled with trusting myself in thoughts and actions. But now the evidence is accumulating and I’m making progress.

When I say trust myself, I mean actually believing that I’m going to do the things I say I’m going or want to do. For a long period last year I was very consistent with my writing. I wrote every day and I shared my work regularly. However, even though I was writing every day, I wasn’t giving the craft its due discipline.

Part of being a writer is sitting your butt down in the chair for a good portion of the day. I used to have a goal of 1000 words a day. That was a decent goal to get me started, but I now know I’m capable of doing more. I can write about 3000 words in an hour when I sit down and refuse to get up until 60 minutes have passed. I’ve now done it consistently for more than two weeks and I have zero desire to ever stop. It’s a damn good feeling to write more than 15,000 words in a week. Now, I’m starting to trust myself that I can sit and focus when I need to.

I’m also trusting that I can work hard when necessary as well.

My girlfriend is an amazing woman. She works 12-hour shifts at the Children’s Hospital where, except for her lunch break, she is focused and working for the entire time. I can’t even begin to tell you the last time I worked consistently for 12-hours straight. I’m sure I could if called upon, but it would probably require that external force to make it happen. Now that I’m <del>unemployed</del> self-employed and working on building a freelance business and a writing career, all of my focus and hard work must be internally motivated. That has always been a big challenge for me.

However, over the past several weeks I’ve been proving to myself that I can do it. At the end of the week, on Sunday, I’ve been setting out the major goals and themes of my week. How many hours I need to spend writing, researching new leads, sending out pitch emails, applying for gigs, and doing general life maintenance tasks such as sleeping, exercising, and cleaning. These have probably become the most productive weeks of my life simply because I’m choosing to do the work that aligns with my goals and supports the life I want.

Of course there are times when I’m not productive. Distraction and procrastination are habits that die hard (if they ever die…). There are some days where I catch myself browsing YouTube or Reddit for extended periods. Those two sites are my biggest distractions so, for the month of June, I’m not visiting those sites at all. Again, I’ve been pretty damn good about following that. I’ve been to YouTube maybe 3 times and for all but one occasion it was to cue up a song I wanted to hear. Is it perfect? No. But I’m trying to get away from the need to be perfect. The plan that you follow perfectly 80% of the time is better than the plan you follow 100% of the time but then give up on.

I’ve also been more forgiving of myself. I have a tendency, as I’m sure most people do, to think about their day and view with a negative lens.

“Oh, it was a bad day because I watched an hour of Netflix, ate that piece of chocolate, didn’t call my Dad, and didn’t finish this project.” Those facts may be true but it totally neglects the great parts of the day that happened. It casts aside the early morning when you got up with the alarm, the solid routine you followed in the morning, the hour you spent reading a book you love, the work you _did_ get finished, the 30 minutes of exercise you put in and the lunch you had with the one you love. When cast in that light, the day was pretty damn good. Was it perfect? Of course not, but who the hell has a perfect day, every day? It’s okay to not do everything you wanted to do. To beat yourself up about it is to give greater weight to the negative.

You’re going to make mistakes and fall short all the time. That’s a reality. To think you can be perfect and do everything every day is to live in fantasy. Reality will never stack up against fantasy. Instead of beating yourself up for not writing for 1 hour, be happy that you wrote for 15 minutes and then try to push yourself to do more the next day. Maybe you didn’t work hard for a full 8 hours yesterday, but you did 5 hours worth of work and that’s something. You know for a fact you can do 5, so maybe you can push it to 6 tomorrow.

The point is, you can focus on the negative or you can focus on the positive while still being aware of the negative. The positives give you solid evidence that you can point to and say “This I can do. Tomorrow, I’ll do better. But I’m proud of myself for doing this.” No matter how small, you have something positive to be proud about.

Focus on the positives and start trusting yourself. Good things will happen.