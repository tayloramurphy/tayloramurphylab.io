---
id: 78
title: 'Reflected glory and why it&#8217;s keeping you from epicness'
date: 2012-03-04T19:08:41+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=78
permalink: /reflected-glory-and-why-its-keeping-you-from-epicness/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Emotions
tags:
  - challenge
  - epic
  - glory
  - reflected
  - sports
---
Professional sports brands have followings that any major corporation would kill to have. The fanaticism of football, basketball, baseball, and hockey fans is truly astounding. Each year, people spend millions of dollars on merchandise, tickets, food, and transportation all in support of competitive events between two businesses.

**Make no mistake, every sports franchise you see is a business.** They may point to the sporting event as their main reason for existing, but these corporations earn massive profits (usually) for the owners and players. There is nothing inherently wrong with this, but it’s something I’ve taken more notice of lately.

Go to any game, be it a NASCAR race, NFL, NBA, or NHL game and you’re bound to see some fans that go all out in their support of the team. They have the jersey and hat. Maybe they painted their face. They can talk to you for hours about the players, coaching staff, the strategy on the field, and who’s going to be the ‘next big thing’. Whenever they talk about their favorite team, they like to use the pronoun ‘we’, as in, we’re going to win the championship this year, or we’re going to pick up so-and-so from this other team. The person includes themselves in the organization.

To the mind of the true fan, they really are a part of the organization and their participation helps the team succeed. They feel the sting of every loss, and they bask in the glory of every win. They fully believe that they are a significant part of this organization and that the clubs’ every move affects them deeply.

_Editors note: I am not creating a straw-man. There truly are people like this. They are in the minority, to be sure, but they exist._

## This links their happiness to something out of their control

It can make people crazy. When individuals consider themselves to be a part of something with which they have no control or influence, it creates sad situations. At best the person feels like they’ve done something worthwhile just by supporting a team either in person or by watching them on television. At worst, [people die](http://www.totalprosports.com/2011/07/11/9-tragic-sports-fan-deaths/). These people place their own self-worth in the hands of a corporate brand and **manage to excuse themselves from doing epic things**.

Why should somebody work hard to do something great? They’re a fan of the World Football Champion, and the reflected glory of that win is good enough for him. Why create your own success when you can buy a t-shirt of somebody else’s? The pernicious effects of branding and merchandising in sports convinces people that just by wearing the logo and saying they support the company that they’re doing something great and worthwhile.

Well I’ve got some news: **you’re not**. Sitting your ass down on the couch and watching somebody compete for a championship does not include you in their success.

## I don&#8217;t oppose organized sports

On the contrary, I enjoy going to football and hockey games and I am supporters of my local teams. I think sporting events are great on any level, from a scrimmage in the backyard to intramural competitions to professional bouts. Sports are a way to learn teamwork, overcome some adversity, and place a challenge upon yourself to overcome. I think sports are great.

But just like anything in life, **too much of a good thing can be bad**. Take your fandom too far, and you start missing out on amazing things that you can do in your own life. Invest too much of your emotional security in something that’s outside your control and you allow somebody else to have power over you. This isn’t a good thing and should be avoided.

Instead of claiming somebody else’s glory for your own, why don’t you create your own instead? **Do something epic with your own life**. Join a team and compete yourself. Place yourself in [challenging situations](http://www.tayloramurphy.com/blog/30-days-with-no-internet-at-home/ "30 Days with No Internet at Home") just for the opportunity to overcome it. Make your own glory.

Remember, every sports team you see is an organization that is trying to make money. The fact that they do competitive sports just makes it more interesting. Every sports logo you see is the brand of that team. Imagine if people were so fanatical about Suntrust or Regions bank. They’re all companies, and they all try to make money. Use them for what they are, and enjoy them when you wish, but never stop doing your own epic thing. **Make your own brand and create your own glory**. Life is more awesome that way.

_Have you ever felt the reflected glory of a sports franchise&#8217;s win? What are you doing to make your life epic?_