---
id: 347
title: Forward. Not Sideways.
date: 2013-01-14T21:31:52+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=347
permalink: /forward-not-sideways/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Future
  - Learning
  - Personal Development
tags:
  - avoidance
  - commitment
  - creating
  - friend
  - grit
  - habit
  - painting
  - story
  - writing
---
Sometimes we do things because it makes us feel like we’re being productive. Perhaps you do something on a regular basis that is related to what you want to do but, if you’re being completely honest with yourself, isn’t getting you to where you want to be.

Imagine a woman who wanted to be a great painter. At the beginning of her journey she decides, rightfully so, that she should improve her skills in some way. She tries a few things on her own, and she starts to improve a bit, but nothing seems to really be sticking like she thinks it should.

Then a friend of hers gives her a very thoughtful gift: a book of painting suggestions. On each page the scene, colors, and even specific techniques are suggested and recommended. Immediately the woman is able to recognize what a wonderful opportunity this gift is. Right then she commits to doing one of the painting suggestions each day.

In her head she is fully committed to it. And for about three months she sticks with the book, barely missing a day. If she continues working through the book she’ll have spent a year working on it. She’s convinced that if she finishes the book she’ll be a much better painter than she was at the beginning.

As time goes by she dutifully works through the book. Some days she’s not able to do the painting but she doubles up the next time and does two paintings. Then she gets busy with work and family and she misses several days in a row. Now she’s five paintings behind and the thing she used to love and look forward to is starting to feel like homework.

She catches up and keeps going strong for a while but then she misses a few more days and falls back into a malaise caused by not doing what she’s supposed to. Seeing this pattern continue she decides to take a break. She won’t work in the book for almost two weeks but commits to still paint during that time.

The quality and quantity of her paintings during this break are incredible. She paints everyday, though not to a finished product every time, and her joy is back. The few paintings she does finish are some of her best work and she feels an immense sense of pride and joy when she looks at them. She decides to give these paintings away to her friends because she wants to share what she’s created.

After her two weeks is over she is curious to continue with the painting project. She recommits to the same goal again: one page every day. For almost a month she diligently works through the book. Then she misses a day. And another. And a few more. Eventually she returns to that feeling of dissatisfaction and guilt and wonders why she just can’t seem to do the work. A few of her friends ask her for more paintings like the ones she made during her break and one even suggests she try and sell it to an art gallery. She says she’s been working on some but she’s been too busy to finish them.

After another day of not painting she wonders if the project was the right thing to commit to. If she stops would it look like she was quitting and was a failure? How could she commit to working through the book and then just stopping?

&#8212;&#8211;

This story is near and dear to my heart. For the past five months or so I’ve been working through a book, given to me by my [very best friend](http://atheistslut.com/), called [642 Things to Write About](http://www.amazon.com/Things-Journal-Francisco-Writers-Grotto/dp/1452105448/ref=sr_1_1?ie=UTF8&qid=1358220499&sr=8-1&keywords=642+things+to+write+about). She knows of my deep desire to become a writer and she gave me the book to encourage me to write more. As a writer herself she knows first-hand that the hardest part is just starting. This book, and the prompts it contained, were meant to be jumping off points to get the juices flowing.

I loved it and turned it into a project. I said I was going to do two writing prompts per day and post them to my [tumblr account](http://needsomerope.tumblr.com/). Similar to the story above I was very good about doing it for about three months and then I started to slip. It’d be a few days in between stories and then I’d find myself with 8 stories to write. Then I took a break and eventually [recommitted](http://tayloramurphy.com/grit-and-commitment/ "Grit and Commitment") to sticking with the project. Now I’m sitting at 12 stories I’m “supposed” to write.

This essay is my admission that something was wrong with this approach and it has to change.

For a while, the project was extremely helpful. I would write the stories I committed to and then I would write what I wanted to. But a pernicious thought pattern started to creep in. I would write the stories and then feel like I had “done my writing” for the day and could do whatever else I wanted. There were some days where I would spend 10 minutes on my stories and then not write anything else. Even though I had plenty to say. Even though I want to be a writer.

Instead of being a help the project had become a hindrance. Those closest to me started to notice my lack of updates on my main blog. When they’d ask me about it I’d say I’ve been busy and hey, at least I’ve been doing my stories. As if doing the stories gave me permission to not write anything else.

It also became apparent that the project was becoming an avoidance technique. Perhaps this is a topic worth diving into in more detail later but I’ve realized I’m rather afraid of failure. For most of my life I haven’t failed at many things. Now I’m seeking a career path that has a high probability of failure and it’s a bit scary. So to avoid thinking about it I start a project that will ostensibly help me but, in truth, isn’t.

Another aspect of this avoidance is the desire that most of have to be told what to do. It can be very difficult to think of what you should do on your own. Much easier to have someone else tell you what to create. After all, what if you pick wrong, screw up, and waste your time? By having this project I didn’t have to think and I wouldn’t have to get in touch with the dark and vulnerable parts of myself where my true greatness and creativity could flourish. But I don’t want to be told what to do. It’s just easier if I am.

As much as it hurts to admit, the 642 Things project is making me busy but not productive. I’m moving sideways and not forwards and I’m not doing and creating the things I want to.

My time would be better spent writing about the myriad topics I want to write about. All the little phrases and openers and ideas I’ve jotted down in my moleskine but haven’t written about because I’m either too scared or believe I’m not good enough.

I’m putting the 642 Things book back in its place: as a tool to help me. I still fully intend on finishing every prompt in the book (and on writing every day) but it’s going to be on a different time table. Instead of doing 2 stories every day, I’m going to do however many stories I want whenever I feel a need to clear the rust and get the juices flowing. Some days that might be one. Others it could be four. Some it could be none. But I’m not going to allow it to become the reason why I don’t write about what I truly want to.

The path to making a living as a writer and creating awesome shit isn’t straight and there will be many distractions on the way.

It’s okay to be distracted as long as you realize it at some point and start moving forward. Not sideways.