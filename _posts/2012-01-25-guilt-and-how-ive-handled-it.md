---
id: 28
title: 'Guilt and How I&#8217;ve Handled It'
date: 2012-01-25T22:10:59+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=28
permalink: /guilt-and-how-ive-handled-it/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Dreams
  - Emotions
tags:
  - action
  - assumptions
  - dream
  - emotion
  - guilt
---
Guilt is an emotion that I’ve become familiar with lately. Particularly the guilt that comes with wanting or desiring something that I feel like I’m not supposed to want or have.

Guilt comes in many forms. Perhaps you feel guilty because you cheated on some work. Maybe you feel guilty because you ate that cookie when you told yourself you weren’t going to. **These are all real emotions that people feel**.

The guilt that I’m experiencing, especially right now, is the guilt for wanting to have a different lifestyle. Graduate school has a way of draining the energy from a person. The long hours, low pay (if any), and seemingly never ending list of things to do can make even the strongest of people wish they were elsewhere or that their life was different.

_These feelings are ok_. In fact, these feelings are perfectly normal and I can guarantee that you’re not the only one feeling them. It’s natural to want something better for yourself. It’s OK to have dreams that have nothing to do with your current situation. We call them dreams because they’re not reality (yet!).

The feeling of guilt comes from wanting something that you believe you are not allowed to have. Your belief structure has been set up in a way that what you want, is not what you think you’re supposed to want. And when you _don’t_ desire what you think you’re supposed to desire, you feel guilt or shame.

The majority of people will probably have these feelings, tell themselves to get with the program, or toughen up, and keep doing what they’re doing. After all, that’s what they’re supposed to be doing, right?

The problem with this way of thinking is that it’s based on false assumptions. There is no correct way to live your life. There isn’t anything that you absolutely have to do with your life. Certain actions are demonstrably better, or worse, for your overall life and so society has chosen to make some of those things required or illegal (i.e. education and drug use, respectively). But outside of some basic necessities, you don’t have to do anything.

You don’t have to spend your days in a job you don’t like. You don’t have to spend several years getting a degree that you only _think_ may help your career. You aren’t required to do something unfun just because you have a natural aptitude for it. **The world will continue to exist if you don’t**.

My current feelings of guilt stem from the fact that the Ph.D. I’m working towards is something that I’m not even sure I want anymore. I feel like I have to finish my degree before I can do anything else. And I feel like I will be compelled to get a ‘good’ job after I graduate because, well, that’s just what you do once you graduate. But I don’t believe it has to be like that. This belief isn’t something that I have fully internalized. It takes constant reminding to truly believe it. I still feel guilty periodically when I have these feelings, but the more I push back against that type of thinking, the better I feel.

So what are we to do with those thoughts and feelings that yearn for something different or better? My choice, so far, has been to fully embrace them. I know I want a different lifestyle than the one I see coming down the road for me. I am 100% committed to finishing my degree, but after that, I am not going to follow the path that has been laid out for me.

**I’ve taken these yearnings and turned them into actionable steps**. Some of these steps have yet to be accomplished, but they are all extremely doable in a timely manner.

**Make my intentions public**

I’ve done that with this blog. I purchased my domain name, installed wordpress, and am writing for the world to see. Whether anybody reads it or not is truly irrelevant. Yes, I would love to connect with people and have my words read, but, especially right now, it makes no difference to me.

**Get explicit about my dreams**

I have yet to fully do this. The challenge here is that there are about a zillion things that I want to do or try. Not all of them can be sources of income or the basis upon which I build my lifestyle. I will eventually do them all, but they can’t all be a priority, especially not right now.

**Practice a marketable skill**

I have firmly decided that writing is either going to make or break what I do. Communication is an absolutely essential part of thriving on this planet. Connections cannot occur if thoughts and ideas aren’t effectively transmitted. I’ve made a commitment to write at least 1000 words per day. Whether it’s for a blog post, a story, a journal, or just stream-of-conciousness, the only way that I can become better is to practice, practice, practice. I have aspirations of guest-posting, writing a book, writing for magazines, and even writing my thesis. Becoming an excellent writer will not happen overnight. Dedicated practice is the path for me.

**Clear out distraction**

This is an ongoing process. I’ve stated before that I am going through a divorce. This has caused me to get rid of most of my belongings and pare things down to the essentials. I am also making difficult decisions about the ways I spend my time. I’ve canceled Netflix and cable. I’m selling my TV and entertainment system. I’m even considering shutting off the internet at my house. This is in an effort to i) clear out distractions and ii) save some money. I still have a ways to go and many things to donate or sell, but I’m well on my way to have a distraction free environment where I can make my dreams a reality.

These are the basic steps that I’ve taken or am planning to take in the near future. I believe them to be worthy steps that will aid me in making a strong headway towards creating the lifestyle that is fulfilling and happy. I’ve recognized that the guilt I was feeling was an improper manifestation of a real desire: to change my life. Only by being completely honest and fully embracing the underlying feelings, can you or I hope to make a positive impact in life.

**Do you feel guilty at work? Is there something that you believe you _have_ to do? Chances are excellent that you don’t. Embrace the underlying desires that are causing you guilt and create actionable steps that will help you achieve them**