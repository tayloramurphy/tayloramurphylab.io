---
id: 424
title: The Caprice of Life
date: 2014-11-08T20:42:24+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=424
permalink: /the-caprice-of-life/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Emotions
---
<p class="p1">
  I watched a dog die recently. It was in the back of Lindsay&#8217;s jeep, lying on it’s right side, head towards the front of the vehicle. I had put it there after finding it on the street crying in pain. She was all black, head to toe. Her pelvis and femur were crushed, I was later told by the vet.
</p>

<p class="p1">
  The puppy, and it was a puppy, no more than 2 months old, lived a short, sad life. The night before it died it slept out in the rain, where the temperature was closer to freezing than comfort. The next morning it chased after a member of its family, the only thing it knew and loved in the world. The person she loved was in this large, blue moving thing. That was all she knew. She didn’t know she should stay close to the house, inside the fence. She chased after the blue thing and got too close. Then the wheel was on top of her and then it wasn’t, but the damage was done.
</p>

<p class="p1">
  My girlfriend saw this happen and yelled out to warn the woman. Her effort was in vain. The woman got out of the car and looked briefly for her pet. She must have known she hit something, right? But it was too dark, she said. So she got back in her car and left for work.
</p>

<p class="p1">
  I was in bed still, checking some inane thing on my phone. Our dogs were going crazy and Lindsay came in hysterical.
</p>

<p class="p1">
  She did what? Just now?
</p>

<p class="p1">
  I put some pants on and grabbed my glasses and flip flops. Lindsay tells me on the porch what happened. I could see the dog from our porch, a black lump in the street illuminated by the single lamppost. It looked dead from where we were.
</p>

<p class="p1">
  But then it lifted it’s head. It was laying on its right side, the same side it would later die on. I ran to her and crouched right by her. She lifted her head again and made the most pitiful noise I had ever heard. What do I do? She cried again. I picked her up as gently as I could but every movement caused her pain. How could it not?
</p>

<p class="p1">
  I remember every second of this. The pitch of her cry. The blood coming out of her nose. The tiny rocks from the street that were stuck in her coat. The way her eyes seemed to be too large and full of fear. Her wet fur.
</p>

<p class="p1">
  We didn’t know what to do. I put her in the back of the jeep and ran inside to grab my coat, keys, and wallet. We decided to take her to the emergency vet hospital. I knew there was no chance she would make it, but what else could we do?
</p>

<p class="p1">
  We were less than a mile from the house before she died. I didn’t hear anything but when we stopped at the light it was clear she was dead. No breathing. Nothing.
</p>

<p class="p1">
  I drove in the dark and rain. We decided Lindsay should still go to work. It seemed the ultimate cruelty that something traumatic like this happens and she still has to work. Such is the life of a nurse. I drop Lindsay off, tell her I love her, and start to drive away while tears stream from my eyes and sobs begin wracking my body.
</p>

<p class="p1">
  I was holding it all in for Lindsay, I realized. Now that I was alone I felt everything all at once. The callousness of the world. Anger at the dumb bitch who ran this innocent creature over. Confusion and disbelief. But most intensely of all: sadness. Profound, heartbreaking sadness.
</p>

<p class="p1">
  The road was blurry from the rain and my tears. I composed myself enough to drive safely to the vet. The dog was dead, there was no point in hurrying.
</p>

<p class="p1">
  It’s a 24 hour emergency vet clinic and there were people there at 6:30 in the morning. I parked the car and opened the lift gate. There she was, lifeless and pathetic. I lifted her as gently as I could. Everything was limp. Her head lolled when I supported her body and her body sagged when I supported her head. I cradled her close to my body while trying to get in the building which had a stupid buzzer lock and I couldn’t get anybody to help me open the door.
</p>

<p class="p1">
  I looked a mess. Unshaved, bedhead, soaked with rain, dirt, and blood. A nurse came out and took her from me. She asked me what happened.
</p>

<p class="p1">
  I don’t know.
</p>

<p class="p1">
  Hit by a car?
</p>

<p class="p1">
  Yeah. I mean I know what happened. I don’t know what’s wrong.
</p>

<p class="p1">
  She left the waiting room and went into the back. I stood in the waiting room for only a minute but it felt like forever. My entire body was shaking. There was blood and rocks on my hands. I felt exhausted.
</p>

<p class="p1">
  The nurse poked her head into the waiting room and asked me to come in the office. I knew the dog was dead but there was still a part of me that hoped I was wrong. I can’t take the pulse of a dog so how could I verify?<span class="Apple-converted-space">  </span>She closes the door and tells me what I already knew. I ask if I can wash my hands. The vet opens the door a bit and matter-of-factly states what the injuries were.
</p>

<p class="p1">
  I ask for some water and sit down. I’m told I did the right thing and that I’m a good Samaritan. I offer to pay for disposal. They waive the fee. She asks if I want to bring the body back to the owners.
</p>

<p class="p1">
  I look her in the eyes and say “They don’t get her back.”
</p>

<p class="p1">
  Eventually I calm down enough to leave and the nurse tells me about the Nashville Animal Control and how people are terrible and that she’s called them on people more times than she can count. That’s why she has 5 cats and 2 dogs.
</p>

<p class="p1">
  Tears come more easily when I’m driving home. The back of the jeep is still dirty with asphalt and blood. My hands are still shaking.
</p>

<p class="p1">
  When I get home I sit on the floor with both of my dogs. They know I’m acting weird but they don’t know why. Dad’s home is all they know. I hug and pet them for a bit and then take a shower. I throw all my clothes in the wash and try to cleanse myself of this trauma.
</p>

<p class="p1">
  Later that day I talked to the woman who ran the puppy over. I had a million things I wanted to say to this lady but when I saw her I just couldn’t. The emotion of the morning came back full force and all I could say was your dog is dead and I took care of it. She made excuses and was everything but remorseful. I went back home and snuggled with my dogs, upset about the whole situation and the fact that I have to live next to these people for the next two years.
</p>

* * *

<p class="p1">
  This whole situation was a lot to deal with. In a moment I was faced with the fragility of life, the carelessness of humans, and the painful suffering of an innocent. That dog didn’t deserve what happened to it. Nothing that small and innocent ever deserves it. One consolation is that it does not have to live with the people that killed her. Her suffering was meted out quick, instead of slow and over the course of a lifetime.
</p>

<p class="p1">
  Every time I park my car on the street, it’s directly in front of where it happened. I can’t help but see it in my mind. The location is the same, only time has passed. I also don’t know how to look at my neighbors. For now I avoid seeing them, hard as that is. There’s too much anger and disbelief.
</p>

<p class="p1">
  This whole thing is just a crazy moment in this world. Animals are killed every day, hundreds, if not thousands, by cars. There was nothing special about this specific moment. It was like a pebble thrown into the ocean. Those of us right by the impact felt it, but the ripples of the moment quickly dissipated. The retelling of the story is one of those ripples, but it too is removed from the moment and, as such, is blunted in its impact. But such is life. This moment has left a scar on Lindsay and me that will never disappear. The wounds heal, of course, but the evidence remains. Already, time is helping. I don’t cry when I look at that spot, and the mindshare it takes up decreases every day, but the memories are still there, and they always will be. Ready to be called up at a moment’s notice.
</p>

<p class="p1">
  I hope the lessons stay too. The lesson to drive slower and pay more attention in neighborhoods. To be a better advocate for those with no voice. The lesson that a lot of people suck in this world and we fight against that by being good and caring. The lesson that crazy things will always happen, and never when you expect them. The lesson that your life can be affected even with the shortest of interactions.
</p>

<p class="p1">
  And the lesson that love, even love for something you barely knew existed, could be stronger than you could have ever imagined.
</p>

<p class="p1">
  I loved that damn dog and I barely knew it. I wish it hadn’t died. But I’m glad it was with Lindsay and me in the end. We loved it the best way we knew how. That has to count for something, right?
</p>