---
id: 390
title: The World Looks Different
date: 2013-07-24T18:44:04+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=390
permalink: /the-world-looks-different/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Learning
  - Personal Development
tags:
  - change
  - frelelance
  - hard work
  - monty oum
  - pushing
  - red vs blue
  - rooster teeth
  - scary
  - world
---
> The world looks different when you&#8217;re pushing yourself every second you&#8217;ve got.

This is a quote that won&#8217;t get out of my head for about two weeks now. I heard it from [Monty Oum](https://twitter.com/montyoum). Don&#8217;t know him? I&#8217;m not surprised.

Monty is one of the lead animators for [Rooster Teeth](http://roosterteeth.com/home.php) productions. Rooster Teeth is well-known (well, in certain circles) for their [Red vs. Blue](http://roosterteeth.com/archive/?sid=rvb&v=more) machinima series and the [Achievement Hunter](http://www.youtube.com/user/letsplay/videos?view=0) brand.

About 4 years ago, Monty came onto the team as an animator. He was well known for his [Haloid](http://www.youtube.com/watch?v=cL-mR79GErU) video and to this day continues to stun people with his animated fighting sequences.

Of course I&#8217;ve never met the dude but his personal philosophy comes across quick once you do a little digging on him. His [deviantart](http://montyoum.deviantart.com/) page is full of animations of his latest creation, [RWBY](http://roosterteeth.com/archive/?sid=rwby&v=more), and a version of the above quote is on there as well.

In short, I&#8217;m a fan of this dude and inspired by his work.

But this quote has been stuck with me for days now. I think it&#8217;s because it states a profound truth that I&#8217;ve learned from personal experience.

Working as a freelancer and trying to &#8220;do your own thing&#8221; is fraught with uncertainty, fear, and worry. When I put my nose to the grindstone and push myself to do the work, much of the intensity in those feelings is diminished or gone. When I&#8217;m lazy and procrastinate those feelings become overwhelming.

By working hard I start to push back against the fear and uncertainty. I make headway in the fight. The way I feel about myself improves. I&#8217;m happier, healthier, and kinder. I&#8217;m proud of myself in a way that&#8217;s not possible by being lazy.

The world looks different when you&#8217;re pushing yourself because you&#8217;re dictating the action. Instead of being passive and dealing with what comes to you, you&#8217;re striking out and whacking through the jungle. Your work and effort become the machete clearing the tangle of chaos.

When you push yourself, and I mean really push yourself, your world changes for the better. You know when you&#8217;re pushing yourself and when you&#8217;re just coasting. That feeling of coasting is what too many people strive for.

You can&#8217;t coast through life. You have to put the pedal to metal and drive hard to get what and where you want. Nothing less than your best is going to change your world for the better.

Push yourself with every second you&#8217;ve got. It&#8217;s worth it.