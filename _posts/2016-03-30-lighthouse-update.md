---
id: 450
title: Lighthouse Update
date: 2016-03-30T22:00:01+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=450
permalink: /lighthouse-update/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Dreams
  - Future
  - Learning
  - Personal Development
---
Over a year ago I made a post about how I don&#8217;t know what I&#8217;m doing professionally. That I didn&#8217;t have a lighthouse to guide me during times that were difficult or when my path was uncertain. This post is an update to that fact. Spoiler alert: I might&#8217;ve found my lighthouse.

When I was a kid there were a few things I was into big time: dinosaurs, space, and computer games. I had all the books about dinosaurs, all the books about the space shuttle, and a bunch of video games that were super awesome at the time (some of them are still awesome!). Basically, I was a typical 90&#8217;s kid nerd.

Then I started growing up and things changed. I went to Space Camp when I was 12, so I was still into space then. When I was 13 or 14 I joined the Civil Air Patrol and went to Aviation Challenge. Soon after that I started taking classes at the local community college while being homeschooled for high school. 9/11 happened. I got a car and a job. Girlfriends, teenage years, more school and a marriage. Even more things and changes happened and it all culminated in me earning my PhD in Chemical and Biomolecular Engineering in 2013 after having gone through a divorce and a complete excision of my religious faith (which is to say I changed a huge portion of my identity).

I had become a not-the-useful-kind of Doctor and was then thrust into the real world of professional life. By the time November 2014 rolled around I felt like I&#8217;d done and seen a lot and finally had a real moment to breathe and reflect on things. That&#8217;s what led to the original lighthouse post.

Now I&#8217;m reflecting on my professional life again and it&#8217;s amazing to see how things have grown and improved since that post.

My role at my company has gone from a Data Scientist to a Director position where I now manage two people on my team. This promotion helped me learn even more about what it means to be a good leader and manager. It has also changed what my day to day is like and I find myself enjoying work more. Lindsay and I became engaged this past summer and are marrying in less than two months. She also started school which has been stressful and challenging in its own right. I&#8217;ve also started seeing my old therapist again (who helped me through my divorce) to get more focused on setting and accomplishing goals in my life. He&#8217;s basically been a personal trainer for my brain to help me, in part,  find that lighthouse.

And I must say, it&#8217;s working. Quite well. In fact, I&#8217;d say that going to therapy has been the best decision I made for my overall personal and professional happiness last year.

I started seeing him in February of 2015. The first thing we did was take stock of how I felt about where my life was in terms of my family, mental, physical, emotional, spiritual, and professional well-being. I thought about what was going well, what wasn&#8217;t going well, and what I wanted to improve. Things were great but I knew they could be better.

So I started taking steps to make them better. I added some daily habits to keep me sane: I now try to read, meditate, and practice gratitude every morning. I exercise more frequently and eat better food. I reach out to family and friends more. I focus more at work. The latest thing I&#8217;ve done is cut out Reddit and YouTube for the next 3 months. I&#8217;m already seeing more positive gains from that.

But this post isn&#8217;t about my habits, it&#8217;s about my lighthouse. So here it is.

Simply put, my lighthouse is to become an astronaut.

Easier said than done, right? Well, of course it is. So what does it mean to decide you want to be an astronaut? How does one&#8217;s day to day change? And, more importantly, why an astronaut?

For me, an astronaut is the perfect embodiment of a number of factors. In my mind an astronaut has a plethora of highly admirable qualities. They are smart, ambitious, scientific, space-lovers, leaders, engineers, technically-capable, brave, adventurous, respectable, and rare. They are the thing every kid wants to be but few actually become when they grow up. Of course, astronauts are human too and full of ego, hubris, jealousy, and the entire spectrum of human flaws. But their vocation strives for more than those weaknesses and literally reaches for the stars.

This desire fully welled within me when NASA&#8217;s latest call for astronauts occurred. I was immediately interested but felt like I was disqualified for any number of reasons. As it turned out, I had accidentally become qualified to be an astronaut. I met all the educational and professional requirements. Lindsay and I had become SCUBA certified too. After quadruple-checking my height, I made the decision that I was going to apply. It was a no-brainer. But more than that, I felt like I&#8217;d been given full permission to fall in love with space again. Prior to that, I&#8217;d felt, for whatever reason, that it wasn&#8217;t really ok.

One of the joys of my current job is that I became friends with a coworker who&#8217;s an even bigger space nerd than me. He&#8217;s the one who I convinced to buy a Tesla and the one I talk to about all the SpaceX launches. So just being around him made me feel like I could let my freak flag fly more. Then a bunch of things happened all around the same time that made me feel even more comfortable being a space freak. The Planetary Society launched their [Kickstarter campaign](https://www.kickstarter.com/projects/theplanetarysociety/lightsail-a-revolutionary-solar-sailing-spacecraft) for the LightSail. I backed it and joined the Planetary Society. The movie The Martian came out. The [National Space Society](https://www.facebook.com/Middle-Tennessee-Space-Society-1457043781189997/) was displaying at Opry Mills for The Martian and I got to talking with them. I wound up going to their next meeting and joining. Then NASA made their call. I applied. Simultaneously I rejoined the [Civil Air Patrol](https://www.facebook.com/Music-City-Composite-Squadron-Civil-Air-Patrol-690499024396941/?fref=ts) to both spend more time with my family and to get involved again with a program that teaches young people about aerospace and leadership. I also joined an organization called [Astronauts 4 Hire](http://www.astronauts4hire.org/) which 100% is on-point with its name. All in all I made several non-trivial decisions to start spending my time on space-related things. And throughout all of that, I had a girlfriend (now fiancee) who was supportive and full of encouragement.

And because I&#8217;m lucky enough to work where I do, I don&#8217;t have to hide any of this from my employer. All of my coworkers know about my application to NASA and have been nothing but supportive. I&#8217;ve made a stronger commitment to building my programming and data-related skills in part because I do believe those skills are valuable when you&#8217;re dealing with space, but also because I&#8217;ve realized just how much I enjoy doing that sort of stuff. So while my day-to-day has nothing to do with space, it&#8217;s helping me build a skill set I want while doing something I&#8217;m proud of. I want to continue improving and building these skills while also fostering other skills outside of work. I want to get my pilot&#8217;s license. I want to contribute to the body of citizen-scientist knowledge through organizations like the Planetary Society and A4H. I want to write and publish more in general but also about space-related topics. My time has become more constrained but for once in my life I&#8217;m really happy with everything I&#8217;m doing. I&#8217;ve chosen to do all these things and I&#8217;m proud of them!

But let&#8217;s get real here for a moment. What are my actual chances of becoming an astronaut? Considering over 18,000 people applied to the last call, I&#8217;d say very slim. It&#8217;s highly, highly unlikely that I will become an astronaut. And I&#8217;m really ok with that. No, really. I am. Here&#8217;s why.

My decision to &#8220;become an astronaut&#8221; has more nuance than it seems. It&#8217;s probable, just based on plain old statistics, that I will not become a NASA astronaut. But that does not prevent me from making decisions that move me towards that goal. And while I may never reach that goal, I will be gaining skills and doing work I&#8217;m proud of. I&#8217;ve been very careful to not tie my happiness or satisfaction with my life to whether or not I become an astronaut. It&#8217;d be silly to intertwine the two so heavily. Will I be a little disappointed if it never happens? Yes. But that&#8217;s part of being an adult. You don&#8217;t get to do everything you want to, especially things that are just plain unlikely to happen. So instead, I&#8217;m tying my happiness to the daily decisions and actions I make. It&#8217;s tied to whether or not I&#8217;m doing the things I say I want to do. It&#8217;s tied to how much love and positivity I put into the world. I could be in space but if I didn&#8217;t have a family who loved and respected me, then it wouldn&#8217;t be worth it. My happiness and self-worth isn&#8217;t tied to a specific outcome, it&#8217;s tied to a process that I can control.

For me, deciding to be an astronaut is more about making decisions that will make it more likely for you to be selected. And there are many skills and attributes that being an astronaut requires. To me, the pursuit of perfection in these skills and attributes alone is enough for a great and worthwhile life. Getting to go to space would be a cherry on top of all that. I think [this comic](http://zenpencils.com/comic/106-chris-hadfield-an-astronauts-advice/) from Zen Pencils about Commander Chris Hadfield and [this article](http://www.forbes.com/sites/startswithabang/2015/12/15/the-astronaut-hopefuls-manifesto-an-insiders-guide-to-nasa-astronaut-selection/#619b091e5e64) from the founder of A4H are a great summary of my thoughts on this.

&nbsp;

All in all, it&#8217;s safe to say that I&#8217;ve found my lighthouse. But as it turned out, upon closer inspection, it wasn&#8217;t a lighthouse.

&nbsp;

&nbsp;

It was a star.

&nbsp;