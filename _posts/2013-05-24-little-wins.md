---
id: 361
title: Little Wins
date: 2013-05-24T12:15:19+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=361
permalink: /little-wins/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Future
  - Learning
  - Personal Development
tags:
  - failure
  - families
  - growth
  - happiness
  - little wins
  - writing
---
One of the many things I’ve learned over the years about happiness is that summarily acknowledging at the end of the day what you’re grateful for can have a net positive effect on your happiness. A simple gratitude journal can do wonders to help put a seemingly negative day in it’s true light.

I’ve enjoyed keeping a gratitude journal periodically but something about it in my mind was off-track. I liked it, but I felt it placed too much focus on things occurring outside of myself. For example, if I say &#8220;I’m grateful for my family, my girlfriend, my dogs, and the food I eat&#8221; then I’m focusing on things outside of myself. That’s not a bad thing at all but it shifts the burden of joy outside of me. Very rarely during this exercise was I grateful for things about me. Occasionally I would say my health, intelligence, luck,  or whatever, but those are very vague and not useful.

Instead, I’ve been shifting my mind to focus on my little wins. What were the things I did during the day that made me happier or got me closer to my goals? This has the positive effect of shifting the search internally so that your mood is less influenced by extrinsic factors.

Yesterday, my day started great but ended poorly. I awoke with my alarm and ate a good breakfast. I read for a while and then wrote about 2000 words. My Mom was in the hospital so I went to my folks house to spend time with my sisters and grandparents while waiting on word from my Dad (she’s fine and the surgery went great). Later I picked up their CSA, spent time with Lindsay and the dogs, drove to the hospital to see my Mom, and finally came home.

That’s when things started to go downhill. I started to become really anxious about the future and my desire to become a writer. Glancing through jobs on E-Lance and seeing the scope of the jobs as well as the competition made me doubt my aspirations and ability. I finally distracted myself from these difficult feelings by bingeing on Internet content (read: YouTube and Reddit) and snacking. Finally, I went to bed late, completely exhausted by the effort of distracting myself. I was kicking myself for having failed so bad at the day.

On review, yesterday wasn’t a great day. But that’s not wholly true. Parts of the day weren&#8217;t great. Overal all it was actually a good day. If I scored each hour by it’s level of goodness I would be on the net positive side. I was placing an undue amount of weight on the negative events of yesterday. There were many small wins throughout the day that I can be proud of.

  * <span style="font-size: 13px; line-height: 19px;">I wrote a good amount yesterday.</span>
  * <span style="font-size: 13px; line-height: 19px;">I started my day right with good food, coffee, and reading. </span>
  * <span style="font-size: 13px; line-height: 19px;">I spent some wonderful time with my grandparents and sisters playing Mario Kart, Catan, and the stock market (with my Granddad) as well as sharing a meal with them. </span>
  * <span style="font-size: 13px; line-height: 19px;">I helped my parents by picking up their CSA.</span>
  * <span style="font-size: 13px; line-height: 19px;">I took the time to visit my mother in the hospital. This is a win for me because even though I couldn’t do much to help anything (she was in pain and feeling nauseous) I know how much it would mean to me if they visited me were I in her shoes.</span>
  * I went on a wonderful walk with my girlfriend and dogs.
  * <span style="font-size: 13px; line-height: 19px;">I laughed a lot during the day, even some during my internet binge, and truly became aware of how much less I deeply laugh since I was a child and in college. </span>

There was some other little wins throughout the day, but the ones I listed are what I want the takeaway from yesterday to be. The takeaways aren’t the late night or the worried feelings, though I believe those can be good to motivate some action. Instead, by focusing on the positive, I can build upon what I had yesterday and try to improve my day and the number of wins to beat yesterday.

It’s so easy to get caught up in the fiction of what other people are doing. We see successful people and imagine they must be “on” for 60+ hours at a time without any breaks and moments of weakness. This just isn’t true, however. We fall prey to the [survivorship bias](http://youarenotsosmart.com/2013/05/23/survivorship-bias/) whereby we only see the successes or the positive things. After all, who wants to share how they [failed](http://sebastianmarshall.com/internal-scorecard-1) and sucked?

It’s important to focus on the reality of the previous day, week, month, year and see how reality as it is. I tend to be more focused on the negative so I am painfully aware of all the wrong things I’m doing poorly and how I want them to be different. To compensate this I need to force myself to be aware of the positive. Perhaps you’re already great about focusing on the positive and have a tendency to gloss over where things went wrong or you’re failing. Maybe a healthy does of what’s been negative in your life might help spur you to greater heights. I’m not sure. I’ll let you know what effect increased awareness of the negative has if I start to focus too much on the positive.

In the short term, I’m going to be more aware of the little wins I have throughout the day, that is, the things I did to move closer to my dreams and live a happier life.

Here’s to more little wins in all of our lives.