---
id: 371
title: The Beginning of The End
date: 2013-06-18T18:11:39+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=371
permalink: /the-beginning-of-the-end/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Dreams
  - Future
  - Learning
---
In August of last year some of my closest friends held a party. “The Beginning of the End” it was called and it was the perfect description for a momentous occasion.

It was the start of the last year for both the two hosts as roommates and for many of us currently in graduate school. The party was great, as they tend to be, filled with smiles, conversation, laughter, and dancing. At one point, one of the two roommates gave a short speech (a habit of his I always looked forward to) and then asked each of us to pick one of the many pictures on their walls. On the back of the picture we were to attach a post-it note and write down a dream or goal we had for the coming year.

I picked a picture of me and the other roommate from our freshman year of grad school. We were headed to a football game; one of her first games in America. On my head was a ridiculous fluffy wig which was matched only by the ridiculousness of my goatee. We both had huge smiles.

Almost one year later and this picture was handed to me at the last party to be held in that apartment. The next week they would be moving out of the place they’d inhabited for four years. She was off to get married in Colombia, he moved in with someone else to finish his PhD. Then he’d be off back home in Belgium.

She was crying right before she handed back the pictures. A flood of wonderful memories drawn out by the pictures. The passing out of each picture was unceremonious but very momentous. It was a gift of the memory and good times. An acknowledgement that all good things do come to an end and a reminder that it’s okay to look back and remember the times we all spent together.

When I had the picture in my hand I couldn’t remember what I’d written on the back. 10 months seems like a lifetime ago and a blink of the eye. I flipped the photo over to see my chicken scratch:

“Earn location-independent income and travel the world.” Simple, pithy, and a seemingly out of reach desire, especially at that time. My dissertation and the following graduation seemed so far off. There was still so much work to do when I wrote that. I was on another road and even then I had my eye on taking an exit and going to another road.

It made me smile to see what I wrote. It’s like I went back in time and my past self said “Hey, glad to see you’re still working on the goals we wanted. Keep it up.” I showed the picture and post-it to my girlfriend and she smiled and I smiled because we both knew one of the reasons she fell in love with me was my penchant for this sort of dream.

I smiled because I knew she fully supports me, even if it means we’re physically separated while I’m doing the second part of that dream.

The old me, who had no idea what the road would look like, how things would twist and turn, how they&#8217;d end up different than I had hoped but still pointing in the same direction. The injuries and surgeries and debt and love and canines and moves and tears and all the things that were to come in the next few months. Now the old me is the current me and I’m facing the same unknown as the old me. The past is clear and the future is unknown but I’m still working to figure out my own path. To hack down the bushes and tress and foliage in my way to create my own path. It’s a hard path and it’s less traveled but it also excites and scares me.

A week ago I was offered the opportunity to do academic editing for a large company. It’s not exactly freelance work but it gives me a step up. It’s not the end all be all of what I’m going to do, but it’s another twist in the road on this journey of life. It’s an intentional twist which, often times, is the best kind.

This picture, a seemingly small gesture, holds so much information and guidance. The impetus to take more pictures and create more opportunities for remembrance. The suggestion to make big goals and check on them periodically. The challenge to create more memories with friends, even when we’re busy. The message that we’re all a work in progress and we’re all in this together.