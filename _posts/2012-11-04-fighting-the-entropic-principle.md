---
id: 236
title: Fighting the Entropic Principle
date: 2012-11-04T20:20:35+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=236
permalink: /fighting-the-entropic-principle/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Learning
  - Personal Development
tags:
  - connections
  - emotions
  - entropy
  - growing
  - health
  - humans
  - learning
  - personal development
---
The more I delve into the world of personal development and growth, the more I see the same message repeated in varied ways. Each person has their own spin on the topic, but generally every suggestion, action, or belief can be whittled down to a shared fundament and it’s usually something we already know but have trouble actualizing.

For a while I was irritated by this repetition. Look at all these charlatans stealing money from people who don’t know any better, I would say. My thinking was if the folks shelling out their cash had bothered to look elsewhere they would have found what they wanted and it probably would have been free.

As I reflected more on this topic, I came to a conclusion that was a complete 180 from where I started. I believe it’s good that so many people are preaching the same message; it helps fight individual entropy.

In an isolated system, the energy that’s available to do work will increase. While this is usually discussed in the context of specific systems (chemical reactions, engines, etc.) I believe it has some applicability to we humans as individuals.

When you are isolated from the world around you, physically, mentally, and emotionally, your energy available to create and grow starts to decrease. Sure, for a while you can sustain yourself purely on internal mechanizations, but at some point you will need input from external sources. Food is an input. As is air. These are obvious. What’s less obvious is the need for agents outside yourself to help sustain your mental growth.

When we isolate ourselves from new ideas, people, and experiences, we start to decay in our understanding of the world. Our information is outdated and the available energy for growth and creation decreases; entropy increases. Ideas, even if they’re old ones, presented in new forms help decrease the entropy and sustain our minds and hearts.

Writers, speakers, bloggers, and vloggers are all people sharing a similar message with their own unique twist. I may disagree with some of their methods, but they’re all creating a positive effect on the world. They are the external force which helps others decrease their own entropy and gain the energy they need to improve their lives.

I write and speak so that others may draw upon the force of my words and ideas to decrease their own entropy and grow in some small way. I read and listen to others so that I can draw upon the force of their words and ideas to decrease my own entropy and grow.

By ourselves we can do very little and not for very long. It’s the connections with others that helps us to learn and grow and push back against our own entropic gains.