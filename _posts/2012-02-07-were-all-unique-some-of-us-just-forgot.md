---
id: 49
title: 'We&#8217;re All Unique, Some of Us Just Forgot'
date: 2012-02-07T23:12:32+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=49
permalink: /were-all-unique-some-of-us-just-forgot/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Future
tags:
  - atheism
  - atheist
  - blogging
  - change
  - creativity
  - entrepeneur
  - growth
  - learning
  - logic
  - minimalism
  - passion
  - science
  - writing
---
For those of you who are not aware, I’m what you might call a legitimate scientist. My daily work involves studying cancer metabolism but my training is in chemical engineering. I say this not to toot my own horn, but rather to give you some perspective on my mentality. My mindset comes from a very scientific and logical background.

With this in mind, I find it very interesting (some might say _uncommon)_ some of the things I’ve done lately. In the past I’ve been a very by-the-book, straightforward, risk-averse type individual. I had no time for very frivolous things (like _art_) and I especially didn’t have time for things that seemed to have no point at all.

Recently, though, I’ve had a significant change of heart. My old ways of thinking have died and along with them have gone old prejudices.

## **The Transition Period**

I’m in the middle of this vast transition period. I’ve gone from a good ‘ol Catholic, Republican, Carnivorous, Gun-toting Southern boy, to quite the opposite. My old self would probably look at me and think what a fucking hippy.

In the past year I’ve had many of my assumptions tested and found wanting. I’ve grown and changed and I know, without a doubt, that I’m a much better person because of these changes. There will always be growth in my life (I’m wholly committed to that now) but I can look back even 6 months ago and see how I’ve improved myself from then.

I’ve been in difficult situations and I’ve matured because of them. My current actions are due to may of the changes that I’ve been through. I wanted to make a list of something the new things I’m doing. I do this to help both me and you, the reader, understand where I’m going and what I’m trying to do.

**I’m learning/trying:**

&#8211; **Blogging**: I had a blog for a while last year about how to be a better grad student. It was a good learning experience. This blog is more personal in nature, but I think I will have a greater opportunity to connect with people through the stories I share and learning experiences that I put myself through.

&#8211; **Minimalism**: I’d been fascinated by this concept ever since I started reading [Zen Habits](http://zenhabits.net/), and [The Minimalists](http://www.theminimalists.com/) blog has encouraged me even more. Due the ending of my marriage, minimalism has been thrust upon me. Instead of resisting it, I’m fully embracing it.

&#8211; **Entrepreneurialism**: I don’t want to enter the rat race when I graduate. I want to live a fulfilled life where my work is in deep alignment with my needs _and_ with what I can offer to the world. I’m in Jonathan Mead’s Trailblazer course right now (which is something else I wouldn’t have done in the past&#8230; but more of that in a future post) and I’m trying to learn how to design the lifestyle that I want. I do want to earn money from my blog at some point, but I have to earn that right.

&#8211; **Atheism**: I am an agnostic atheist and I’m proud to say it. This was probably the biggest change that occurred in the past year, but I know that I am a much better person because of it. Being a free-thinker and humanist has changed my worldview so much, and I have to say, it’s awesome.

&#8211; **Passion** **/ Creativity**: I am absolutely _fascinated_ by the creative types in this world. I look at designers, artists, authors, musicians, dancers, filmographers, and anybody else who does something creative and I am in awe. My mind does not really work like that. I’m very logical and my artistic creativity has been quashed by almost 10 years of engineering. I still have some scientific creativity, but it’s nothing like I see around me. The amazing things that I see other people do are something that I _desperately_ want to be a part of. I’m crazy about it right now.

## **Where To From Here?**

That is pretty much where I am right now. I’m a mixed up back of interests, experiences, and emotion. But, as I’m learning, _so is everybody else_. Nobody really has it figured out, and everybody is just trying to do the best they can.

And that’s what I’m trying to do here too. Everyday I’m committed to doing something that is going to help me grow as a person and expand my world. Currently, it’s writing 1000 words per day (There&#8217;s that creativity coming in). Next month it might be something else. I have some cool ideas cooking in my brain (and recorded in Evernote otherwise I would forget) and I hope to share them with you as I try them. I want this to be a learning process for everybody involved.

Oh, and one last thing. I give myself permission to change any of these things at any time. Nothing in life is set in stone and I can’t guarantee that all of these things about me will stay the same. But what I can guarantee is that I’ll share my story and maybe bring you along with me.

_If you have any questions or just want to talk about something I’ve written (or anything else really) please feel free to email me any time at_ _engineeringtheuncommon at gmail.com_. _I would love to hear from you!_