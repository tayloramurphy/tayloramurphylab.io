---
id: 99
title: Why I Really Dig Traveling Now, Even for a Funeral
date: 2012-03-13T21:43:03+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=99
permalink: /why-i-really-dig-traveling-now-even-for-a-funeral/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Comfort Zone
tags:
  - comfort
  - conversation
  - death
  - florida
  - flying
  - funeral
  - minimalism
  - travel
  - zone
---
I’m in Florida this week attending the funeral of my great-grandmother. I didn’t know her that well and, if I’m being honest, I came more to see the rest of my family than for the funeral. As bad as it sounds, the funeral is just a good excuse.

I’ve really begun to love traveling. It used to fill me with a certain dread. The idea of having to pack all of the things I might need and schlepp that around the airport. Going through security and feeling panicked because I wasn’t sure if somebody was going to run off with my laptop and I’d have to chase them in my stocking feet with my pants around my ankles. The uncomfortableness of sitting next to somebody on the plane who might (God, no!) start a conversation with me.

In my ideal world, I would’ve transported myself from one safe haven (home) to another safe haven (relatives). That was then. But now, I’m stoked at the chance to have these experiences and make the most of them.

With minimalism, I’ve been able to pare down everything I _might_ need to everything I _do_ need. If I’m missing something, well I can pick it up later. TheMinimalists [20/20](http://www.theminimalists.com/in-case/) rule works well. Now I’m able to go with just two small bags which makes me lighter and more mobile.

Security is easier too. With less stuff, I don’t have as much to track and, with proper planning, I can breeze through with minimal stress.

When I take my seat on the plane, I get excited because I can’t wait to find out the story of the person that’s going to sit next to me. Maybe they’re a world-traveler making a short US stop. Perhaps an executive at a well known company. Maybe a student on their way to visit some family. Who knows? But I do know that, just like me, they’re a person who’s had their own share of experiences in life and that, by default, makes them unique and interesting. (What’s up for debate is how long that interest lasts&#8230;)

Each new experience that comes our way in life can be seen as something unwanted and disruptive. Or it can be seen as an opportunity to learn and grow by experiencing life outside our small comfort zone. Great things happen when we step off our well-worn paths and into something unknown.

Nobody wants to hear the story of how you stayed home and did the familiar. The uncommon is what’s interesting. Those are the stories that are shared for a long time.

_Fun fact: I wanted to work the word obsequy in here because I think it&#8217;s great, but it just didn&#8217;t flow. I suppose I could have said I&#8217;m attending the obsequy of my great grandmother&#8230; but who says that? If I were attending multiple funerals I would&#8217;ve felt more comfortable using the plural: obsequies. But then I&#8217;d be very sad because, really, who wants to attend many of these things?_