---
title: Thoughts on Identity
date: 2020-05-06T15:21:00+00:00
author: Taylor Murphy
layout: post
permalink: /thoughts-on-identity/
---

I'm currently reading through James Clear's book "Atomic Habits". It's been an engaging read so far and I've underlined more sections than I was expecting to.

One part that stood out to me was centered on identity. It's the idea that to change your habits you need to change your identity - become the type of person who would do, or not do, the action in mind. 

I've heard this advice before and it's not particularly surprising, but it stuck with me for some reason. Perhaps because we're quarantined still in this pandemic and questioning your whole identity is a good way to pass the time. 

But I digress.

One bad habit I've been wanting to break for a long time is biting my nails and cuticles. I don't remember when I picked this up, but I know I've been doing it for a long time. My nails have never been a source of pride for me, quite the opposite. There are times I'll be watching a video by some YouTuber who's hands are full frame in a video and I think, "Oh God, I could never have a camera that zoomed in on my fingers, I'd be so embarrassed." The end result of this habit isn't pleasing to me at all. It's so bad that I judge *other* people when I see that their nails and cuticles are just as bad as mine. 

In his book, James shared a specific story of somebody who stopped biting their nails (page 33), but sadly the quote is "Through mindful willpower alone, I managed to do it". I've even gone the manicure route to try and add some motivation, but that didn't do the trick either. I'm hopeful I'll come up with additional ideas as I continue to read the book.

One thing I will be trying though is continually reinforcing the identity of "not a nail biter". I aim to eventually get to the identity of "person proud of his nails".  I've been working on it the past few days, trying to gently repeat it and remind myself throughout the day. It's a challenging habit to break because I'm not able to remove my hands to dispel the temptation, and it's not obvious at all to me what other habit I could replace it with. I suspect that it really is something I'll have to destroy through shear willpower alone. 

Watching my son start to bite his own nails is a motivator, though. I keep thinking about that poem "A Careful Man I Want to Be" and the line:

> I cannot once escape his eyes.
Whatever he sees me do he tries.

At nearly 2, he copies basically everything. He is an excellent motivator, usually, for forming and maintaining good habits. Though this pandemic has everybody watching more TV than we should...

I suspect as time goes on I'll be thinking a lot about identity. I'm moved by the argument that your habits shape your identity, and vice versa. I hold in my head many identities that I *think* I am, but I think the evidence tends to point the other way at times. 

Starting small, starting with nails, isn't a bad place to begin to change for the better.
