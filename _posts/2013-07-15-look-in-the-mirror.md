---
id: 385
title: Look in the Mirror
date: 2013-07-15T21:35:22+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=385
permalink: /look-in-the-mirror/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Future
---
When was the last time you looked in the mirror? Not the one you stare in when brushing your teeth. The one that a friend holds up to reveal the ugly parts of your life. The one that shows you&#8217;re not the man or woman you want to be. The one that throws back in your face all your terrible excuses and extreme laziness. That&#8217;s the mirror you need every day.

Who cares if your hair isn&#8217;t perfect or there&#8217;s something in your teeth. That stuff won&#8217;t ruin your life. The other stuff, the laziness, lack of direction, and general ennui, is what will ruin your life.

Take a good look in the mirror that reflects your life. Is what you see what you want?

Excerpted quote of Johann Wolfgang Von Goethe taken from Robert Greene&#8217;s [Mastery](http://www.amazon.com/Mastery-Robert-Greene/dp/0670024961/ref=sr_1_1?ie=UTF8&qid=1373942457&sr=8-1&keywords=mastery):

&#8220;The misery that oppresses you lies not in your profession but in yourself! What man in the world would not find his situation intolerable if he chooses a craft, an art, indeed any form of life, without experiencing an inner calling? Whoever is born with a talent, or to a talent, must surely find in that the most pleasing of occupations! Everything on this earth has its difficult sides! Only some inner drive &#8211; pleasure, love &#8211; can help us overcome obstacles, prepare a path, and lift us out of the narrow circle in which others tread out their anguished, miserable existences!&#8221;

&nbsp;