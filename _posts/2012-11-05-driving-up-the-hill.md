---
id: 239
title: Driving Up the Hill
date: 2012-11-05T18:41:14+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=239
permalink: /driving-up-the-hill/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Comfort Zone
  - Future
  - Learning
  - Personal Development
tags:
  - challenge
  - difficult
  - growth
  - hard
  - ikigai
  - personal development
  - sebastian marshall
---
A good friend of mine recently brought to my attention how hard I can be on myself. “It seems like you’re suffering from Catholic guilt, Taylor” she said to me. When I pushed her further to explain what she meant by Catholic guilt, basically she meant that Catholics (of which we were both former members) can be exceptionally hard on themselves. They have to go to confession, avoid sins of the flesh, be wary of thoughtcrime, and grovel for forgiveness when appropriate. Evidently the degree to which I’d been lambasting myself for my seeming failures was near the level of many Catholics. She said this sort of tongue-in-cheek, but I took it for greater reflection because there is a nugget of truth to it.

I know I can be very hard on myself. Somewhere in my mind is some sort of cognitive dissonance where I can say that I know no one can be perfect, yet I judge myself against the standard of perfection. There are times when I feel like I haven’t used my time wisely I can be overly critical to the point of near depression. It’s not entirely healthy and I do realize that.

But there is some good reason to why I am hard on myself. Within the past few months I read a book, [_Ikigai_](http://www.amazon.com/gp/product/B006M9T8NI/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B006M9T8NI&linkCode=as2&tag=sebastianmcom-20I), by [Sebastian Marshall](http://sebastianmarshall.com/). The dude is quite amazing and he’s done some inspiring work. One post he wrote really stuck with me and continues to influence my actions today.

The premise is simple: compare yourself not to your peers and those in your immediate sphere, compare yourself to the greatest people in history. The great thinkers, scientists, politicians, and athletes. Set their standards as your new goal posts and you’re guaranteed to go much further in life.

I thought about this for a while and I just couldn’t let it go. When you look at your friends and the people you spend most of your time with, you can probably see it’s pretty easy to rise to the top of the ranks in terms of being and doing the best. Simply giving a damn and trying a bit is enough to catapult you forward. When you can look around at everyone around you and see that you’re doing pretty well for yourself, it’s easy to rest on your laurels and not push yourself very far. After all, you’re doing better than nearly everyone you know, so why go much further?

That’s when you need to move the goal posts. You _know_, deep down inside, you’re capable of much more and that people with worse circumstances than yours have done more with less. By comparing yourself to these great men and women you go from the top of the stack back to the bottom. And that’s a good thing.

It’s good to be at the bottom of the pile. It’s cliche, but when you’ve hit rock bottom the only way to go is up. When you truly believe that you have a near infinite amount of growth above you then it becomes easy to continue growing because that’s the only way for you to go. You realize that all the things you’ve done to try and better yourself pale in comparison to what you could be doing. This fact shouldn’t depress you. It should energize you and get you pumped. Think of how awesome you can be! Think of how you’ll look back on the time you though you were so great and realize that you’re much greater than you were then and how you can still be even greater!

I have enough self-awareness to look at myself and realize I’m doing some pretty awesome things. But I know I can do better. I’m hard on myself because I have my goals set very high and I know I’m capable of achieving my goals and becoming a great man. Literally, the only thing that is standing in my way is me. I’m the only thing holding me back. Similarly, I’m the only one that can truly push me forward.

Imagine your growth as a car on a hill. You’re behind the car with your back to the bumper, straining as your legs push against the earth and drive the energy into moving the car. You’re alone back there pushing. Your friends and family can’t get behind the car and push with you, they can only help steer it every once in a while and keep you on track. The hard work, the blood, sweat, and tears that come from doing something extremely challenging are yours alone. Those closest to you can wipe your brow, give you something to eat, or hug when you it’s rough, but at the end of the day, you’re all alone back there pushing hard to roll your car up the hill.

No matter what you think, that’s going to be true for anything you do. It may be sobering to realize that you alone are in control of your success or failure. You can sit at the bottom of the hill leaning against your car checking Facebook on your phone. Or you can put that shit away and start doing the work to make a difference in your life.

What’s it going to be?