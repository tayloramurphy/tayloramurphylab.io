---
id: 249
title: Thoughts on Marriage
date: 2012-12-03T21:42:55+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=249
permalink: /thoughts-on-marriage/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Future
  - Relationships
tags:
  - divorce
  - future
  - history
  - love
  - marriage
  - trust
---
One of the things about getting a divorce is that I’ve spent a lot of time reassessing the motivations for why I got married and my current beliefs about marriage. As evidenced by the arguably short amount of time I was married there were several problems with my marriage. But perhaps it’s final trajectory could have been predicted based on the true motivations for its genesis.

Back in 2008 I was super Catholic. I did everything right, I knew all the moves, and I said “peace be with you” to the people nobody really wanted to say peace to. Every Sunday, especially in college, I could be counted on to be playing guitar and helping set up and teardown for Mass. At the Catholic Student Center on campus I was very friendly and, presumably, well-liked. It really was a great time and I can look back on different moments without irony or shame and enjoy the true feelings I had back then.

I met my future ex-wife there and we started dating just before I graduated. Within two months of dating our relationship turned into a long-distance one because of my work co-op situation and then graduate school. Two years later we were married and living in Nashville together. Less than two years after that we were divorced.

So what went wrong?

Part of me feels like that’s not the best question to ask. To me it’s like looking at the situation in reverse. I feel it’d be better to go the other way: start more towards the beginning and look for why we made the decisions we did to see what could have been done better or what might have set us on the wrong path.

One of the challenges of long-distance relationships is dealing with the roller-coaster of emotions. My ex and I saw each other most every weekend with rarely longer than two weeks passing by our visits. Each weekend was quite awesome. We would always be doing something cool and because we hadn’t seen each other in such a long time the elation and that realtionship-high lasted the whole time. When either of us would leave on Sunday night or Monday morning it was very sad and quite a let down from the awesome time we had on the weekend. Over the course of two years this trained us to expect our time together to always be awesome. We were rarely bored around each other and we were never around each other very long. That was the first problem.

The second problem was our Catholic faith. We, and most of our friends and family, believed we were doing the right thing. We were two Catholic kids who loved each other, wanted to get married, were rather pure in their sexuality, and very devoted to the faith; it seemed like the perfect plan. And to us it was. We were following the proper steps to have a happy life together, we just had to get married for it to start. Especially after I proposed to her we became more and more anxious to get married. There were so many things we wanted to do that we couldn’t because we weren&#8217;t married. Sex is the obvious reason. It was also the little things we wanted to do as well: go on trips and get a hotel without feeling guilty, spend the night with each other without feeling guilty, live together, make plans together, get to see each other more often. In our world, we had the belief that for any of these things to happen we had to be married. To get B you need A. It all seemed so obvious and made sense. So our desire to get married was born out of the strong need to be able, to be given permission really, to do these things. It wasn’t because we were truly ready to make this commitment; we had to do this to get what we wanted.

The third major problem, as I see it, is that neither of us really knew who we were as individuals. Personally, I didn’t fully know who I was as a person and I never really knew who my ex was. Not really. Sure, we shared our secrets and our passions and interests, but we knew nothing about many of the little things that make or break a relationship. We didn’t know about how each of us liked to spend our time when bored. We didn’t really know how we handled stressful situations. We didn’t know each other’s sexual needs and preferences. It was only after we were married that we had those, “Ohhh, so that’s what you’re like” moments. Again, since we only saw each other on the weekends what was presented to each other was our best foot. It was our most fun, exciting, and attractive side.

And of course I didn’t really know who I was either. For my entire life I had done things to please other people. My Catholic faith was shoved on me as a young boy. My choice to pursue Chemical Engineering wasn’t wholly mine (I originally wanted to do Chemistry but was strongly encouraged to do Chemical Engineering because the jobs and pay were better.) Even the choice to get married wasn’t mine (or my ex’s): it was just a necessary step to get what we really wanted. Once I started maturing and questioning these things and taking responsibility for my life is when I started to fully come into who I was more. I found I didn’t believe in the faith of my childhood, I took responsibility for my happiness, I stopped spending all my energy making other people happy at the expense of myself. In short, I changed. Big time. The man who I was at the end of my marriage wasn’t the boy I was at the start. Taylor version four point oh was not the same as version two point oh.

In combination, all of these factors led to the end of my marriage. Could we have stayed together? Maybe? I don’t know. It didn’t make sense then (or now) because I knew how unhappy we were and there was zero progress being made on rectifying the problems. It basically came down to the fact that I needed to change back to who I used to be to make it work and I wasn’t willing to do that because I was a much better and happier man. So we made the decision to get a divorce and right after Thanksgiving 2011 I moved out.

My happiness and well-being decreased for a while beforethings started looking back up. Now, I’m a much happier, healthier and more mature person that I was a year ago. I think we made the right decision and I think my life (I can’t really speak for hers) is better as a result.

All of this begs the question: will I get married again in the future? My honest answer, right now, is probably. But it’ll be for different reasons than the first time.

Marriage, I’ve come to believe, at its fundament is truly a legal contract. What happens when you get married? Two people come together and make a public profession of their love for each other. They make a promise of life-long fidelity and care in front of their closest family and friends. After the ceremony, they sign some papers which make them officially married in the eyes of the State. Of course, the “true” marriage to many people is the magical words said by the priest or celebrant which brings down the blessing of their particular God and creates a lifelong covenant. The extra paperwork afterwards is just the government horning in on people’s lives.

Now the couple is married and they can start living together, sleeping together, and doing all the things they couldn’t do previously.

But let’s take a step back: these people just signed a contract. They both agreed that they will not be intimate with anyone else so long as the other is living. In the eyes of the state they’ve essentially bet half their stuff (and hundreds or thousands of dollars) that neither is going to change over the course of the next 50 years of so of their lives. In the eyes of their God they’ve bet that neither is going to change and, as such, they’ll have less of a chance of going to whatever Hell they’re afraid of.

Many people see marriage as a guarantee. I promise to be faithful if you’ll promise to be faithful and all will be well. The problem, however, is that it’s not a guarantee. Nothing is truly guaranteed by the marriage. Just because someone says they’re going to be faithful and take care of you and love you forever doesn’t mean they will. Getting married creates an illusion that you’ve removed the possibility of the relationship ending. It’s a promise of fidelity without a true guarantee (and the kicker is there is no 100% true guarantee: it’s all a risk.) People change, circumstances change, and feelings change. Relationships end and, if you look at the statistics, more than half of all marriages turn into divorces.

So what am I saying here? That marriage is pointless? Definitely not. I don’t subscribe to a nihilistic view of the world and I’m not here to tell you not to get married. What I am trying to do is understand my own views as well as challenge some of the traditional aspects of marriage.

I believe that the way most people go about marriage is wrong these days. Marriage, in my mind, should probably be one of the last things that a couple does in their relationship. Instead of signing a restrictive contract towards the beginning of the relationship, it should be a very natural extension of the high level of trust and love that’s been developed over time. Love and trust don’t magically appear when two people get married.

Another big question is how necessary is marriage? I still say it is somewhat necessary, but it’s not a one-size fits all lifestyle. Marriage can be a very positive thing for many people and it should be one of the options on the table for couples. We humans are naturally social creatures and the stability and comfort marriage brings can be very good. But only if it’s chosen as part of a mature and conscious life. Marriage won’t solve your problems or make you happy. Those things happen despite the marriage, not because of it.

Will I get married again? Probably. But it will be after living with, having sex with, and growing close to the person for a long time. Hell, we might even have kids before we’re officially married. At some point you realize you don’t need a piece of paper or a contract to realize the special bond you have. Marriage in the future will probably be simply a celebration of the special connection I have with the woman I love. It won’t be something that gives me permission to do what I want.