---
id: 30
title: Obligated to Work
date: 2012-01-28T20:16:57+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=30
permalink: /obligated-to-work/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Emotions
  - Future
tags:
  - choice
  - control
  - decision
  - future
  - graduate
  - guilt
  - obligation
---
In my previous post I discussed the emotion guilt and how it can stem fromwanting something that you think you’re not supposed to. I wanted to continue with this thread but with a slightly different approach.

Imagine there’s woman who was born to a family of doctors. Her dad is an oncologist while here mom is a neurologist. Everybody knows that she is going to be a great doctor one day.

As she grows up, she starts to show excellent aptitude for math and science. She graduates high school early, summa cum laude of course, and heads off to a prestigious Ivy League School. After earning her bachelor’s degree in Biomedical Engineering, she goes to graduate school to work on her M.D./Ph.D. This is 8 years of intense study and she does excellently. She exceeds everyone’s expectations and graduates at the top of her class.

**What should she do from here?**

The obvious answer is to do her residency and then become a full-time staff doctor at a great hospital. After all, she spent her entire life gaining the expertise required to become a medical doctor.

But what if she didn’t do this? What if, instead of doing what she was _supposed_ to do, she decided that this really wasn’t for her and she began to travel the world?

Perhaps she never wanted to truly become a doctor. It is entirely possible that she was great at math and science, but truly had no interest in it. She succeeded because she worked hard, but she really didn’t love what she was doing. She never said anything about this to anyone and only when she had gone as far as she could go in school did she speak up about it.

Her parents, of course, would be severely disappointed if she ‘quit’ now. It would be such a waste after all. But would it really? **She is the only person that should be in control of her life**. Does she have an obligation to become a doctor and utilize her expertise even if she doesn’t want that life path?

I realize that this is an extreme example. I would hope that everybody who is a doctor is doing it because they love the work and they are fulfilled by helping people in this manner. The reason I bring up this scenario is because I am in a very similar situation.

I have spent years gaining the expertise to become a Chemical Engineer. I earned my associate’s degree in Pre-Engineering (whatever that is), my Bachelor’s in Chemical Engineering, and I’m nearly done with my Ph.D. in Chemical and Biomolecular Engineering. I’ve spent the last three years researching cancer metabolism using our unique approach. But what if, when I am Dr. Taylor Murphy, I don’t want to go into academia, industry, consulting, or work for a start-up?

My natural aptitude for science, math, and chemistry made Chemical Engineering the obvious choice. I actually wanted to go into chemistry but I was convinced by my family to do engineering. I think partially for the money and partially because it used more math. I went to graduate school because I was afraid I didn’t know enough with my bachelor’s degree so I went out to ‘learn more’.

**What am I obligated to do once I graduate?** Do I have some great responsibility to stay on this career path and apply my learned skills? What if I choose to never use this knowledge I’ve gained and instead decide to pursue something else.

This imaginary (well maybe not so imaginary) girl that I created and I have a lot in common. We both would be able to use our expertise to help people. We both spent many years learning something. And we both have yearnings to do something entirely different.

On one side of the argument is the idea of having an abundance mentality. I could say, well, there are vast numbers of other engineers in this world. What I don’t do, somebody else will pick up the slack. On the other side, there is a well known need for people in the STEM (Science, Technology, Engineering, and Math) disciplines and I would be doing a disservice to society by not contributing in this way. Would I really be contributing that much, though, if my heart wasn’t really in it?

This is a difficult concept that I’ve been struggling with for a while now. I know, without a doubt, that once I graduate I could get a job _somewhere_ that pays close to 6-figures. There are a lot of people out there right now that would do anything for a high-paying job like that. I could take a job like that, but I have a strong feeling that I would, within a year or two, become quite unhappy with my position. I would keep reading about other people doing things and having experiences that I want, and I would become more pessimistic about my current position. The money is nothing because I know that it won’t fulfill me. It can make me temporarily happy (even if that temporary time is > 5 years) but it won’t fulfill me. If I’m fulfilled in what I’m doing, then I really don’t care how much I make. This is something that’s taken a long time to realize, but it’s true. **All the money in the world is nothing without the fulfillment and satisfaction of knowing I’m doing what makes me truly happy.**

So I ask the question again. What is my obligation? I don’t believe I want to go the traditional route. I’ve been writing a large number of words lately to attempt to build my language skills. I plan on testing, vigorously, other avenues that may interest me. Maybe I will find a great engineering job that is fulfilling and allows me to do the other things I want. Or perhaps I’ll start something now that blossoms into a great opportunity to create the lifestyle I want.

In the end, the girl decided not to go work at a hospital. She chose to work for [Doctor’s Without Borders](https://www.doctorswithoutborders.org/) and contribute to the world in a way that both uses her expertise, fulfills her immensely, and changes people’s lives for the better. If only life were as simple as a story&#8230;