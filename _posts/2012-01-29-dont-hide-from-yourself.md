---
id: 33
title: 'Don&#8217;t Hide From Yourself'
date: 2012-01-29T21:32:01+00:00
author: Taylor Murphy
layout: post
guid: http://www.tayloramurphy.com/blog/?p=33
permalink: /dont-hide-from-yourself/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beginning
  - Dreams
tags:
  - emulate
  - engineer
  - me
  - unique
---
I spent a lot of time recently trying to emulate other people. I looked at all the great things that other people were doing ([Chris Guillebeau](http://chrisguillebeau.com/), [Colin Wright](http://exilelifestyle.com/), [Joshua Millburn](http://joshuafieldsmillburn.com/), [Ryan Nicodemus](http://www.theminimalists.com/), [Leo Babauta](http://zenhabit.net), [Ev Bogue](http://evbogue.com/) and on and on) and I was trying to emulate their success. I wondered if I should travel the world. I wondered if I should give up blogging and just do mailchimp. I wondered if just writing about a singular topic was going to help me get what I wanted.

Then I realized, I wasn’t trying to be me. I was trying to shoehorn my life into somebody else’s success. The few posts I’ve written are all my words, but they’re not 100% _me_. It’s me trying to masquerade as somebody I’m not. I want to be the influential writer who is able to inspire others with words, but I’m not there yet. It’s going to take time. But until then, I have to embrace who _I_ am, not try to be somebody else.

There is a balance between emulation and going it alone. Keeping the blinders on with your nose to the grind stone can lead to missing out on great opportunities to learn new things and connect with new people. However, looking at _everything_ that everybody else is doing and trying to copy everything you see will eventually cause you to lose your uniqueness and originality. You attempt to be something your not and you lose what’s special about what you’re bringing to the table.

I have finally realized that I am an engineer. I have spent almost 10 years training to be an engineer. I cannot escape this defining label. And that’s OK. Instead of running from it and pretending like it never happened, I’m going to embrace it, make it a part of the ‘Brand of Me’ and use it how I want to use it.

In doing this, I’ve found a sense of peace that I’m focused on something that is more aligned with who I actually am as a person.

So, sorry everybody. I’m not going to be just like somebody else. I’m going to be me. I’m going to engineer the uncommon and [live the life I want](http://alex-rubenbauer.de/fbts/index-83.html). My only hope is that I can help others to do the same.