---
id: 125
title: Do I Have to Write, or Do I Just Want to Write?
date: 2012-04-24T14:37:54+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=125
permalink: /do-i-have-to-write-or-do-i-just-want-to-write/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
tags:
  - creativity
  - desire
  - dreams
  - future
  - habit
  - have
  - need
  - passion
  - practice
  - want
  - work
  - write
---
This question was posed to me in epistolary form by Christopher Hitchens. In his _Letters to a Young Contrarian_, he mentions this philosophy in passing, as an afterthought. But it stuck with me through the rest of the book and I had to write it down immediately in my notebook.

This isn’t some tautology. There is a distinct difference. Asking if I want to do something speaks to my immediate needs. Do I want to go see a movie or do I want to take a nap? Do I want to go out to dinner or do I want to stay home and cook? These are choices that I can make based upon my free will.

Asking whether I have to do something is a much deeper question that goes beyond my transient desires. I have many haves in my life, as do all humans.

I have to sleep.

I have to eat.

I have to drink.

But beyond that, what do I really have to do? I’ve been exploring this idea over the past few months. People who are obsessed with an idea, or have a deep burning passion, have to do what they do. They can’t not do it. It is literally beyond their capacity to not do it. I have a few actions in my life that are like this.

I have to read.

I have to exercise.

I have to reflect.

I have to fellowship.

But do I have to write?

Just 6 months ago I wasn’t doing any sort of regular writing. I wrote for work, and I wrote on facebook and in emails, but I wasn’t doing any sort of reflection and creation with the written word. My writing then was just adding to the noise.

Over the past 4 months I’ve started making writing a focused habit. I have a goal of 1000 words / day. I started a blog where I can publish some of my work. I have documents in a folder on my hard drive that hold some of my attempts at fiction as well as 70,000+ words of stuff that will probably never see the light of day.

I’ve consistently tried to improve my writing as well. I’ve read over half a dozen books on how to improve my writing. I’ve tried to curate my thoughts so that when I translate them to the written word, it creates an interesting and compelling story. At the heart of this is my desire to add value to the world through my words.

The choice to start writing was born out of necessity. I recognized my strong need to improve how I write. If I want to create a lifestyle that is location-independent, then my words are going to be one of the primary tools that helps me achieve this goal. Both written and spoken. It was an obvious move for me. To make a living out of words, I have to be skilled in using them.

But something happened the more I practiced. I started to depend on these words. They became a way of healing my soul, if you will. The more I wrote, the calmer I was and the clearer I thought. Every sentence I put down has helped me to become a better person in some small way.

Some times the words flow like a fountain and it’s all I can do to keep up. At other times, more often than not, I have to lasso each one and wrestle it into place. It’s a battle of wills, and each day and each sentence presents a new challenge that I relish overcoming.

There was a period last month where I didn’t write for almost two weeks. I hated that week. My world started to become more chaotic. I could feel myself getting sucked back into old habits and patterns that I used to think made me happy. Without that daily output, I was starting to lose my way.

Once I started laying the tracks again, I was able to realign myself and enhance my calm. Even if I wrote ‘what the hell am I doing’, just seeing those combination of letters helped me to refocus. Those were my words.

Writing has helped me reflect on life and this world and all the crazy shit in it. As I’ve improved my writing, I’ve gained perspective on what really matters. I don’t mean to claim I have everything figured out. That will never happen. But I’m making positive progress towards a life that is meaningful and in alignment with who I truly am.

For so long my mind has been overly logical. Buried behind it was this creative side that needed an escape valve. When I started writing consistently, that buried part of me has been bursting out. It’s unwieldy and times and it certainly shows it’s lack of refinement, but at least it’s getting out. It’s up to me to polish that fountain, focus it’s energy, and create something worthwhile.

So do I have to write? No. I _need_ to write. I _love_ to write. Why else would I force myself to write 1000 words at 3 in the morning while drunk just so I could meet my daily goal? (true story) What else could make me chase this crazy thing other than a deep need to do it?

I am a writer, ergo I write.