---
id: 421
title: The Intentionally Accidental Life
date: 2014-08-10T10:54:25+00:00
author: Taylor Murphy
layout: post
guid: http://tayloramurphy.com/?p=421
permalink: /the-intentionally-accidental-life/
image:
  - ""
seo_follow:
  - 'false'
seo_noindex:
  - 'false'
categories:
  - Beliefs
  - Learning
  - Personal Development
  - Relationships
---
_This is the text of the speech I recently gave at my Toastmasters meeting, which is why I say that you&#8217;re listening to something you&#8217;re clearly reading on a screen._

> “We judge others by their actions but we judge ourselves by our intentions.”

A year ago I was going to become a blogger who traveled the world, lived in small apartments or hostels, had no real long-term plans, and tramped my way across the world. A year before that I was going to be a moderately successful academic who enjoyed teaching and sort of enjoyed research in a topic he somewhat enjoyed. A year before that I was going to be a good Catholic husband and support my wife and who knows how many soon-to-be kids in a house I didn’t really like. And a year before that I was 23 and in the the thick of graduate school because I was afraid of the real world and didn’t want to face the possibility that I wasn’t good at something.

Funny how things change isn’t it? How 3, 4, 5 years go by seemingly all at once. Day by day nothing seems to change but when you look in the mirror everything is different. Weird how our brains work like that.

I’m only 27 years old, perhaps young to some of you listening to this, but I feel I’ve earned the right to have some opinions about life, love, happiness, work, success, and failure. And, since I’ve finished my competent communicator manual, I officially have all the skills I need to speak about whatever I want. So there.

I am beyond lucky to be in the position I’m in right now. I have a wonderful life and I know I’m lucky to have it. I couldn’t be happier with the job I’m working: my coworkers are wonderful, my work is engaging and challenging, and the opportunities that lay before me are numerous and ready. I only need to stand up and claim them. I have a wonderful love life with an amazing woman, two dogs who convinced me with their love and mess that I really am a dog person and not the cat person I may have previously thought. I live in a wealthy country where my income allows me to go on amazing adventures. Just this year I’ve gone scuba diving, white water rafting, horseback riding, bowling, ridden 1/8th scale trains, and hiked in the beautiful nature of Tennessee.

I don’t share any of this to boast. I simply wanted to highlight the wonderful things that are abundant in my life, but also to acknowledge how many of these things came to me out of pure luck. I just happened to be born to two people who live in the USA. We happened to move to move to Chattanooga when I was 10. I happened to meet someone in college in Chattanooga who would later be my ex-wife. I happened to get accepted to Vanderbilt for graduate school instead of Georgia Tech. In my last years in grad school, a guy happened to join the lab who just happened to be friends with my current girlfriend. She happened to get a new dog when we started dating. I fell in love with both. I happened to be looking for a job around the same time my current company was looking for someone with skills similar to mine.

So much of my life was just chance. Yes, there was a part of it that wouldn’t have happened without effort on my part, but a lot of it was just plain luck. I think it’s important to be very open about the role random chance plays in your life. Bad things and good things happen. So it goes.

The second point I wanted to make was a matter of perspective. I briefly listed several things that are going well in my life, but that, of course, is a very edited list. Much like our Facebook pages, I chose to share with you the highlights of my life. Here are some of the lowlights.

I’ve got debt I’m still trying to pay off from when I thought I was going to be a blogger and travel the world. This is on top of the debt I have from my wrist surgery. Plus there is some student loan debt in there. Debt is like a constant monkey on my back. There are days I don’t see my girlfriend for more than 5 minutes in a 72 hour period. She works nightshift and is very driven in her career. We live opposite lives at times even though we share the same bed. I have a total commute time of 1 hour every day and I absolutely hate commuting that long. I’m self conscious about my ribs that poke out way too much and my wrists that are the same size as my 11 year old sister. I have a career that’s fun and exciting but I still really don’t know what I want. I’m making the best of what I have right now but I can’t tell you I want to do X, Y, and Z by the time I’m 40. I struggle greatly with distractions on the internet. It’s the biggest roadblock in my life and it’s a constant struggle. I’m never 100% happy. Everything can be going great and about 5% of me will just be depressed or sad about nothing and everything. It’s a weird trick of the human brain. I’ve lost a lot of friends going through my divorce and while I still have friends and the quality certainly has gone up, I still don’t have the same kind of bonds with other men that I used to have.

I say all this to point out that any event in life can have a number of different perspectives. I’ve made the choice to have a stoic view on most events and push my brain towards the positives. Our beliefs about events in our life, and whether they are good or bad, are just that: beliefs. You can see a car accident as a horrible and stressful thing or you can look for the good in it. You got to meet a new person. You practiced dealing with insurance companies. You became a little more aware about the people stopping behind you in traffic. That’s just one, admittedly poor, example but it’s true about everything that happens in our life. There can be something positive in every event in life. It’s all in how we perceive and what we choose to feel about it. On the flip side of this, you can always find something negative in everything. But that’s a pretty miserable way to live and so I don’t recommend it.

Love is something odd about humans. The robot side of me says it doesn’t make sense and is complicated and annoying. The human side of me very much enjoys love and thinks it’s one of the best things about the world. I’ve dated many people, married and divorced one of them, and am now living with my girlfriend whom I love more than I’ve loved another person. It’s a romantic and cliched thing to say, that I love this person more than I’ve loved anyone, but it’s also objectively true. My capacity for love has grown as I’ve grown and so I am capable of much more love for other people than I used to be. That and my girlfriend is my best friend and she’s just awesome.

For me to learn how to love more I had to learn first how to love myself. This was a complicated process and, fortunately or unfortunately depending on your perspective, happened about a year into my marriage. I was a very selfish, weak-willed, uncommunicative man. I saw myself as a nice guy, but I didn’t respect or love myself enough to take care of myself. I expected others to take care of my needs for me. At some point, and after a lot of reading and soul-searching, if you will, I started to take responsibility for my life. I started to respect myself and my needs more and I started loving myself more. I don’t want to enter the realm of new-agey crap here, but I want to make it clear that before I truly knew how to love and respect another person, I had to know how to love and respect myself. It’s one of the best things I ever did for myself.

This process of learning and growing isn’t over. I’m not perfect and I never will be. But I do strive to be better every day. Some days I make it. Some days I fail. But my eyes and effort are faced forward towards growth, away from stagnation and selfishness. I love more freely and openly because I can and I know that it makes the world a better place.

These are just a few of the lessons I’ve learned these past few years. There’s many more but I think these three have been some of the most productive for me. First: acknowledging that so much of our lives is up to chance. Second, because so much is left to chance, that we have the power to choose how we respond to these situations. We can find the good in everything or give in to cynicism and look for the bad. Third, because we have this power of perspective we can and should look for the good in ourselves thereby loving ourselves more. How we perceive and love ourselves (and others) is not up to chance. That is something we can control: our love.

I don’t claim to have any special knowledge about life, love, or luck. I’m just a guy, a lucky guy at that, who’s doing his best to live and grow in this world just like the rest of you. My only hope is that by sharing my story I’m giving something back to the world that has given so much to me.